package com.relevancelab.ra;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Logger;

import com.relevancelab.ra.data.Agile;
import com.relevancelab.ra.data.AgileMasterList;
import com.relevancelab.ra.data.Audit;
import com.relevancelab.ra.data.FURLS;
import com.relevancelab.ra.data.FURLSMasterList;
import com.relevancelab.ra.data.FdaProCode;
import com.relevancelab.ra.data.GUDID;
import com.relevancelab.ra.data.GUDIDMasterList;
import com.relevancelab.ra.data.ItemMapping;
import com.relevancelab.ra.data.Label;
import com.relevancelab.ra.data.LabelMasterList;
import com.relevancelab.ra.data.Master510k;
import com.relevancelab.ra.data.OracleMaster;
import com.relevancelab.ra.data.OracleMasterList;
import com.relevancelab.ra.data.SourceConsolidation;
import com.relevancelab.ra.util.Util;

public class RaConsolidation {
	static int version;
	Properties properties = null;
	OracleMaster itemInOracle = null;
	Agile itemInAgile = null;
	GUDID itemInGUDID = null;
	FURLS itemInFurls = null;
	Label label = null;
	String itemNo = null;
	String itemKeyOracle = null;
	String itemKeyGudid = null;
	String itemKeyAgile = null;
	String itemKeyWODash = null;
	String itemKeyWODashOracle = null;
	String itemKeyWODashGudid = null;
	String itemKeyWODashAgile = null;
	SourceConsolidation sourceConsolidation = null;
	Map<String, OracleMaster> oracleMasterList = null;
	Map<String, OracleMaster> oracleMasterListWODash = null;
	Map<String, Agile> agileList = null;
	Map<String, Agile> agileListWODash = null;
	Map<String, Label> labelList = null;
	Map<String, Label> labelListWODash = null;
	Map<String, FURLS> furlsListByDL = null;
	Map<String, FURLS> furlsListBySN = null;
	Map<String, GUDID> gudidList = null;
	Map<String, GUDID> gudidListWODash = null;
	Set<String> listingNoSetGUDID = null;
	Set<String> submissionNoGUDID = null;
	Set<String> listingNoSetOracle = null;
	Set<String> submissionNoOracle = null;
	Map<String, FURLS> furlsListByProductCode = null;
	Map<String, Master510k> master510kList = null;
	Map<String, FdaProCode> fdaProCodeList = null;
	Map<String, ItemMapping> itemMappingList = null;
	Map<String, Set<Audit>> auditList = null;
	Set<String> itemKeyList = null;
	Set<String> itemExistence = null;
	Map<String, SourceConsolidation> sourceConsolidationMap = null;
	private static final Logger logger = Logger.getLogger(RaConsolidation.class);
	private static String dbURL = null;
	private static String dbUser = null;
	private static String dbPwd = null;
	private static int batchSize = 5000;
	List<SourceConsolidation> sourceConsolidationList = null;
	private static final String removeSC = "[^a-zA-Z0-9]";
	private static final String error = "Error";
	private static final String preamendment = "Preamendment";
	private static final String enforcementDiscretion = "Enforcement Discretion";
	private static final String exempt = "Exempt";
	private static final String notExempt = "Not Exempt";
	private static final String k510 = "510(k)";
	private static final String pma = "PMA";
	private static final String na = "N/A";
	private static final String gmdnCodeReplacement = ",";
	private static final String SYSTEMGEN = "systemgenerated";
	private static final String hde = "HDE";
	private static boolean update_action = false;
	static int count = 0;

	// ORACLE
	private OracleMasterList setOracleList() throws SQLException {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		OracleMaster oracleMaster = null;
		Map<String, OracleMaster> oracleList = null;
		Map<String, OracleMaster> oracleListWithoutDash = null;
		OracleMasterList oracleMasterList = new OracleMasterList();
		String itemKey = null;
		final String oracleQuery = "select * from l1_oracle_item_master";
		try {
			conn = Util.getPostGresConnection(dbURL, dbUser, dbPwd);
			stmt = conn.createStatement();
			rs = stmt.executeQuery(oracleQuery);
			oracleList = new HashMap<>();
			oracleListWithoutDash = new HashMap<>();
			while (rs.next()) {
				oracleMaster = new OracleMaster();
				itemKey = rs.getString("itemno");
				if (itemKey.isEmpty())
					continue;
				oracleMaster.setItemNo(itemKey);
				oracleMaster.setItemStatus(rs.getString("item_status"));
				oracleMaster.setItemType(rs.getString("item_type"));
				oracleMaster.setProdcutName(rs.getString("product_name"));
				oracleMaster.setIpOwnerName(rs.getString("ip_owner_name"));
				oracleMaster.setIntegraManufactured(rs.getString("integra_manufactured"));
				oracleMaster.setIntegraManufacturerName(rs.getString("integra_manufacturer_name"));
				oracleMaster.setIntegraManufacturerNameDesc(rs.getString("site_descr"));
				oracleMaster.setIls510K20(rs.getString("dff_501k_pma_exempt"));
				oracleMaster.setFdaProcode(rs.getString("dff_fda_product_code_ils"));
				oracleMaster.setIlsFDADeviceListingNo(rs.getString("dff_fda_device_listing_ils"));
				oracleMaster.setMakeBuy(rs.getString("make_buy"));
				oracleMaster.setIlsEstablishmentRegNo(rs.getString("dff_ils_est_registration_no"));
				oracleMaster.setIlsRegAddress(rs.getString("ils_reg_address"));
				oracleMaster.setVendor510KOldHtsDescPrevious(rs.getString("vendor_510k_old_hts_descr"));
				oracleMaster.setVendorFDAEstablishmentRegNo(rs.getString("dff_vendor_fda_registration"));
				oracleMaster.setVendorDeviceNo(rs.getString("dff_vendor_device_number"));
				oracleMaster.setGmdnCode(rs.getString("dff_gmdn_code"));
				oracleMaster.setSiteDesc(rs.getString("site_descr"));
				oracleMaster.setOracleSubmissionNo(rs.getString("dff_501k_pma_exempt"));
				oracleMaster.setIlsDev21(rs.getString("ils_dev_21"));
				oracleMaster.setIlsDev6(rs.getString("ils_dev_6"));
				oracleMaster.setIlsDev21original(rs.getString("ils_dev_21"));
				oracleList.put(itemKey, oracleMaster);
				oracleListWithoutDash.put(itemKey.replaceAll(removeSC, ""), oracleMaster);
			}
			oracleMasterList.setOracleItemList(oracleList);
			oracleMasterList.setOracleItemListWODash(oracleListWithoutDash);
			logger.info("size of oracle List is" + oracleList.size());
		} catch (Exception e) {

			logger.info(error, e);
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (Exception e) {
					logger.info(e);
				}

			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception e) {
					logger.info(e);
				}

			if (conn != null)
				try {
					conn.close();
				} catch (Exception e) {
					logger.info(e);
				}
		}

		return oracleMasterList;
	}

	// agile
	private AgileMasterList setAgileList() throws Exception {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		Map<String, Agile> agileList = null;
		Map<String, Agile> agileListWODash = null;
		Set<String> fdaProCode = null;
		String agileKey = null;
		Agile agile = null;
		AgileMasterList agileMasterList = new AgileMasterList();
		/*
		 * l1_agile_dump is provided by jesus once qfind agile tables are updated we
		 * should use view l1_agile
		 */ final String agileQuery = "select * from l1_agile_dump_view";
		try {
			conn = Util.getPostGresConnection(dbURL, dbUser, dbPwd);
			stmt = conn.createStatement();
			rs = stmt.executeQuery(agileQuery);
			agileList = new HashMap<>();
			agileListWODash = new HashMap<>();
			while (rs.next()) {

				agileKey = rs.getString("item_number");

				if (agileList.containsKey(agileKey)) {
					agileList.get(agileKey).getFdaProductCode().add(rs.getString("fda_product_codes"));
					agileListWODash.get(agileKey.replaceAll(removeSC, "")).getFdaProductCode()
							.add(rs.getString("fda_product_codes"));
				} else {
					fdaProCode = new HashSet<>();
					agile = new Agile();
					agile.setItemNo(agileKey);
					agile.setItemType(rs.getString("item_type"));
					agile.setItemStatus(rs.getString("lifecycle_phase"));
					// agile.setItemStatus(rs.getString("item_status"));
					agile.setProductName(rs.getString("description"));
					fdaProCode.add(rs.getString("fda_product_codes"));
					agile.setFdaProductCode(fdaProCode);
					agile.setFdaClass(rs.getString("fda_class"));
					agile.setFdaRegulatoryStatus(rs.getString("fda_regulatory_status"));
					agile.setDesignSpecificationOwner(rs.getString("design_spec_owner"));
					// agile.setSubMissionNo(rs.getString("premarket_submission_number"));
					agileList.put(agileKey, agile);
					agileListWODash.put(agileKey.replaceAll(removeSC, ""), agile);
				}
			}
			agileMasterList.setAgileList(agileList);
			agileMasterList.setAgileListWODash(agileListWODash);

		} catch (Exception e) {
			logger.info("context4", e);
			e.printStackTrace();
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (Exception e) {
					logger.info(e);
				}

			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception e) {
					logger.info(e);
				}

			if (conn != null)
				try {
					conn.close();
				} catch (Exception e) {
					logger.info(e);
				}
		}
		logger.info("size of agileList is" + agileList.size());
		return agileMasterList;

	}

	// furls
	private FURLSMasterList setFURLSList() throws SQLException {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		FURLS furlsProcode = null;
		logger.info("loading furls");
		FURLSMasterList furlsMasterList = null;

		String furlsSNKey = null;
		String furlsDLKey = null;
		Set<String> productCode = null;
		FURLS furlsSN = null;
		FURLS furlsDL = null;
		String furlsQuery = "select * from l1_furls";
		String listingNumber = null;
		String listingSubmissionNumber = null;
		String listingExceptionFlag = null;
		String furlsProductCodeAsKey = null;
		Set<String> dlNo = null;
		try {
			conn = Util.getPostGresConnection(dbURL, dbUser, dbPwd);
			stmt = conn.createStatement();
			rs = stmt.executeQuery(furlsQuery);
			furlsListBySN = new HashMap<>();
			furlsListByDL = new HashMap<>();
			furlsListByProductCode = new HashMap<>();
			furlsMasterList = new FURLSMasterList();
			while (rs.next()) {
				furlsProductCodeAsKey = rs.getString("listing_product_code");
				furlsSNKey = rs.getString("listing_submission_number");
				furlsDLKey = rs.getString("listing_number");
				listingNumber = rs.getString("listing_number");
				listingSubmissionNumber = furlsSNKey;
				listingExceptionFlag = rs.getString("listing_exception_flag");
				String submissionTypeFurls = null;
				if (listingSubmissionNumber == null && listingExceptionFlag == null)
					submissionTypeFurls = exempt;
				else if (listingSubmissionNumber == null && listingExceptionFlag != null
						&& listingExceptionFlag.equalsIgnoreCase(preamendment))
					submissionTypeFurls = preamendment;
				else if (listingSubmissionNumber == null && listingExceptionFlag != null
						&& listingExceptionFlag.equalsIgnoreCase(enforcementDiscretion))
					submissionTypeFurls = enforcementDiscretion;
				else if (listingSubmissionNumber != null && listingSubmissionNumber.toLowerCase().startsWith("k")
						&& listingExceptionFlag == null)
					submissionTypeFurls = k510;
				else if (listingSubmissionNumber != null && listingSubmissionNumber.toLowerCase().startsWith("p")
						&& listingExceptionFlag == null)
					submissionTypeFurls = pma;
				else if (listingSubmissionNumber != null && listingSubmissionNumber.toLowerCase().startsWith("h")
						&& listingExceptionFlag == null)
					submissionTypeFurls = hde;
				else if (listingSubmissionNumber != null && listingSubmissionNumber.toLowerCase().startsWith("d")
						&& listingExceptionFlag == null)
					submissionTypeFurls = na;

				if (furlsSNKey != null) {
					if (furlsListBySN.containsKey(furlsSNKey)) {
						furlsSN = furlsListBySN.get(furlsSNKey);
						productCode = furlsSN.getProductCode();
						productCode.add(rs.getString("listing_product_code"));
						furlsSN.getDeviceListingNo().add(rs.getString("listing_number"));
					} else {
						dlNo = new HashSet<>();
						productCode = new HashSet<>();
						furlsSN = new FURLS();
						dlNo.add(rs.getString("listing_number"));
						productCode.add(rs.getString("listing_product_code"));
						furlsSN.setProductCode(productCode);
						furlsSN.setDeviceListingNo(dlNo);
						furlsSN.setPreMarketSubmissionNo(listingSubmissionNumber);
						furlsSN.setPreMarketException(listingExceptionFlag);
						furlsSN.setItemTypeFurls(submissionTypeFurls);
						furlsListBySN.put(furlsSNKey, furlsSN);
					}
				}
				if (furlsDLKey != null) {
					if (furlsListByDL.containsKey(furlsDLKey)) {
						furlsDL = furlsListByDL.get(furlsDLKey);
						productCode = furlsDL.getProductCode();
						productCode.add(rs.getString("listing_product_code"));
						furlsDL.getDeviceListingNo().add(rs.getString("listing_number"));
					} else {
						dlNo = new HashSet<>();
						productCode = new HashSet<>();
						dlNo.add(rs.getString("listing_number"));
						productCode.add(rs.getString("listing_product_code"));
						furlsDL = new FURLS();
						furlsDL.setDeviceListingNo(dlNo);
						furlsDL.setProductCode(productCode);
						furlsDL.setPreMarketSubmissionNo(listingSubmissionNumber);
						furlsDL.setPreMarketException(listingExceptionFlag);
						furlsDL.setItemTypeFurls(submissionTypeFurls);
						furlsListByDL.put(furlsDLKey, furlsDL);
					}
				}
				if (furlsProductCodeAsKey != null) {
					if (furlsListByProductCode.containsKey(furlsProductCodeAsKey)) {
						furlsListByProductCode.get(furlsProductCodeAsKey).getProductCode()
								.add(rs.getString("listing_product_code"));
						furlsListByProductCode.get(furlsProductCodeAsKey).getDeviceListingNo()
								.add(rs.getString("listing_number"));
					} else {
						dlNo = new HashSet<>();
						productCode = new HashSet<>();
						dlNo.add(rs.getString("listing_number"));
						productCode.add(rs.getString("listing_product_code"));
						furlsProcode = new FURLS();
						furlsProcode.setDeviceListingNo(dlNo);
						furlsProcode.setProductCode(productCode);
						furlsProcode.setPreMarketSubmissionNo(listingSubmissionNumber);
						furlsProcode.setPreMarketException(listingExceptionFlag);
						furlsProcode.setItemTypeFurls(submissionTypeFurls);
						furlsListByProductCode.put(furlsProductCodeAsKey, furlsProcode);
					}
				}
			}
			furlsMasterList.setFurlsListByDL(furlsListByDL);
			furlsMasterList.setFurlsListBySN(furlsListBySN);
			furlsMasterList.setFurlsListByProductCode(furlsListByProductCode);
			logger.info("size of furls List is " + furlsListBySN.size());
		} catch (Exception e) {
			e.printStackTrace();
			logger.info(e);
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (Exception e) {
					logger.info(e);
				}

			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception e) {
					logger.info(e);
				}

			if (conn != null)
				try {
					conn.close();
				} catch (Exception e) {
					logger.info(e);
				}
		}
		return furlsMasterList;
	}

	// gudidfunction
	private GUDIDMasterList setGUDIDList() throws SQLException {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		logger.info("loading gudid");
		Map<String, GUDID> gudidList = null;
		Map<String, GUDID> gudidListWODash = null;
		GUDID gudid = null;
		String gudidQuery = "select * from l1_gudid";
		String gudidKey = null;
		GUDIDMasterList gudidMasterList = null;
		try {
			gudidMasterList = new GUDIDMasterList();
			conn = Util.getPostGresConnection(dbURL, dbUser, dbPwd);
			stmt = conn.createStatement();
			rs = stmt.executeQuery(gudidQuery);
			Set<String> fdaProductCodes = null;
			Set<String> deviceExempt = null;
			Set<String> fdaPremarketSubmissionNo = null;
			Set<String> gmdnCodes = null;
			Set<String> deviceListingNo = null;
			Set<String> city = null;
			String gmdnCode = null;
			gudidList = new HashMap<>();
			gudidListWODash = new HashMap<>();
			while (rs.next()) {
				gmdnCode = rs.getString("gmdn_pt_code").replaceAll(gmdnCodeReplacement, "");
				gudidKey = rs.getString("version_model_number");
				if (gudidList.containsKey(gudidKey)) {
					gudid = gudidList.get(gudidKey);
					gudid.getFdaProductCodes().add(rs.getString("product_code"));
					String deviceExemptval = rs.getString("dm_exempt");
					if (deviceExemptval != null) {
						if (deviceExemptval.trim().equalsIgnoreCase("false"))
							gudid.getDeviceExempt().add(notExempt);
						else if (deviceExemptval.trim().equalsIgnoreCase("true"))
							gudid.getDeviceExempt().add(exempt);

					}
					gudid.getFdaPremarketSubmissionNo().add(rs.getString("submission_number"));
					gudid.getGmdnCodes().add(gmdnCode);
					gudid.getFdaMedicalDeviceListingNo().add(rs.getString("fda_listing_number"));
					gudid.getCity().add(rs.getString("city"));
				} else {
					gudid = new GUDID();
					city = new HashSet<>();
					fdaProductCodes = new HashSet<>();
					deviceExempt = new HashSet<>();
					fdaPremarketSubmissionNo = new HashSet<>();
					fdaPremarketSubmissionNo.add(rs.getString("submission_number"));

					gmdnCodes = new HashSet<>();
					deviceListingNo = new HashSet<>();
					deviceListingNo.add(rs.getString("fda_listing_number"));
					fdaProductCodes.add(rs.getString("product_code"));
					String deviceExemptval = rs.getString("dm_exempt");
					if (deviceExemptval != null) {
						if (deviceExemptval.trim().equalsIgnoreCase("false"))
							deviceExempt.add(notExempt);
						if (deviceExemptval.trim().equalsIgnoreCase("true"))
							deviceExempt.add(exempt);

					}

					gmdnCodes.add(gmdnCode);
					gudid.setItemNo(gudidKey);
					gudid.setCatalogNo(rs.getString("catalog_number"));
					gudid.setFdaProductCodes(fdaProductCodes);
					gudid.setDeviceExempt(deviceExempt);
					gudid.setFdaPremarketSubmissionNo(fdaPremarketSubmissionNo);
					gudid.setFdaSubmissionSupplementNo(rs.getString("supplement_number"));
					gudid.setFdaMedicalDeviceListingNo(deviceListingNo);
					gudid.setLabelerDUNSNo(rs.getString("duns_number"));
					gudid.setLabelerDUNSDesc("");
					city.add(rs.getString("city"));
					gudid.setCity(city);
					gudid.setCompany(rs.getString("company_name"));
					gudid.setGmdnCodes(gmdnCodes);
					gudid.setDistributionStatus(rs.getString("device_comm_distribution_status"));
				}
				gudidList.put(gudidKey, gudid);
				gudidListWODash.put(gudidKey.replaceAll(removeSC, ""), gudid);
				gudidMasterList.setGudidList(gudidList);
				gudidMasterList.setGudidListWODash(gudidListWODash);

			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.info(error, e);
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (Exception e) {
					logger.info(e);
				}

			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception e) {
					logger.info(e);
				}

			if (conn != null)
				try {
					conn.close();
				} catch (Exception e) {
					logger.info(e);
				}
		}
		logger.info("after loading size of gudid is " + gudidList.size());
		return gudidMasterList;
	}

	private LabelMasterList setLabelList() throws SQLException {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		logger.info("loading gudid");
		Map<String, Label> labelList = null;
		Map<String, Label> labelListWODash = null;
		Label label = null;
		String labelQuery = "select item_number, case when manufacturer is null then '' "
				+ "else manufacturer end as manufacturer,title from l1_solrmaster_label";
		String labelKey = null;
		LabelMasterList labelMasterList = null;
		try {
			labelMasterList = new LabelMasterList();
			conn = Util.getPostGresConnection(dbURL, dbUser, dbPwd);
			stmt = conn.createStatement();
			rs = stmt.executeQuery(labelQuery);
			labelList = new HashMap<>();
			labelListWODash = new HashMap<>();
			while (rs.next()) {
				label = new Label();
				labelKey = rs.getString("item_number");
				label.setItemNo(labelKey);
				label.setManufacturer(rs.getString("manufacturer"));
				label.setProductName(rs.getString("title"));
				labelList.put(labelKey, label);
				labelListWODash.put(labelKey.replaceAll(removeSC, ""), label);
			}
			labelMasterList.setLabelList(labelList);
			labelMasterList.setLabelListWODash(labelListWODash);
			logger.info("after loading size of gudid is " + labelList.size());
		} catch (Exception e) {
			logger.info(error, e);
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (Exception e) {
					logger.info(e);
				}

			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception e) {
					logger.info(e);
				}

			if (conn != null)
				try {
					conn.close();
				} catch (Exception e) {
					logger.info(e);
				}
		}

		return labelMasterList;
	}

	// masterf
	private Map<String, Master510k> setMaster510kList() throws SQLException {
		Map<String, Master510k> master510List = new HashMap<>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String masterItemQuery = "select * from l1_master_510k";
		try {
			conn = Util.getPostGresConnection(dbURL, dbUser, dbPwd);
			stmt = conn.createStatement();
			rs = stmt.executeQuery(masterItemQuery);
			while (rs.next()) {
				String supplementNo = rs.getString("supplement_number");
				String clearanceApprovalDate = String.valueOf(rs.getString("clearance_date"));
				String submissionDate = String.valueOf(rs.getString("received_date"));
				String submissionNo = String.valueOf(rs.getString("n510k_no"));
				String deviceClass = String.valueOf(rs.getString("device_class"));
				Master510k master510k = new Master510k();
				master510k.setClearanceApprovalDate(clearanceApprovalDate);
				master510k.setSubmissionDate(submissionDate);
				master510k.setSubmissionNo(submissionNo);
				master510k.setDeviceClass(deviceClass);
				// submission no is key i.e k number
				if (supplementNo != null) {
					int len = supplementNo.length();
					if (len > 3)
						supplementNo = supplementNo.substring(len - 3, len);
				}
				master510List.put(submissionNo + supplementNo, master510k);

			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.info(error, e);
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (Exception e) {
					logger.info(e);
				}

			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception e) {
					logger.info(e);
				}

			if (conn != null)
				try {
					conn.close();
				} catch (Exception e) {
					e.printStackTrace();
					logger.info(e);
				}
		}

		return master510List;
	}

	// fda pro code tablfe
	private Map<String, FdaProCode> setProCodeList() throws SQLException {
		Map<String, FdaProCode> proCodeList = new HashMap<>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String query = "select * from l1_fda_pro_code where productcode is not null;";
		try {
			conn = Util.getPostGresConnection(dbURL, dbUser, dbPwd);
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);
			while (rs.next()) {

				Set<String> productcodes = new HashSet<>(Arrays.asList(rs.getString("productcode").split(";")));
				String deviceclass = rs.getString("deviceclass");
				for (String productcode : productcodes) {
					FdaProCode fdaProCode = new FdaProCode();
					fdaProCode.setDeviceclass(deviceclass);
					fdaProCode.setProductcode(productcode);
					proCodeList.put(productcode.trim(), fdaProCode);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.info(error, e);
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (Exception e) {
					logger.info(e);
				}

			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception e) {
					logger.info(e);
				}

			if (conn != null)
				try {
					conn.close();
				} catch (Exception e) {
					e.printStackTrace();
					logger.info(e);
				}
		}
		return proCodeList;
	}

	private Map<String, ItemMapping> setItemMappingList() throws SQLException {
		ItemMapping itemMapping = null;
		Map<String, ItemMapping> itemMappingList = new HashMap<>();
		String query = "select * from mapping_list";
		try (Connection conn = Util.getPostGresConnection(dbURL, dbUser, dbPwd);
				PreparedStatement oracleStmt = conn.prepareStatement(query);
				ResultSet rs = oracleStmt.executeQuery()) {
			while (rs.next()) {
				itemMapping = new ItemMapping();
				String key = rs.getString("item_no");
				itemMapping.setItemNo(key);
				itemMapping.setItemNoOracle(rs.getString("item_no_oracle"));
				itemMapping.setItemInGudid(rs.getString("item_in_gudid"));
				itemMapping.setItemNoAgile(rs.getString("item_no_agile"));
				itemMapping.setVersionModelNumberGudid(rs.getString("version_model_number_gudid"));
				itemMapping.setCatalogNumberGudid(rs.getString("catalog_number_gudid"));
				itemMapping.setProdcuctNameOnLabel(rs.getString("product_name_on_label"));
				itemMapping.setLegalalMgfOnLabel(rs.getString("legal_manufacturer_on_label"));
				itemMapping.setDivision(rs.getString("divison"));
				itemMapping.setSiteRollup(rs.getString("site_rollup"));
				itemMappingList.put(key, itemMapping);
			}

		}

		catch (Exception e) {
			e.printStackTrace();
			logger.info(e);
		}
		return itemMappingList;

	}

	private Set<String> getInvoiceItemList() {
		Set<String> itemList = null;
		String query = "select sku as item_no  from l0_invoice group by sku";
		try (Connection conn = Util.getPostGresConnection(dbURL, dbUser, dbPwd);
				PreparedStatement pgStmt = conn.prepareStatement(query);
				ResultSet rs = pgStmt.executeQuery()) {
			itemList = new HashSet<>();
			while (rs.next()) {

				String itemNo = rs.getString("item_no");
				itemList.add(itemNo);
			}

		}

		catch (Exception e) {
			e.printStackTrace();
			logger.info(e);
		}

		return itemList;
	}
	/*
	 * private List<ActiveItems> setActiveItemList() throws SQLException {
	 * Connection conn = null; Statement stmt = null; ResultSet rs = null;
	 * List<ActiveItems> activeItemsList = null; String item = null; ActiveItems
	 * activeItems = null; String activeItemQuery =
	 * "select itemno from l1_active_item_list"; try { conn =
	 * Util.getPostGresConnection(dbURL, dbUser, dbPwd); stmt =
	 * conn.createStatement(); rs = stmt.executeQuery(activeItemQuery);
	 * activeItemsList = new ArrayList<>(); while (rs.next()) { activeItems = new
	 * ActiveItems(); item = rs.getString("itemno"); activeItems.setItemNO(item);
	 * activeItemsList.add(activeItems); } } catch (Exception e) {
	 * e.printStackTrace(); logger.info(error, e); } finally { if (rs != null) try {
	 * rs.close(); } catch (Exception e) { logger.info(e); }
	 * 
	 * if (stmt != null) try { stmt.close(); } catch (Exception e) { logger.info(e);
	 * }
	 * 
	 * if (conn != null) try { conn.close(); } catch (Exception e) {
	 * e.printStackTrace(); logger.info(e); } } return activeItemsList; }
	 */

	private void buildRaConsolidationTable() throws SQLException, IOException {
		try {
			sourceConsolidationList = new ArrayList<>();
			// getting all the required list
			OracleMasterList oracleMaster = this.setOracleList();
			if (oracleMaster != null) {
				oracleMasterList = oracleMaster.getOracleItemList();
				oracleMasterListWODash = oracleMaster.getOracleItemListWODash();

			}

			AgileMasterList agileMaster = this.setAgileList();
			if (agileMaster != null) {
				agileList = agileMaster.getAgileList();
				agileListWODash = agileMaster.getAgileListWODash();
			}

			LabelMasterList labelMaster = this.setLabelList();
			if (labelMaster != null) {
				labelList = labelMaster.getLabelList();
				labelListWODash = labelMaster.getLabelListWODash();
			}

			FURLSMasterList furlsMaster = this.setFURLSList();
			if (furlsMaster != null) {
				furlsListByDL = furlsMaster.getFurlsListByDL();
				furlsListBySN = furlsMaster.getFurlsListBySN();
				furlsListByProductCode = furlsMaster.getFurlsListByProductCode();
			}

			GUDIDMasterList gudidMaster = this.setGUDIDList();
			if (gudidMaster != null) {
				gudidList = gudidMaster.getGudidList();
				gudidListWODash = gudidMaster.getGudidListWODash();
			}

			master510kList = this.setMaster510kList();
			fdaProCodeList = this.setProCodeList();
			itemMappingList = this.setItemMappingList();
			itemKeyList = itemMappingList.keySet();
			this.setSourceConsolidationList();

		} catch (

		Exception e) {
			e.printStackTrace();
			logger.info(error, e);
		}

	}

	@SuppressWarnings("deprecation")
	private void sourceComparisonResults(int flag) throws IOException {

		String query = "select *,to_char(now(), 'MON-yyyy') as date from source_comparison";
		int match = 0;
		int mismatch = 0;
		int open = 0;
		int close = 0;
		String month = null;
		String description = null;
		String insertQuery = "insert into source_comparison_results(description,match,mismatch,open,close,month_year)"
				+ "values(?,?,?,?,?,?)";
		try (Connection conn = Util.getPostGresConnection(dbURL, dbUser, dbPwd);
				PreparedStatement oracleStmt = conn.prepareStatement(query);
				PreparedStatement updateStmt = conn.prepareStatement(insertQuery);
				ResultSet rs = oracleStmt.executeQuery();
				FileWriter out = new FileWriter(new File("config.properties"))) {
			// checking rs if it null then there is no data in consolidation
			// table

			while (rs.next()) {
				match = Integer.parseInt(rs.getString("match"));
				mismatch = Integer.parseInt(rs.getString("mismatch"));
				open = Integer.parseInt(rs.getString("open"));
				close = Integer.parseInt(rs.getString("close"));
				month = rs.getString("date");
				DateFormat format = new SimpleDateFormat("MMM-yyyy", Locale.ENGLISH);
				Date presentDate = format.parse(month);
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(presentDate);
				calendar.add(calendar.MONTH, -1);
				Date pastDate = calendar.getTime();
				DateFormat formatDate = new SimpleDateFormat("MMM-yyyy");
				String present = formatDate.format(presentDate);
				String past = formatDate.format(pastDate);

				if (version == 0) {
					description = "initial run";
					this.properties.setProperty("version", String.valueOf(version + 1));
					this.properties.store(out, null);
				} else {
					if (flag == 0)
						// description = "before running version " + String.valueOf(version + 1);
						description = "End of " + past;
					else {
						// description = "Begining of " + String.valueOf(version + 1);
						description = "Begining of " + present;
						this.properties.setProperty("version", String.valueOf(version + 1));
						this.properties.store(out, null);
						out.close();
						/*
						 * this.properties.put(,); this.properties.store(out, "updating values");
						 * out.close();
						 */ }
				}

			}

			updateStmt.setString(1, description);
			updateStmt.setInt(2, match);
			updateStmt.setInt(3, mismatch);
			updateStmt.setInt(4, open);
			updateStmt.setInt(5, close);
			updateStmt.setString(6, month);
			updateStmt.executeUpdate();
		} catch (

		Exception e) {
			e.printStackTrace();

		}

	}

	private void setSourceConsolidationList() {
		/*
		 * for every item in active items we calculte value of fileds in source
		 * consolidation table using oracle, furls,agile,gudid as base tables
		 */
		Set<String> invoiceList = this.getInvoiceItemList();
		sourceConsolidationMap = new HashMap<>();
		for (String itemKey : itemKeyList) {
			sourceConsolidation = new SourceConsolidation();
			itemKeyOracle = itemMappingList.containsKey(itemKey) ? itemMappingList.get(itemKey).getItemNoOracle()
					: null;
			itemKeyGudid = itemMappingList.containsKey(itemKey)
					? itemMappingList.get(itemKey).getVersionModelNumberGudid()
					: null;
			itemKeyAgile = itemMappingList.containsKey(itemKey) ? itemMappingList.get(itemKey).getItemNoAgile() : null;

			// to debug for item no -- itemKey=" "
			if (itemKeyOracle != null) {
				itemKeyWODashOracle = itemKeyOracle.replaceAll(removeSC, "");
			}
			if (itemKeyGudid != null) {
				itemKeyWODashGudid = itemKeyGudid.replaceAll(removeSC, "");
			}
			if (itemKeyAgile != null) {
				itemKeyWODashAgile = itemKeyAgile.replaceAll(removeSC, "");
			}
			if (itemKey != null) {
				itemKeyWODash = itemKey.replaceAll(removeSC, "");
			}
			if (oracleMasterList != null) {
				itemInOracle = oracleMasterList.get(itemKeyOracle);
			}

			if (itemInOracle == null)
				itemInOracle = oracleMasterListWODash.get(itemKeyWODashOracle);

			itemInGUDID = gudidList != null ? gudidList.get(itemKeyGudid) : null;
			if (itemInGUDID == null && gudidListWODash != null)
				itemInGUDID = gudidListWODash.get(itemKeyWODashGudid);

			itemInAgile = agileList != null ? agileList.get(itemKeyAgile) : null;
			/*
			 * if (itemInAgile == null && agileListWODash != null) itemInAgile =
			 * agileListWODash.get(itemKeyWODashAgile);
			 */
			label = labelList != null ? labelList.get(itemKey) : null;
			if (label == null && labelListWODash != null)
				label = labelListWODash.get(itemKeyWODash);

			itemNo = itemKey;
			ItemMapping itemMapping = itemMappingList.get(itemKey);
			sourceConsolidation.setItemNo(itemNo);
			sourceConsolidation.setIsItemInOracle(itemInOracle);
			sourceConsolidation.setIsItemInAgile(itemInAgile);
			sourceConsolidation.setCatNOnLabel(itemMapping);
			sourceConsolidation.setItemStatus(itemInOracle, itemInAgile);
			sourceConsolidation.setDivision(itemMapping);
			sourceConsolidation.setSiteRollup(itemMapping);
			sourceConsolidation.setItemType(itemInOracle, itemInAgile);
			sourceConsolidation.setProductName(itemInOracle, itemInAgile);
			sourceConsolidation.setProductNameOnTheLabel(itemMapping);
			sourceConsolidation.setLegalManufacturer(itemMapping);
			sourceConsolidation.setDesignSpecificatinOwner(itemInAgile);

			// sourceConsolidation.setIpOwnerName(itemInOracle);
			sourceConsolidation.setIntgMfg(itemInOracle);
			sourceConsolidation.setIntgMfgName(itemInOracle);
			// sourceConsolidation.setSiteDescrIntegraMfrName(itemInOracle);
			sourceConsolidation.setIls510k20(itemInOracle);
			sourceConsolidation.setIlsFda21(itemInOracle);
			sourceConsolidation.setIlsDev6(itemInOracle);
			sourceConsolidation.setMakeBuy(itemInOracle);
			sourceConsolidation.setIlsEstReg_22(itemInOracle);
			// sourceConsolidation.setIlsRegAddress(itemInOracle);
			// sourceConsolidation.setVendor510kOldHtsDescrPrevious(itemInOracle);
			sourceConsolidation.setVendEstReg25(itemInOracle);
			sourceConsolidation.setVendDev23(itemInOracle);
			sourceConsolidation.setGmdn28(itemInOracle);

			// gudid
			sourceConsolidation.setItemInGudid(itemInGUDID);
			sourceConsolidation.setModelVersion(itemInGUDID);
			sourceConsolidation.setCatNo(itemInGUDID);

			sourceConsolidation.setLabeleDunsInGudid(itemInGUDID);
			sourceConsolidation.setFdaProductCodesInGudid(itemInGUDID);
			sourceConsolidation.setDeviceExemptFromPremarkeSubmissionRequirementsGudid(itemInGUDID);
			sourceConsolidation.setFdaPremarketSubmissionNumbersGudid(itemInGUDID);
			sourceConsolidation.setFdaSubmissionSupplementNumberGudid(itemInGUDID);
			sourceConsolidation.setFdaMedicalDeviceListingNumberDl(itemInGUDID);
			sourceConsolidation.setGmdnCodesGudid(itemInGUDID);

			// based on gudid furls derivations
			sourceConsolidation.setDlInFurlsBasedOnSubmissionInGudid(itemInGUDID, furlsListBySN);
			sourceConsolidation.setProductCodeInFurlsBasedOnSubmissionInGudid(itemInGUDID, furlsListBySN);
			sourceConsolidation.setSubmissionExceptionInFurlsBasedOnDlInGudid(itemInGUDID, furlsListByDL);
			sourceConsolidation.setProductCodeInFurlsBasedOnDlInGudid(itemInGUDID, furlsListByDL);

			// based on oracle furls values
			sourceConsolidation.setDlInFurlsBasedOnSubmissionInOracle(itemInOracle, furlsListBySN);
			sourceConsolidation.setProductCodeInFurlsBasedOnSubmissionInOracle(itemInOracle, furlsListBySN);
			sourceConsolidation.setSubmissionExceptionInFurlsBasedOnDlInOracle(itemInOracle, furlsListByDL);
			sourceConsolidation.setProductCodeInFurlsBasedOnDlInOracle(itemInOracle, furlsListByDL);
			// agile data
			sourceConsolidation.setFdaClassAgile(itemInAgile);
			sourceConsolidation.setFdaProductCodeInAgile(itemInAgile);
			sourceConsolidation.setFdaRegulatoryStatus(itemInAgile);

			// furls based on aglie
			sourceConsolidation.setDLNoInFURLSBasedOnProcodeInAgile(itemInAgile, furlsListByProductCode);
			sourceConsolidation.setSubmissionNoExceptionInFURLSBasedOnProcodeInAgile(itemInAgile,
					furlsListByProductCode);
			// device class
			/*
			 * sourceConsolidation.
			 * setDeviceClassrFomMaster510kpmaLogBasedOnSubmissionInGudid( itemInGUDID,
			 * master510kList, fdaProCodeList); sourceConsolidation.
			 * setDeviceClassFromMaster510kpmaLogBasedOnSubmissionOracle( itemInOracle,
			 * itemInGUDID, master510kList, fdaProCodeList); sourceConsolidation.
			 * setDeviceClassFromFdaPublicProCodeDbBasedOnProCodeAgile( itemInAgile,
			 * furlsListByProductCode, fdaProCodeList);
			 */
			// green
			sourceConsolidation.setFdaSubmissionNumberG(itemInGUDID, itemInOracle, itemInAgile, furlsListBySN);
			sourceConsolidation.setFdaSubmissionSupplementNumberG(itemInGUDID);
			sourceConsolidation.setApprovalDateG(master510kList);
			sourceConsolidation.setSubmissionDateG(master510kList);
			sourceConsolidation.setSubmissionStatusG();
			sourceConsolidation.setSubmissionTypeG(itemInGUDID, itemInOracle, furlsListBySN, itemInAgile,
					furlsListByDL);
			sourceConsolidation.setApprovedG();
			sourceConsolidation.setFdaProductCodeCalculatedG(itemInGUDID, itemInOracle, itemInAgile,
					furlsListByProductCode);
			sourceConsolidation.setFdaClassG(master510kList, fdaProCodeList);

			sourceConsolidation.setGmdnCodesG(itemInOracle, itemInGUDID);
			sourceConsolidation.setFdaMedicalDeviceListingNumberG(itemInGUDID, itemInOracle, itemInAgile);
			sourceConsolidation.setStatus();
			sourceConsolidation.setAction();
			sourceConsolidation.setUpdated_by();
			sourceConsolidation.setDistributionStatus(itemInGUDID);
			sourceConsolidation.setItemExclusionFlag("");
			// first using only 3 characters for all calculation then showing original
			// procode
			sourceConsolidation.setIlsFda21(itemInOracle, 1);
			sourceConsolidation.setK510dtIfUsedToBringSkuToMarketG(na);
			sourceConsolidation.setInvoiceFlag(invoiceList, itemNo);
			sourceConsolidationList.add(sourceConsolidation);
			sourceConsolidationMap.put(itemNo, sourceConsolidation);
		}
	}

	private void setAuditList() {
		String query = "select * from updated_user_results";
		String key = null;
		Audit audit = null;
		Set<Audit> auditSet = null;

		try (Connection conn = Util.getPostGresConnection(dbURL, dbUser, dbPwd);
				PreparedStatement auditStmt = conn.prepareStatement(query);
				ResultSet rs = auditStmt.executeQuery()) {
			auditList = new HashMap<>();
			while (rs.next()) {
				audit = new Audit();
				key = rs.getString("item_no");
				audit.setItemNo(key);
				audit.setItemInGudid(rs.getString("item_in_gudid"));
				audit.setModelVersion(rs.getString("model_version"));
				audit.setFdaProCodes(rs.getString("fda_product_codes"));
				audit.setDeviceExemptFromPremarketSubmissionRequirements(
						rs.getString("device_exempt_from_premarket_submission_requirements"));
				audit.setFdaPremarketSubmissionNumbers(rs.getString("fda_premarket_submission_numbers"));
				audit.setFdaSupplementNumbers(rs.getString("fda_submission_supplement_numbers"));
				audit.setFdaMedicalDeviceLisntingNodl(rs.getString("fda_medical_device_listing_number_dl"));
				audit.setDlInFurlsBasedOnSubmissionInGudid(rs.getString("dl_in_furls_based_on_submission_in_gudid"));
				audit.setProductCodeInFurlsBasedOnSubmissionInGudid(
						rs.getString("product_code_in_furls_based_on_submission_in_gudid"));
				audit.setSubmissionExceptionInFurlsBasedOnDlInGudid(
						rs.getString("submission__exception_in_furls_based_on_dl_in_gudid"));
				audit.setProductCodeInFurlsBasedOndLInGudid(
						rs.getString("product_code_in_furls_based_on_dl_in_gudid_"));
				audit.setIls510k20(rs.getString("ils_510k_20"));
				audit.setIlsFda21(rs.getString("ils_fda_21"));
				audit.setIlsDev6(rs.getString("ils_dev_6"));
				audit.setdLInFurlsBasedOnSubmissionInOracle(rs.getString("dl_in_furls_based_on_submission_in_oracle"));
				audit.setProductCodeInFurlsBasedOnSubmissionInOracle(
						rs.getString("product_code_in_furls_based_on_submission_in_oracle"));
				audit.setSubmissionExceptionInFurlsBasedOnDLInOracle1(
						rs.getString("submission_exception_in_furls_based_on_dl_in_oracle1"));
				audit.setProductCodeInFurlsBasedOnDLInOracle(
						rs.getString("product_code_in_furls_based_on_dl_in_oracle"));

				audit.setFdaClass1(rs.getString("fda_class1"));
				audit.setFdaProductCode1(rs.getString("fda_product_code1"));
				audit.setFdaRegulatoryStatus(rs.getString("fda_regulatory_status"));
				audit.setDlInFurlsBasedOnProCodeInAgile(rs.getString("dl_in_furls_based_on_submission_in_oracle_"));
				audit.setSubmissionExceptionInFurlsbasedonproductCodeInAgile(
						rs.getString("submission_exception_in_furls_based_on_dl_in_oracle"));

				audit.setDivison(rs.getString("division"));
				audit.setSiteRollup(rs.getString("site_rollup"));

				audit.setApproved(rs.getString("approved"));
				audit.setFdaProCode(rs.getString("fda_product_code"));
				audit.setFdaClass(rs.getString("fda_class"));
				audit.setSubmissionType(rs.getString("submission_type"));
				audit.setFdaSubmissionNumber(rs.getString("fda_submission_number"));
				audit.setFdaMedicalDLnumber(rs.getString("fda_medical_device_listing_number"));
				audit.setFdaSubmissionSupplementNumber(rs.getString("fda_submission_supplement_number"));// fda_submission_supplement_number
																											// is green
																											// section
																											// field
				audit.setApprovalDate(rs.getString("approval_date"));
				audit.setSubmissionStatus(rs.getString("submission_status"));
				audit.setSubmissionDate(rs.getString("submission_date"));
				audit.setK510DtIfUsedToBringSkuToMarket(rs.getString("k510_dt_if_used_to_bring_sku_to_market"));// ils
																												// 510k
																												// 20 is
																												// not
				// referring to
				// ils_510k_20,is referring
				// to
				// k510_dt_if_used_to_bring_sku_to_market(green
				// section original fiels
				// name id 510k_dt_if_bring
				// to sku market) in
				// consolidation
				audit.setGmdnCodes(rs.getString("gmdn_codes"));
				audit.setCatNoOnLabel(rs.getString("cat_no_on_label"));
				audit.setItemStatus(rs.getString("item_status"));
				audit.setItemType(rs.getString("item_type"));
				audit.setProductName(rs.getString("product_name"));
				audit.setProductNameOnTheLabel(rs.getString("product_name_on_the_label"));
				audit.setLegalManufacturer(rs.getString("legal_manufacturer"));
				audit.setComments(rs.getString("comments"));
				audit.setUpdatedBy(rs.getString("updated_by"));
				audit.setAction(rs.getString("action"));
				audit.setUserStatus(rs.getString("user_status"));

				if (auditList.containsKey(key)) {
					auditList.get(key).add(audit);
				} else {
					auditSet = new HashSet<>();
					auditSet.add(audit);
					auditList.put(key, auditSet);
				}
			}
		} catch (Exception e) {
			logger.error("exceptino due to", e);
		}
	}

	private String updatedValue(String currVal, String sysVal, String userVal) {
		String updateVal = null;
		if (currVal != null && sysVal != null && !currVal.trim().equalsIgnoreCase(sysVal.trim())) {
			updateVal = currVal;
			/*
			 * if (action_udpdate) { update_action = true; }
			 */
		} else
			updateVal = userVal;

		return updateVal;
	}

	boolean auditCompare(String newVal, String oldVal) {
		if (String.valueOf(newVal).trim().toLowerCase().equals(String.valueOf(oldVal).trim().toLowerCase()))
			return false;
		else
			return true;
	}

	private void deltaRefresh() {
		this.setAuditList();
		Set<String> keySet = this.auditList.keySet();
		Audit auditUser = null;
		Audit systemUser = null;
		SourceConsolidation sourceConsolidation = null;
		String currVal = null;
		String sysVal = null;
		String userVal = null;
		String updatedVal = null;
		for (String key : keySet) {

			if (sourceConsolidationMap.containsKey(key)) {
				sourceConsolidation = sourceConsolidationMap.get(key);
				for (Audit audit : auditList.get(key)) {
					if (!SYSTEMGEN.equals(audit.getUpdatedBy()))
						auditUser = audit;
					else
						systemUser = audit;

				}
			}
			boolean actionRun = false;

			if (auditUser == null) {
				actionRun = true;
			} else {
				actionRun = !actionRun ? auditCompare(sourceConsolidation.getItemInGudid(), auditUser.getItemInGudid())
						: true;
				actionRun = !actionRun
						? auditCompare(sourceConsolidation.getModelVersion(), auditUser.getModelVersion())
						: true;
				actionRun = !actionRun
						? auditCompare(sourceConsolidation.getFdaProductCodesInGudid(), auditUser.getFdaProCodes())
						: true;
				actionRun = !actionRun
						? auditCompare(sourceConsolidation.getDeviceExemptFromPremarkeSubmissionRequirementsGudid(),
								auditUser.getDeviceExemptFromPremarketSubmissionRequirements())
						: true;
				actionRun = !actionRun
						? auditCompare(sourceConsolidation.getFdaPremarketSubmissionNumbersGudid(),
								auditUser.getFdaPremarketSubmissionNumbers())
						: true;
				actionRun = !actionRun
						? auditCompare(sourceConsolidation.getFdaSubmissionSupplementNumberGudid(),
								auditUser.getFdaSupplementNumbers())
						: true;
				actionRun = !actionRun
						? auditCompare(sourceConsolidation.getFdaMedicalDeviceListingNumberDlGudid(),
								auditUser.getFdaMedicalDeviceLisntingNodl())
						: true;

				actionRun = !actionRun
						? auditCompare(sourceConsolidation.getDlInFurlsBasedOnSubmissionInGudid(),
								auditUser.getDlInFurlsBasedOnSubmissionInGudid())
						: true;

				actionRun = !actionRun
						? auditCompare(sourceConsolidation.getProductCodeInFurlsBasedOnSubmissionInGudid(),
								auditUser.getProductCodeInFurlsBasedOnSubmissionInGudid())
						: true;
				actionRun = !actionRun
						? auditCompare(sourceConsolidation.getSubmissionExceptionInFurlsBasedOnDlInGudid(),
								auditUser.getSubmissionExceptionInFurlsBasedOnDlInGudid())
						: true;
				actionRun = !actionRun
						? auditCompare(sourceConsolidation.getProductCodeInFurlsBasedOnDlInGudid(),
								auditUser.getProductCodeInFurlsBasedOndLInGudid())
						: true;
				actionRun = !actionRun ? auditCompare(sourceConsolidation.getIls510k20(), auditUser.getIls510k20())
						: true;
				actionRun = !actionRun ? auditCompare(sourceConsolidation.getIlsFda21(), auditUser.getIlsFda21())
						: true;

				actionRun = !actionRun ? auditCompare(sourceConsolidation.getIlsDev6(), auditUser.getIlsDev6()) : true;
				actionRun = !actionRun
						? auditCompare(sourceConsolidation.getDlInFurlsBasedOnSubmissionInOracle(),
								auditUser.getdLInFurlsBasedOnSubmissionInOracle())
						: true;

				actionRun = !actionRun
						? auditCompare(sourceConsolidation.getProductCodeInFurlsBasedOnSubmissionInOracle(),
								auditUser.getProductCodeInFurlsBasedOnSubmissionInOracle())
						: true;

				actionRun = !actionRun
						? auditCompare(sourceConsolidation.getSubmissionExceptionInFurlsBasedOnDlInOracle(),
								auditUser.getSubmissionExceptionInFurlsBasedOnDLInOracle1())
						: true;
				actionRun = !actionRun
						? auditCompare(sourceConsolidation.getProductCodeInFurlsBasedOnDlInOracle(),
								auditUser.getProductCodeInFurlsBasedOnDLInOracle())
						: true;
				actionRun = !actionRun ? auditCompare(sourceConsolidation.getFdaClassAgile(), auditUser.getFdaClass1())
						: true;
				actionRun = !actionRun
						? auditCompare(sourceConsolidation.getFdaProductCodeInAgile(), auditUser.getFdaProductCode1())
						: true;
				actionRun = !actionRun
						? auditCompare(sourceConsolidation.getFdaRegulatoryStatus(), auditUser.getFdaRegulatoryStatus())
						: true;
				actionRun = !actionRun
						? auditCompare(sourceConsolidation.getDLNoInFURLSBasedOnProcodeInAgile(),
								auditUser.getDlInFurlsBasedOnProCodeInAgile())
						: true;
				actionRun = !actionRun
						? auditCompare(sourceConsolidation.getSubmissionNoExceptionInFURLSBasedOnProcodeInAgile(),
								auditUser.getSubmissionExceptionInFurlsbasedonproductCodeInAgile())
						: true;

			}
			if (!actionRun) {
				// sourceConsolidation.setDivision(auditUser.getDivison());
				// sourceConsolidation.setSiteRollup(auditUser.getSiteRollup());
				sourceConsolidation.setGmdnCodesG(auditUser.getGmdnCodes());
				sourceConsolidation.setApprovedG(auditUser.getApproved());
				sourceConsolidation.setFdaProductCodeCalculatedG(auditUser.getFdaProCode());
				sourceConsolidation.setFdaClassG(auditUser.getFdaClass());
				sourceConsolidation.setSubmissionTypeG(auditUser.getSubmissionType());
				sourceConsolidation.setFdaSubmissionNumberG(auditUser.getFdaSubmissionNumber());
				sourceConsolidation.setFdaMedicalDeviceListingNumberG(auditUser.getFdaMedicalDLnumber());
				sourceConsolidation.setFdaSubmissionSupplementNumberG(auditUser.getFdaSubmissionSupplementNumber());
				sourceConsolidation.setApprovalDateG(auditUser.getApprovalDate());
				sourceConsolidation.setSubmissionStatusG(auditUser.getSubmissionStatus());
				sourceConsolidation.setSubmissionDateG(auditUser.getSubmissionDate());
				// sourceConsolidation.setCatNOnLabel(auditUser.getCatNoOnLabel());
				// sourceConsolidation.setItemStatus(auditUser.getItemStatus());
				// sourceConsolidation.setItemType(auditUser.getItemType());
				// sourceConsolidation.setProductName(auditUser.getProductName());
				// sourceConsolidation.setProductNameOnTheLabel(auditUser.getProductNameOnTheLabel());
				// sourceConsolidation.setLegalManufacturer(auditUser.getLegalManufacturer());
				sourceConsolidation.setAction(auditUser.getAction());
				sourceConsolidation.setUserStatus(auditUser.getUserStatus());
				sourceConsolidation.setUpdated_by(auditUser.getUpdatedBy());
				sourceConsolidation.setCommentsG(auditUser.getComments());

			}
		}
	}

	private void addingRaConsolidationtoDb() {

		if (sourceConsolidationList.isEmpty())
			logger.info("list is empty");

		else {
			int counter = 0;
			String insertQuery = "INSERT INTO temp_l1_source_consolidation(item_no, is_item_in_oracle, is_item_in_agile,is_item_in_movex,"
					+ "is_item_in_intuitive, " + "cat_no_on_label, item_status, item_type,division, site_rollup,"
					+ " product_name, product_name_on_the_label,legal_manufacturer, design_specification_owner, ip_owner_name,"
					+ "intg_mfg_11, intg_site_12, site_descr_integra_mfr_name, approved,fda_product_code, "
					+ "fda_class, submission_type, fda_submission_number,fda_submission_supplement_number, submission_status, "
					+ "submission_date,approval_date, approval_clearance_letter_filed_internally, permission_to_use_letter_filed_internally,k510_dt_if_used_to_bring_sku_to_market, "
					+ "fda_medical_device_listing_number,gmdn_codes, comments, item_in_gudid, model_version, "
					+ "cat_no, labeler_duns_in_gudid,fda_product_codes, device_exempt_from_premarket_submission_requirements,fda_premarket_submission_numbers, "
					+ "fda_submission_supplement_numbers,fda_medical_device_listing_number_dl, gmdn_codes1, "
					+ "dl_in_furls_based_on_submission_in_gudid,product_code_in_furls_based_on_submission_in_gudid,"
					+ " submission__exception_in_furls_based_on_dl_in_gudid,product_code_in_furls_based_on_dl_in_gudid_, device_class_from_master_510kpma_log_based_on_submission_in_gud,ils_510k_20, ils_fda_21, "
					+ "ils_dev_6, dl_in_furls_based_on_submission_in_oracle,product_code_in_furls_based_on_submission_in_oracle, submission_exception_in_furls_based_on_dl_in_oracle1,product_code_in_furls_based_on_dl_in_oracle, "
					+ "device_class_from_master_510kpma_log_based_on_submission,fda_class1, fda_product_code1, fda_regulatory_status, dl_in_furls_based_on_submission_in_oracle_,"
					+ "submission_exception_in_furls_based_on_dl_in_oracle, device_class_from_fda_public_pro_code_db_based_on_pro_code_in_a,make_buy, ils_est_reg_22, ils_reg_address, "
					+ "vendor_510k_old_hts_descr_previous,vend_est_reg_25, vend_dev_23, gmdn_28,status,"
					+ "fda_pro_code_oracle,submission_number_oracle,device_listing_number_oracle,action,updated_by,distribution_status,item_exclusion_flag,invoice_flag,user_status) VALUES "
					+ "(?,?,?,?,?,?,?,?,?,?,?,?,?,?, ?, ?,?, ?, ?, ?,?, ?, ?,?, ?, ?, ?,?, ?, ?, ?,?, ?, ?,?, ?, ?,?, ?,?, ?, ?, ?, ?, ?,?, ?,?, ?,?, ?, ?,?, ?,?, ?,?,?, ?, ?, ?,?, ?,?, ?,?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?,?,?)";
			try (Connection conn = Util.getPostGresConnection(dbURL, dbUser, dbPwd);
					PreparedStatement oracleStmt = conn.prepareStatement(insertQuery);
					Statement stmt = conn.createStatement()) {

				stmt.execute("truncate temp_l1_source_consolidation");
				for (SourceConsolidation sourceConsolidation : sourceConsolidationList) {
					if (sourceConsolidation.getItemNo() != null) {
						oracleStmt.setString(1, sourceConsolidation.getItemNo());
						oracleStmt.setString(2, sourceConsolidation.getIsItemInOracle());
						oracleStmt.setString(3, sourceConsolidation.getIsItemInAgile());
						oracleStmt.setString(4, sourceConsolidation.getIsItemInMmovex());
						oracleStmt.setString(5, sourceConsolidation.getIsItemInIntuitive());
						oracleStmt.setString(6, sourceConsolidation.getCatNOnLabel());
						oracleStmt.setString(7, sourceConsolidation.getItemStatus());
						oracleStmt.setString(8, sourceConsolidation.getItemType());
						oracleStmt.setString(9, sourceConsolidation.getDivision());
						oracleStmt.setString(10, sourceConsolidation.getSiteRollup());
						oracleStmt.setString(11, sourceConsolidation.getProductName());
						oracleStmt.setString(12, sourceConsolidation.getProductNameOnTheLabel());
						oracleStmt.setString(13, sourceConsolidation.getLegalManufacturer());
						oracleStmt.setString(14, sourceConsolidation.getDesignSpecificatinOwner());
						oracleStmt.setString(15, sourceConsolidation.getIpOwnerName());
						oracleStmt.setString(16, sourceConsolidation.getIntgMfg());
						oracleStmt.setString(17, sourceConsolidation.getIntgMfgName());
						oracleStmt.setString(18, sourceConsolidation.getSiteDescrIntegraMfrName());
						oracleStmt.setString(19, sourceConsolidation.getApprovedG());
						oracleStmt.setString(20, sourceConsolidation.getFdaProductCodeCalculatedG());
						oracleStmt.setString(21, sourceConsolidation.getFdaClassG());
						oracleStmt.setString(22, sourceConsolidation.getSubmissionTypeG());
						oracleStmt.setString(23, sourceConsolidation.getFdaSubmissionNumberG());
						oracleStmt.setString(24, sourceConsolidation.getFdaSubmissionSupplementNumberG());
						oracleStmt.setString(25, sourceConsolidation.getSubmissionStatusG());
						oracleStmt.setString(26, sourceConsolidation.getSubmissionDateG());
						oracleStmt.setString(27, sourceConsolidation.getApprovalDateG());
						oracleStmt.setString(28, sourceConsolidation.getApprovalClearanceLetterFiledInternallyG());
						oracleStmt.setString(29, sourceConsolidation.getPermissionToUseLetterFiledInternallyG());
						oracleStmt.setString(30, sourceConsolidation.getK510dtIfUsedToBringSkuToMarketG());
						oracleStmt.setString(31, sourceConsolidation.getFdaMedicalDeviceListingNumberG());
						oracleStmt.setString(32, sourceConsolidation.getGmdnCodesG());
						oracleStmt.setString(33, sourceConsolidation.getCommentsG());

						oracleStmt.setString(34, sourceConsolidation.getItemInGudid());
						oracleStmt.setString(35, sourceConsolidation.getModelVersion());
						oracleStmt.setString(36, sourceConsolidation.getCatNo());
						oracleStmt.setString(37, sourceConsolidation.getLabeleDunsInGudid());
						oracleStmt.setString(38, sourceConsolidation.getFdaProductCodesInGudid());
						oracleStmt.setString(39,
								sourceConsolidation.getDeviceExemptFromPremarkeSubmissionRequirementsGudid());
						oracleStmt.setString(40, sourceConsolidation.getFdaPremarketSubmissionNumbersGudid());
						oracleStmt.setString(41, sourceConsolidation.getFdaSubmissionSupplementNumberGudid());
						oracleStmt.setString(42, sourceConsolidation.getFdaMedicalDeviceListingNumberDlGudid());
						oracleStmt.setString(43, sourceConsolidation.getGmdnCodesGudid());
						oracleStmt.setString(44, sourceConsolidation.getDlInFurlsBasedOnSubmissionInGudid());
						oracleStmt.setString(45, sourceConsolidation.getProductCodeInFurlsBasedOnSubmissionInGudid());
						oracleStmt.setString(46, sourceConsolidation.getSubmissionExceptionInFurlsBasedOnDlInGudid());
						oracleStmt.setString(47, sourceConsolidation.getProductCodeInFurlsBasedOnDlInGudid());
						oracleStmt.setString(48,
								sourceConsolidation.getDeviceClassrFomMaster510kpmaLogBasedOnSubmissionInGudid());
						oracleStmt.setString(49, sourceConsolidation.getIls510k20());
						oracleStmt.setString(50, sourceConsolidation.getIlsFda21());
						oracleStmt.setString(51, sourceConsolidation.getIlsDev6());
						oracleStmt.setString(52, sourceConsolidation.getDlInFurlsBasedOnSubmissionInOracle());
						oracleStmt.setString(53, sourceConsolidation.getProductCodeInFurlsBasedOnSubmissionInOracle());
						oracleStmt.setString(54, sourceConsolidation.getSubmissionExceptionInFurlsBasedOnDlInOracle());
						oracleStmt.setString(55, sourceConsolidation.getProductCodeInFurlsBasedOnDlInOracle());

						oracleStmt.setString(56,
								sourceConsolidation.getDeviceClassFromMaster510kpmaLogBasedOnSubmissionOracle());
						oracleStmt.setString(57, sourceConsolidation.getFdaClassAgile());
						oracleStmt.setString(58, sourceConsolidation.getFdaProductCodeInAgile());
						oracleStmt.setString(59, sourceConsolidation.getFdaRegulatoryStatus());
						oracleStmt.setString(60, sourceConsolidation.getDLNoInFURLSBasedOnProcodeInAgile());
						oracleStmt.setString(61,
								sourceConsolidation.getSubmissionNoExceptionInFURLSBasedOnProcodeInAgile());
						oracleStmt.setString(62,
								sourceConsolidation.getDeviceClassFromFdaPublicProCodeDbBasedOnProCodeAgile());
						oracleStmt.setString(63, sourceConsolidation.getMakeBuy());
						oracleStmt.setString(64, sourceConsolidation.getIlsEstReg_22());
						oracleStmt.setString(65, sourceConsolidation.getIlsRegAddress());
						oracleStmt.setString(66, sourceConsolidation.getVendor510kOldHtsDescrPrevious());
						oracleStmt.setString(67, sourceConsolidation.getVendEstReg25());
						oracleStmt.setString(68, sourceConsolidation.getVendDev23());
						oracleStmt.setString(69, sourceConsolidation.getGmdn28());
						oracleStmt.setString(70, sourceConsolidation.getStatus());
						oracleStmt.setString(71, sourceConsolidation.getFdaProCodeOracle());
						oracleStmt.setString(72, sourceConsolidation.getSubmissionNoOracle());
						oracleStmt.setString(73, sourceConsolidation.getDeviceListingNoOracle());
						oracleStmt.setString(74, sourceConsolidation.getAction());
						oracleStmt.setString(75, sourceConsolidation.getUpdated_by());
						oracleStmt.setString(76, sourceConsolidation.getDistributionStatus());
						oracleStmt.setString(77, sourceConsolidation.getItemExclusionFlag());
						oracleStmt.setString(78, sourceConsolidation.getInvoiceFlag());
						oracleStmt.setString(79, sourceConsolidation.getUserStatus());

						oracleStmt.addBatch();
						counter++;
						if (counter % batchSize == 0) {
							oracleStmt.executeBatch();
							oracleStmt.clearBatch();
							System.out.println("rows inserted in consolidation " + counter);
							logger.info("rows inserted in consolidation " + batchSize);
						}
					}
				}
				oracleStmt.executeBatch();
				oracleStmt.clearBatch();
				stmt.execute("select consolidationupdate()");

			} catch (Exception e) {
				e.printStackTrace();
				logger.info(e);
			}

		}

	}

	private void setProperties(String file) {
		try {
			FileReader fr = new FileReader(file);
			properties = new Properties();
			properties.load(fr);
			dbURL = properties.getProperty("PGURL");
			dbUser = properties.getProperty("PGUSER");
			dbPwd = properties.getProperty("PGPASS");
			version = Integer.parseInt(properties.getProperty("version"));
		} catch (Exception e) {
			e.printStackTrace();
			logger.info(e);
		}

	}

	public void deltaUpdate() {
		/*
		 * String gudidLocation = null; try { FileReader fr = new
		 * FileReader("config.properties"); Properties properties = new Properties();
		 * properties.load(fr); gudidLocation = properties.getProperty("GUDID");
		 * 
		 * } catch (Exception e) { e.printStackTrace();
		 * 
		 * }
		 * 
		 * File folder = new File(gudidLocation); File[] listOfFiles =
		 * folder.listFiles();
		 * 
		 * for (File file : listOfFiles) { if (file.isFile()) {
		 * com.relevancelab.ra.GUDID.App.processXML(file.getPath()); } }
		 */
		System.out.println("processing gudid");
		DeltaUpdate.GudidUpadte();
		System.out.println("processing furls");
		DeltaUpdate.FurlsUpdate();
		System.out.println("processing agile");
		DeltaUpdate.AgileUpdate();
		System.out.println("processing mapping");
		// mapping list -- x-ref table
		DeltaUpdate.MappingListUpdate();
		// DeltaUpdate.refreshInvoice();
		// DeltaUpdate.OracleDeltaUpdate();//loadind data into the l0 level table and
		// delta update both happens
	}

	public static void main(String[] args) throws Exception {
		RaConsolidation raConsolidation = new RaConsolidation();
		raConsolidation.deltaUpdate();
		raConsolidation.setProperties("config.properties");
		if (version > 0)
			raConsolidation.sourceComparisonResults(0);
		raConsolidation.buildRaConsolidationTable();
		// to update for item no which already exits rule has been created to
		// update the row with latest updated values.
		// delta function is used to update the values changed by the user.
		raConsolidation.deltaRefresh();
		raConsolidation.addingRaConsolidationtoDb();
		raConsolidation.sourceComparisonResults(1);

	}
}