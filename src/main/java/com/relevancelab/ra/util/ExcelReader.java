package com.relevancelab.ra.util;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

public class ExcelReader {
	PreparedStatement preparedStatement = null;
	int counter = 0;
	static final int BATCHSIZE = 5000;

	public void readXLFile(Connection conn, final String path, String table) throws SQLException, IOException {
		// XSSFWorkbook workBook = null;
		Workbook workBook = null;
		// File files=new File("D:/integra data/test");

		try {
			ArrayList<String> rowList = null;
			String cellValue = null;
			File files = new File(path);

			if (files.exists() && files.isDirectory()) {

				for (File file : files.listFiles()) {
					TableInfo tableInfo = createPraparedStatement(conn, table);
					this.preparedStatement = conn.prepareStatement(tableInfo.getPreparedStatement());
					workBook = WorkbookFactory.create(new FileInputStream(file));
					DataFormatter dataFormatter = new DataFormatter();
					for (Sheet sheet : workBook) {
						int rowStart = Math.min(10, sheet.getFirstRowNum())+1;
						int rowEnd = Math.max(1, sheet.getLastRowNum());
						for (int rowNum = rowStart; rowNum <= rowEnd; rowNum++) {
							Row r = sheet.getRow(rowNum);
							if (r == null)
								continue;
							int lastColumn = Math.max(r.getLastCellNum(), 9);
							rowList = new ArrayList<>();
							for (int cn = 0; cn < lastColumn; cn++) {
									Cell c = r.getCell(cn, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL);
								if(c==null)
									cellValue=null;
								else
									cellValue = dataFormatter.formatCellValue(c).trim();
								rowList.add(cellValue);
							}
							insertIntoDb(tableInfo, rowList);
						}
					}
				}
			}
		} catch (EncryptedDocumentException | InvalidFormatException | IOException e) { // InvalidFormatException

			e.printStackTrace();
		} finally {
			if (workBook != null)
				workBook.close();
			if (preparedStatement != null) {
				preparedStatement.executeBatch();
				preparedStatement.clearBatch();

			}
		}

	}

	public void insertIntoDb(TableInfo table, ArrayList<String> rowList) {
		try {
			for (int i = 1; i <= table.getLength(); i++) {
				this.preparedStatement.setString(i, rowList.get(i - 1));
			}
			preparedStatement.addBatch();
			counter++;
			//System.out.println(counter);
			if (counter % BATCHSIZE == 0) {
				preparedStatement.executeBatch();
				preparedStatement.clearBatch();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static TableInfo createPraparedStatement(Connection conn, String table) throws SQLException {
		StringBuilder preparedStatementQuery = new StringBuilder();
		preparedStatementQuery.append("insert into ").append(table).append(" values").append("(");
		TableInfo tableInfo = new TableInfo();

		try (Statement statement = conn.createStatement();
				ResultSet rs = statement.executeQuery("select * from " + table + " limit 0");) {

			final int columnCount = rs.getMetaData().getColumnCount();
			tableInfo.setLength(columnCount);
			int i = 0;
			while (i < columnCount) {
				if ((columnCount - 1) == i)
					preparedStatementQuery.append("?)");
				else
					preparedStatementQuery.append("?,");
				i++;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		tableInfo.setPreparedStatement(preparedStatementQuery.toString());
		tableInfo.setName(table);

		return tableInfo;
	}

	public static void main(String[] args) {
		ExcelReader excelReader = new ExcelReader();
		FileReader fr = null;
		Connection conn = null;
		try {
			fr = new FileReader(new File("config.properties"));
			Properties properties = new Properties();
			properties.load(fr);
			String dbURL = properties.getProperty("PGURL");
			String dbUser = properties.getProperty("PGUSER");
			String dbPwd = properties.getProperty("PGPASS");
			String path = properties.getProperty("FURLACTIVE");
			conn = Util.getPostGresConnection(dbURL, dbUser, dbPwd);
			excelReader.readXLFile(conn, path, "l1_furls_test");// taking connection of the database to store th

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (conn != null)
				try {
					conn.close();
					fr.close();
				} catch (SQLException | IOException e) {
					e.printStackTrace();
				}

		}

	}
}
