package com.relevancelab.ra.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;


import com.relevancelab.ra.data.JobDetails;


public class Util {
	

	public static List<String> sortHashSet(Set<String> hashSet) {
		if (hashSet != null && !hashSet.isEmpty()) {

			List<String> sortList = new ArrayList<>(hashSet);
			sortList.remove(null);
			Collections.sort(sortList);
			return sortList;
		} else
			return null;
	}
public static Connection getMsSqlSever(String host,String userName,String password) {
	Connection conn=null;
	try {
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		DriverManager.setLoginTimeout(10000);
		conn=DriverManager.getConnection("jdbc:sqlserver://US311EDW004:1433;database=DimensionDB;username=DataStage;password=D4ta$tag3!");
	}catch(Exception  e) {
		e.printStackTrace();
	}
	if(conn!=null)
	System.out.println("Connection Successesful");
return conn;	
}
	public static Connection getOracleDBConnetion(String host, String userName, String password) throws Exception {
		System.out.println("inside getOracleDBConnetion() method...");
		Connection conn = null;
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			conn = DriverManager.getConnection(host, userName, password);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (conn != null)
			System.out.println("Got Oracle connection !!");

		return conn;
	}

	public static Connection getPostGresConnection(String dbURL, String username, String password) throws Exception {

		Connection connection = null;
		try {
			Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection(dbURL, username, password);

		} catch (Exception ee) {
			System.out.println("Exception in getting data from batch table " + ee.getMessage());
			System.out.println("Exception" + ee.getMessage());
			System.out.println("Exception in detail" + ee);
			ee.printStackTrace();
			System.out.println(ee.getMessage());
			throw ee;
		}

		if (connection != null) {
			System.out.println("You made it, take control of your Postgress database now!");
		} else {
			System.out.println("Failed to make connection!");
		}

		return connection;
	}

	public static void updateJobDetails(JobDetails jobDetails) throws SQLException {
		String jobDate;
		java.util.Date createdDate;
		DateFormat dateFormat;
		StringBuilder jobDetailsQuery = new StringBuilder();
		StringBuilder jobHistoryUpdateQuery = new StringBuilder();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		int jobID = 0;
		String status = "Failed";
		String project;
		String job;
		int totalCount;
		int deltaCount;
		boolean isStatus;
		long startTime;
		String dbURL;
		String dbUserName;
		String dbPassword;
		try {
			project = jobDetails.getProject();
			job = jobDetails.getJob();
			totalCount = jobDetails.getTotalCount();
			deltaCount = jobDetails.getDeltaCount();
			isStatus = jobDetails.isStatus();
			startTime = jobDetails.getStartTime();
			dbURL = jobDetails.getDbURL();
			dbUserName = jobDetails.getDbUserName();
			dbPassword = jobDetails.getDbPassword();
			if (isStatus)
				status = "Successful";

			conn = getPostGresConnection(dbURL, dbUserName, dbPassword);
			jobDetailsQuery.append("SELECT id from job_details where category_name = '").append(project)
					.append("' and title ='").append(job).append("'");
			if (conn == null)
				return;
			stmt = conn.createStatement();
			rs = stmt.executeQuery(jobDetailsQuery.toString());
			while (rs.next()) {
				jobID = rs.getInt(1);
			}
			createdDate = new java.util.Date();
			dateFormat = new SimpleDateFormat("YYYY-MM-dd");
			jobDate = dateFormat.format(createdDate);
			long endTime = System.currentTimeMillis();
			int totalTime = (int) ((endTime - startTime) / 1000);
			jobHistoryUpdateQuery.append("UPDATE job_details SET docs_in_repo = ").append(totalCount)
					.append(", delta_no= ").append(deltaCount).append(", refresh_date= '").append(jobDate)
					.append("', refresh_duration= ").append(totalTime).append(", refresh_status='").append(status)
					.append("' WHERE id = ").append(jobID);
			stmt.executeUpdate(jobHistoryUpdateQuery.toString());
			updateJobHistroy(jobID, totalTime, jobDetails);
		} catch (Exception e) {

		} finally {
			if (conn != null)
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			if (stmt != null)
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
	}

	private static void updateJobHistroy(int jobID, int totalTime, JobDetails jobDetails) throws Exception {
		PreparedStatement pstmt = null;
		Connection conn = null;
		int deltaCount;
		boolean isStatus;
		String errorType;
		String errorLog;
		String dbURL;
		String dbUserName;
		String dbPassword;
		String status = "";
		try {
			deltaCount = jobDetails.getDeltaCount();
			isStatus = jobDetails.isStatus();
			errorType = jobDetails.getErrorType();
			errorLog = jobDetails.getErrorLog();
			dbURL = jobDetails.getDbURL();
			dbUserName = jobDetails.getDbUserName();
			dbPassword = jobDetails.getDbPassword();
			if (isStatus)
				status = "Successful";
			conn = getPostGresConnection(dbURL, dbUserName, dbPassword);
			if (conn == null)
				return;
			pstmt = conn.prepareStatement(
					"INSERT INTO job_history(job_id, records_processed, duration, status,error_type,error_log)VALUES (?, ?, ?, ?, ?, ?);");
			pstmt.setInt(1, jobID);
			pstmt.setInt(2, deltaCount);
			pstmt.setInt(3, totalTime);
			pstmt.setString(4, status);
			pstmt.setString(5, errorType);
			pstmt.setString(6, errorLog);
			pstmt.execute();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (conn != null)
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			if (pstmt != null)
				try {
					pstmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
	}

	public static String intToRoman(int num) {
		StringBuilder roman = new StringBuilder();
		String m = "M";
		//100,200,---900
		String[] c = { "", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM" };
		//10--90
		String[] x= { "", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC" };
		//0-9
		String[] i = { "", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX" };
		
			if (num / 1000 > 0) {
				for (int p = 1; p <= num / 1000; p++)
					roman.append(m);
			}
			num = num % 1000;

			String hundereds = c[(num % 1000) / 100];
			String tens = x[(num % 100) / 10];
			String ones = i[num % 10];
			roman.append(hundereds).append(tens).append(ones);
				return roman.toString();
	}
public static void main(String[] args){
	System.out.println(intToRoman(3549));
	
}
}


