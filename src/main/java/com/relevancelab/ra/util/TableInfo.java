package com.relevancelab.ra.util;

import java.sql.PreparedStatement;

public class TableInfo {
	private String name;
	private int length;
	private String preparedStatement;

	public String getPreparedStatement() {
		return preparedStatement;
	}

	public void setPreparedStatement(String preparedStatement) {
		this.preparedStatement = preparedStatement;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
