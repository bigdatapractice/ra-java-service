package com.relevancelab.ra;

import java.util.Set;
import java.util.TreeSet;

class A
{
	public void inA()
	{
		System.out.println("Class A");
	}
}

class B extends A
{
	public void inA()
	{
		System.out.println("Class B");
	}
	
}

enum Name 
{
Alice, Smith, Bob, Jones
}

public class Examp2 {

	
	
	public static void main(String []aa)
	{
		Set<Name> obj = new TreeSet<Name>();
		   obj.add(Name.Alice);
		   obj.add(Name.Smith);
		   obj.add(Name.Bob);
		   obj.add(Name.Smith);
		    for(Name d: obj)
			{
		    System.out.println(d);
		    }
}
}