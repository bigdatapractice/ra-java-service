package com.relevancelab.ra.data;

public class FdaProCode {
	private String productcode;
	private String deviceclass;

	public String getProductcode() {
		return productcode;
	}

	public void setProductcode(String productcode) {
		this.productcode = productcode;
	}

	public String getDeviceclass() {
		return deviceclass;
	}

	public void setDeviceclass(String deviceclass) {
		this.deviceclass = deviceclass;
	}

}
