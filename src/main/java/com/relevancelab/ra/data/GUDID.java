package com.relevancelab.ra.data;

import java.util.Set;

public class GUDID {

	private String itemNo;
	private String catalogNo;
	private String labelerDUNSNo;
	private String labelerDUNSDesc;
	private Set<String> fdaProductCodes;
	private Set<String> deviceExempt;
	private Set<String> fdaPremarketSubmissionNo;
	private String fdaSubmissionSupplementNo;
	private Set<String> fdaMedicalDeviceListingNo;
	private Set<String> gmdnCodes;
	private Set<String>city;
	private String company;
	private String distributionStatus;
	

	public String getDistributionStatus() {
		return distributionStatus;
	}

	public void setDistributionStatus(String distributionStatus) {
		this.distributionStatus = distributionStatus;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	
	public Set<String> getCity() {
		return city;
	}

	public void setCity(Set<String> city) {
		this.city = city;
	}

	public String getItemNo() {
		return itemNo;
	}

	public void setItemNo(String itemNo) {
		this.itemNo = itemNo;
	}

	public String getCatalogNo() {
		return catalogNo;
	}

	public void setCatalogNo(String catalogNo) {
		this.catalogNo = catalogNo;
	}

	public Set<String> getFdaProductCodes() {
		return fdaProductCodes;
	}

	public void setFdaProductCodes(Set<String> fdaProductCodes) {
		this.fdaProductCodes = fdaProductCodes;
	}

	public Set<String> getDeviceExempt() {
		return deviceExempt;
	}

	public void setDeviceExempt(Set<String> deviceExempt) {
		this.deviceExempt = deviceExempt;
	}

	public Set<String> getFdaPremarketSubmissionNo() {
		return fdaPremarketSubmissionNo;
	}

	public void setFdaPremarketSubmissionNo(Set<String> fdaPremarketSubmissionNo) {
		this.fdaPremarketSubmissionNo = fdaPremarketSubmissionNo;
	}

	public String getFdaSubmissionSupplementNo() {
		return fdaSubmissionSupplementNo;
	}

	public void setFdaSubmissionSupplementNo(String fdaSubmissionSupplementNo) {
		this.fdaSubmissionSupplementNo = fdaSubmissionSupplementNo;
	}

	public Set<String> getFdaMedicalDeviceListingNo() {
		return fdaMedicalDeviceListingNo;
	}

	public void setFdaMedicalDeviceListingNo(Set<String> fdaMedicalDeviceListingNo) {
		this.fdaMedicalDeviceListingNo = fdaMedicalDeviceListingNo;
	}

	public Set<String> getGmdnCodes() {
		return gmdnCodes;
	}

	public void setGmdnCodes(Set<String> gmdnCodes) {
		this.gmdnCodes = gmdnCodes;
	}

	public String getLabelerDUNSNo() {
		return labelerDUNSNo;
	}

	public void setLabelerDUNSNo(String labelerDUNSNo) {
		this.labelerDUNSNo = labelerDUNSNo;
	}

	public String getLabelerDUNSDesc() {
		return labelerDUNSDesc;
	}

	public void setLabelerDUNSDesc(String labelerDUNSDesc) {
		this.labelerDUNSDesc = labelerDUNSDesc;
	}
}