package com.relevancelab.ra.data;

public class ItemMapping {
	private String itemNo;
	private String itemNoOracle;
	private String itemNoAgile;
	private String itemInGudid;
	private String versionModelNumberGudid;
	private String catalogNumberGudid;
	private String prodcuctNameOnLabel;
	private String legalalMgfOnLabel;
	private String division;
	private String siteRollup;

	public String getProdcuctNameOnLabel() {
		return prodcuctNameOnLabel;
	}

	public void setProdcuctNameOnLabel(String prodcuctNameOnLabel) {
		this.prodcuctNameOnLabel = prodcuctNameOnLabel;
	}

	public String getLegalalMgfOnLabel() {
		return legalalMgfOnLabel;
	}

	public void setLegalalMgfOnLabel(String legalalMgfOnLabel) {
		this.legalalMgfOnLabel = legalalMgfOnLabel;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getSiteRollup() {
		return siteRollup;
	}

	public void setSiteRollup(String siteRollup) {
		this.siteRollup = siteRollup;
	}

	public String getItemNo() {
		return itemNo;
	}

	public void setItemNo(String itemNo) {
		this.itemNo = itemNo;
	}

	public String getItemNoOracle() {
		return itemNoOracle;
	}

	public void setItemNoOracle(String itemNoOracle) {
		this.itemNoOracle = itemNoOracle;
	}

	public String getItemNoAgile() {
		return itemNoAgile;
	}

	public void setItemNoAgile(String itemNoAgile) {
		this.itemNoAgile = itemNoAgile;
	}

	public String getItemInGudid() {
		return itemInGudid;
	}

	public void setItemInGudid(String itemInGudid) {
		this.itemInGudid = itemInGudid;
	}

	public String getVersionModelNumberGudid() {
		return versionModelNumberGudid;
	}

	public void setVersionModelNumberGudid(String versionModelNumberGudid) {
		this.versionModelNumberGudid = versionModelNumberGudid;
	}

	public String getCatalogNumberGudid() {
		return catalogNumberGudid;
	}

	public void setCatalogNumberGudid(String catalogNumberGudid) {
		this.catalogNumberGudid = catalogNumberGudid;
	}

}
