package com.relevancelab.ra.data;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import com.relevancelab.ra.util.Util;

public class SourceConsolidation {
	private String itemNo;
	private String isItemInOracle;
	private String isItemInAgile;
	private String isItemInMmovex;
	private String isItemInIntuitive;
	private String catNOnLabel;
	private String itemStatus;
	private String itemType;
	private String division;
	private String siteRollup;
	private String productName;
	private String productNameOnTheLabel;
	private String legalManufacturer;
	private String designSpecificatinOwner;
	private String ipOwnerName;
	private String intgMfg;
	private String intgMfgName;
	private String siteDescrIntegraMfrName;

	// green section
	private String approvedG;
	private String fdaProductCodeCalculatedG;
	private String fdaClassG;
	private String submissionTypeG;
	private String fdaSubmissionNumberG;
	private String fdaSubmissionSupplementNumberG;
	private String submissionStatusG;
	private String submissionDateG;
	private String approvalDateG;
	private String approvalClearanceLetterFiledInternallyG;
	private String permissionToUseLetterFiledInternallyG;
	private String k510dtIfUsedToBringSkuToMarketG;
	private String fdaMedicalDeviceListingNumberG;
	private String gmdnCodesG;
	private String commentsG;

	// gudid
	private String itemInGudid;
	private String modelVersion;
	private String catNo;
	private String distributionStatus;
	private String labeleDunsInGudid;
	private String fdaProductCodesInGudid;
	private String deviceExemptFromPremarkeSubmissionRequirementsGudid;
	private String fdaPremarketSubmissionNumbersGudid;
	private String fdaSubmissionSupplementNumberGudid;
	private String fdaMedicalDeviceListingNumberGudid;
	private String gmdnCodesGudid;

	// furls gudid
	private String dlInFurlsBasedOnSubmissionInGudid;
	private String productCodeInFurlsBasedOnSubmissionInGudid;
	private String submissionExceptionInFurlsBasedOnDlInGudid;
	private String productCodeInFurlsBasedOnDlInGudid;

	// device class
	private String deviceClassrFomMaster510kpmaLogBasedOnSubmissionInGudid;

	// oracle
	private String ils510k20;
	private String ilsFda21;
	private String ilsDev6;

	// furls oracle
	private String dlInFurlsBasedOnSubmissionInOracle;
	private String productCodeInFurlsBasedOnSubmissionInOracle;
	private String submissionExceptionInFurlsBasedOnDlInOracle;
	private String productCodeInFurlsBasedOnDlInOracle;

	// device class oracle
	private String deviceClassFromMaster510kpmaLogBasedOnSubmissionNoOracle;

	// agile
	private String fdaClassAgile;
	private String fdaProductCodeInAgile;
	private String fdaRegulatoryStatus;

	// furls agile
	private String DLNoInFURLSBasedOnProcodeInAgile;
	private String submissionNoExceptionInFURLSBasedOnProcodeInAgile;

	// device class agile
	private String deviceClassFromFdaPublicProCodeDbBasedOnProCodeAgile;

	// oracle
	private String makeBuy;
	private String ilsEstReg_22;
	private String ilsRegAddress;
	private String vendor510kOldHtsDescrPrevious;
	private String vendEstReg25;
	private String vendDev23;
	private String gmdn28;
	private String submissionTypeInFurls;
	private String fdaProCodeOracle;
	private String deviceListingNoOracle;
	private String submissionNoOracle;
	private String status;
	private String action;
	private String userStatus;
	private String updatedBy;
	private String updatedOn;
	private String itemExclusionFlag;
	private String invoiceFlag;

	public String getItemExclusionFlag() {
		return itemExclusionFlag;
	}

	public void setItemExclusionFlag(String itemExclusionFlag) {
		this.itemExclusionFlag = itemExclusionFlag;
	}

	//
	// private static final String removeSC = "[^a-zA-Z0-9]";
	private static final String separator = ",";
	// private static final String itemNotInGUDID = "Item not in GUDID";
	private static final String itemNotInOracle = "Item not in ORAClE";
	private static final String itemNotInAgile = "Item not in AGILE";
	private static final String itemNotInLabel = "Item not in LABEL";
	private static final String itemNotInOracleAgile = "Item not in ORACLE or AGILE";
	private static final String itemNotInGudid = "Item not in GUDID";

	// private static final String error = "Error";
	private static final String submissionNoPattern = "(([a-zA-z]+)?([0-9]+))";
	private static final String preamendment = "Preamendment";
	private static final String enforcementDiscretion = "Enforcement Discretion";
	private static final String exempt = "Exempt";
	private static final String notExempt = "Not Exempt";
	private static final String k510 = "510(k)";
	private static final String pma = "PMA";
	private static final String na = "N/A";
	private static final String match = "Matched";
	private static final String mismatch = "Flagged";
	private static final String OPEN = "OPEN";
	private static final String CLOSE = "NO ACTION";
	private static final String FLAG = "X";
	private static final String NOTINFURLS = "Not Found In FURLS";
	private static final String notReq = "Not Required";
	private static final String[] subMissionTypeList = { exempt.toLowerCase(), preamendment.toLowerCase(),
			enforcementDiscretion.toLowerCase() };
	private static final Set<String> subTypeList = new HashSet<>(Arrays.asList(subMissionTypeList));

	public String getDistributionStatus() {
		return distributionStatus;
	}

	public void setDistributionStatus(GUDID itemInGUDID) {
		distributionStatus = itemInGUDID != null ? itemInGUDID.getDistributionStatus() : itemNotInGudid;
	}

	public String getItemNo() {
		return itemNo;
	}

	public void setItemNo(String itemNo) {
		this.itemNo = itemNo;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public void setSubmissionStatusG(String submissionStatusG) {
		this.submissionStatusG = submissionStatusG;
	}

	public void setCatNOnLabel(ItemMapping itemMapping) {
		this.catNOnLabel = itemMapping.getCatalogNumberGudid();
	}

	public void setCaNOnLabel(String catNOnLabel) {
		this.catNOnLabel = catNOnLabel;
	}

	public void setItemStatus(String itemStatus) {
		this.itemStatus = itemStatus;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public void setProductNameOnTheLabel(String productNameOnTheLabel) {
		this.productNameOnTheLabel = productNameOnTheLabel;
	}

	public void setLegalManufacturer(String legalManufacturer) {
		this.legalManufacturer = legalManufacturer;
	}

	public void setFdaSubmissionSupplementNumberG(String fdaSubmissionSupplementNumberG) {
		this.fdaSubmissionSupplementNumberG = fdaSubmissionSupplementNumberG;
	}

	public void setSubmissionDateG(String submissionDateG) {
		this.submissionDateG = submissionDateG;
	}

	public void setApprovalDateG(String approvalDateG) {
		this.approvalDateG = approvalDateG;
	}

	public void setFdaMedicalDeviceListingNumberG(String fdaMedicalDeviceListingNumberG) {
		this.fdaMedicalDeviceListingNumberG = fdaMedicalDeviceListingNumberG;
	}

	public void setGmdnCodesG(String gmdnCodesG) {
		this.gmdnCodesG = gmdnCodesG;
	}

	public void setIls510k20(String ils510k20) {
		this.ils510k20 = ils510k20;
	}

	public void setFdaSubmissionNumberG(String fdaSubmissionNumberG) {
		this.fdaSubmissionNumberG = fdaSubmissionNumberG;
	}

	public void setSubmissionTypeG(String submissionTypeG) {
		this.submissionTypeG = submissionTypeG;
	}

	public void setFdaClassG(String fdaClassG) {
		this.fdaClassG = fdaClassG;
	}

	public String getIsItemInOracle() {

		return isItemInOracle;
	}

	public void setIsItemInOracle(OracleMaster itemInOracle) {
		if (itemInOracle != null) {
			isItemInOracle = "Yes";

		} else {
			isItemInOracle = "No";

		}

	}

	public String getIsItemInAgile() {
		return isItemInAgile;
	}

	public void setIsItemInAgile(Agile itemInAgile) {
		if (itemInAgile != null) {
			isItemInAgile = "Yes";
		} else {
			isItemInAgile = "No";
		}

	}

	public String getIsItemInMmovex() {
		return isItemInMmovex;
	}

	public void setIsItemInMmovex(String isItemInMmovex) {
		this.isItemInMmovex = isItemInMmovex;
	}

	public String getIsItemInIntuitive() {
		return isItemInIntuitive;
	}

	public void setIsItemInIntuitive(String isItemInIntuitive) {
		this.isItemInIntuitive = isItemInIntuitive;
	}

	public String getCatNOnLabel() {
		return catNOnLabel;
	}

	public void setCatNOnLabel(Label label) {
		if (label != null)
			catNOnLabel = label.getItemNo();
		else
			catNOnLabel = itemNotInLabel;
	}

	public String getItemStatus() {
		return itemStatus;
	}

	public void setItemStatus(OracleMaster itemInOracle, Agile itemInAgile) {
		if (itemInOracle != null) {
			itemStatus = itemInOracle.getItemStatus();

		} else if (itemInAgile != null) {
			itemStatus = itemInAgile.getItemStatus();

		} else
			itemStatus = itemNotInOracleAgile;

	}

	public void setApprovedG(String approvedG) {
		this.approvedG = approvedG;
	}

	public String getItemType() {
		return itemType;
	}

	public void setFdaProductCodeCalculatedG(String fdaProductCodeCalculatedG) {
		this.fdaProductCodeCalculatedG = fdaProductCodeCalculatedG;
	}

	public void setItemType(OracleMaster itemInOracle, Agile itemInAgile) {
		if (itemInOracle != null) {
			itemType = itemInOracle.getItemType();

		} else if (itemInAgile != null) {
			itemType = itemInAgile.getItemType();

		} else
			itemType = itemNotInOracleAgile;

	}

	public String getDivision() {
		return division;
	}

	public void setDivision(ItemMapping itemMapping) {
		this.division = itemMapping.getDivision();
	}
	// get Site Rollup to be added

	public void setSiteRollup(ItemMapping itemMapping) {
		this.siteRollup = itemMapping.getSiteRollup();
	}

	public String getSiteRollup() {
		return siteRollup;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(OracleMaster itemInOracle, Agile itemInAgile) {
		if (itemInOracle != null) {
			productName = itemInOracle.getProdcutName();

		} else if (itemInAgile != null) {
			productName = itemInAgile.getProductName();

		} else
			productName = itemNotInOracleAgile;

	}

	public String getProductNameOnTheLabel() {
		return productNameOnTheLabel;
	}

	public void setProductNameOnTheLabel(ItemMapping itemMapping) {
		productNameOnTheLabel = itemMapping.getProdcuctNameOnLabel();

	}

	public String getLegalManufacturer() {
		return legalManufacturer;
	}

	public void setLegalManufacturer(ItemMapping itemMapping) {

		legalManufacturer = itemMapping.getLegalalMgfOnLabel();

	}

	public String getDesignSpecificatinOwner() {
		return designSpecificatinOwner;
	}

	public void setDesignSpecificatinOwner(Agile itemInAgile) {
		if (itemInAgile != null) {
			designSpecificatinOwner = itemInAgile.getDesignSpecificationOwner();
		} else
			designSpecificatinOwner = itemNotInAgile;

	}

	public String getIpOwnerName() {
		return ipOwnerName;
	}

	public void setIpOwnerName(OracleMaster itemInOracle) {
		if (itemInOracle != null)
			ipOwnerName = itemInOracle.getIpOwnerName();
		else
			ipOwnerName = itemNotInOracle;
	}

	public String getIntgMfg() {
		return intgMfg;
	}

	public void setIntgMfg(OracleMaster itemInOracle) {
		this.intgMfg = itemInOracle != null ? itemInOracle.getIntegraManufactured() : itemNotInOracle;
	}

	public String getIntgMfgName() {
		return intgMfgName;
	}

	public void setIntgMfgName(OracleMaster itemInOracle) {
		intgMfgName = itemInOracle != null ? itemInOracle.getIntegraManufacturerName() : itemNotInOracle;
	}

	public String getSiteDescrIntegraMfrName() {
		return siteDescrIntegraMfrName;
	}

	public void setSiteDescrIntegraMfrName(OracleMaster itemInOracle) {
		siteDescrIntegraMfrName = itemInOracle != null ? itemInOracle.getSiteDesc() : itemNotInOracle;
	}

	public String getItemInGudid() {
		return itemInGudid;
	}

	public void setItemInGudid(GUDID itemInGudid) {
		if (itemInGudid != null)
			this.itemInGudid = "Yes";
		else
			this.itemInGudid = "No";
	}

	public String getModelVersion() {
		return modelVersion;
	}

	public void setModelVersion(GUDID itemInGudid) {
		if (itemInGudid != null)
			modelVersion = itemInGudid.getItemNo();
		else
			modelVersion = itemNotInGudid;
	}

	public String getCatNo() {
		return catNo;
	}

	public void setCatNo(GUDID itemInGudid) {
		if (itemInGudid != null)
			catNo = itemInGudid.getCatalogNo();
		else
			catNo = itemNotInGudid;
	}

	public String getLabeleDunsInGudid() {
		return labeleDunsInGudid;
	}

	// gudid columns

	public void setLabeleDunsInGudid(GUDID itemInGudid) {
		if (itemInGudid != null) {
			String company = itemInGudid.getCompany() == null ? "" : itemInGudid.getCompany();
			Set<String> city = itemInGudid.getCity();
			city.remove(null);
			String cityVal = city.isEmpty() ? "" : StringUtils.join(city, separator);
			labeleDunsInGudid = company + " - " + cityVal;
		} else
			labeleDunsInGudid = itemNotInGudid;
	}

	public String getFdaProductCodesInGudid() {
		return fdaProductCodesInGudid;
	}

	public void setFdaProductCodesInGudid(GUDID itemInGudid) {
		if (itemInGudid != null)
			fdaProductCodesInGudid = StringUtils.join(itemInGudid.getFdaProductCodes(), separator);
		else
			fdaProductCodesInGudid = itemNotInGudid;
	}

	public String getDeviceExemptFromPremarkeSubmissionRequirementsGudid() {
		return deviceExemptFromPremarkeSubmissionRequirementsGudid;
	}

	// needs to be revisited
	public void setDeviceExemptFromPremarkeSubmissionRequirementsGudid(GUDID itemInGudid) {
		if (itemInGudid != null)
			deviceExemptFromPremarkeSubmissionRequirementsGudid = StringUtils.join(itemInGudid.getDeviceExempt(),
					separator);
		else
			deviceExemptFromPremarkeSubmissionRequirementsGudid = itemNotInGudid;

	}

	public String getFdaSubmissionSupplementNumberGudid() {
		return fdaSubmissionSupplementNumberGudid;
	}

	public void setFdaSubmissionSupplementNumberGudid(GUDID itemInGudid) {
		Set<String> supplementNo = new HashSet<>();
		if (itemInGudid != null) {

			Set<String> submissionNo = itemInGudid.getFdaPremarketSubmissionNo();
			submissionNo.remove(null);
			if (!submissionNo.isEmpty()) {
				for (String subno : submissionNo) {
					if (subno.trim().toLowerCase().matches("(p|h).*"))
						supplementNo.add(itemInGudid.getFdaSubmissionSupplementNo());
					else
						supplementNo.add(na);
				}
			} else
				supplementNo.add(na);

		} else
			supplementNo.add(itemNotInGudid);
		fdaSubmissionSupplementNumberGudid = StringUtils.join(supplementNo, separator);

	}

	public String getFdaPremarketSubmissionNumbersGudid() {
		return fdaPremarketSubmissionNumbersGudid;
	}

	public void setFdaPremarketSubmissionNumbersGudid(GUDID itemInGudid) {
		if (itemInGudid != null) {

			List<String> subNoList = Util.sortHashSet(itemInGudid.getFdaPremarketSubmissionNo());
			fdaPremarketSubmissionNumbersGudid = StringUtils.join(subNoList, separator);
		} else
			fdaPremarketSubmissionNumbersGudid = itemNotInGudid;
	}

	public String getFdaMedicalDeviceListingNumberDlGudid() {
		return fdaMedicalDeviceListingNumberGudid;
	}

	public void setFdaMedicalDeviceListingNumberDl(GUDID itemInGudid) {
		fdaMedicalDeviceListingNumberGudid = itemInGudid != null
				? StringUtils.join(itemInGudid.getFdaMedicalDeviceListingNo(), separator)
				: itemNotInGudid;
	}

	public String getGmdnCodesGudid() {
		return gmdnCodesGudid;
	}

	public void setGmdnCodesGudid(GUDID itemInGudid) {
		gmdnCodesGudid = itemInGudid != null ? StringUtils.join(itemInGudid.getGmdnCodes(), separator) : itemNotInGudid;
	}

	public String getDlInFurlsBasedOnSubmissionInGudid() {
		return dlInFurlsBasedOnSubmissionInGudid;
	}

	public void setDlInFurlsBasedOnSubmissionInGudid(GUDID itemInGUDID, Map<String, FURLS> furlsListBySN) {
		if (itemInGUDID != null) {
			Set<String> submissionNoGUDID = itemInGUDID.getFdaPremarketSubmissionNo();
			List<String> submissionNoGUDIDList = Util.sortHashSet(submissionNoGUDID);

			List<String> deviceListingNoList = new ArrayList<>();
			if (submissionNoGUDIDList != null && !submissionNoGUDIDList.isEmpty()) {
				for (String submissionNo : submissionNoGUDIDList) {
					FURLS furls = furlsListBySN.containsKey(submissionNo) ? furlsListBySN.get(submissionNo) : null;
					if (furls == null)
						deviceListingNoList.add(NOTINFURLS);
					else
						deviceListingNoList.addAll(furls.getDeviceListingNo());
				}
				dlInFurlsBasedOnSubmissionInGudid = StringUtils.join(deviceListingNoList, separator);
			} else
				dlInFurlsBasedOnSubmissionInGudid = na;
		} else
			dlInFurlsBasedOnSubmissionInGudid = itemNotInGudid;
	}

	public String getProductCodeInFurlsBasedOnSubmissionInGudid() {
		return productCodeInFurlsBasedOnSubmissionInGudid;
	}

	public void setProductCodeInFurlsBasedOnSubmissionInGudid(GUDID itemInGUDID, Map<String, FURLS> furlsListBySN) {
		if (itemInGUDID != null) {
			List<String> productCodeList = new ArrayList<>();
			Set<String> submissionNoGUDID = itemInGUDID.getFdaPremarketSubmissionNo();
			List<String> submissionNoGUDIDList = Util.sortHashSet(submissionNoGUDID);
			if (submissionNoGUDIDList != null && !submissionNoGUDIDList.isEmpty()) {
				for (String submissionNo : submissionNoGUDIDList) {
					FURLS furls = furlsListBySN.get(submissionNo);
					if (furls == null)
						productCodeList.add(NOTINFURLS);
					else
						productCodeList.addAll(Util.sortHashSet(furls.getProductCode()));

				}
				productCodeInFurlsBasedOnSubmissionInGudid = StringUtils.join(productCodeList, separator);
			} else
				productCodeInFurlsBasedOnSubmissionInGudid = na;
		} else
			productCodeInFurlsBasedOnSubmissionInGudid = itemNotInGudid;
	}

	public String getSubmissionExceptionInFurlsBasedOnDlInGudid() {
		return submissionExceptionInFurlsBasedOnDlInGudid;
	}

	public void setSubmissionExceptionInFurlsBasedOnDlInGudid(GUDID itemInGUDID, Map<String, FURLS> furlsListByDL) {
		if (itemInGUDID != null) {
			String subException = null;
			Set<String> submissionNoList = new HashSet<>();
			Set<String> listingNoSetGUDID = itemInGUDID.getFdaMedicalDeviceListingNo();
			List<String> listingNoSetGUDIDList = Util.sortHashSet(listingNoSetGUDID);
			if (listingNoSetGUDIDList != null && !listingNoSetGUDIDList.isEmpty()) {
				for (String listingNo : listingNoSetGUDIDList) {
					FURLS furls = furlsListByDL.get(listingNo);
					if (furls != null) {
						String submission = furls.getPreMarketSubmissionNo();
						String exception = furls.getPreMarketException();
						subException = submission != null ? submission : exception;
						if (subException == null || subException.isEmpty())
							subException = furls.getItemTypeFurls();
						submissionNoList.add(subException);
					} else
						submissionNoList.add(NOTINFURLS);
				}
				submissionExceptionInFurlsBasedOnDlInGudid = StringUtils.join(submissionNoList, separator);
			} else
				submissionExceptionInFurlsBasedOnDlInGudid = na;
		} else
			submissionExceptionInFurlsBasedOnDlInGudid = itemNotInGudid;
	}

	public String getProductCodeInFurlsBasedOnDlInGudid() {
		return productCodeInFurlsBasedOnDlInGudid;
	}

	public void setProductCodeInFurlsBasedOnDlInGudid(GUDID itemInGUDID, Map<String, FURLS> furlsListByDL) {
		if (itemInGUDID != null) {
			Set<String> listingNoSetGUDID = itemInGUDID.getFdaMedicalDeviceListingNo();
			List<String> listingNoSetGUDIDList = Util.sortHashSet(listingNoSetGUDID);
			List<String> productCodeList = new ArrayList<>();
			if (listingNoSetGUDIDList != null && !listingNoSetGUDIDList.isEmpty()) {
				for (String listingNo : listingNoSetGUDIDList) {
					FURLS furls = furlsListByDL.get(listingNo);
					if (furls == null)
						productCodeList.add(NOTINFURLS);
					else
						productCodeList.addAll(furls.getProductCode());
				}
				productCodeInFurlsBasedOnDlInGudid = StringUtils.join(productCodeList, separator);
			} else
				productCodeInFurlsBasedOnDlInGudid = na;

		} else
			productCodeInFurlsBasedOnDlInGudid = itemNotInGudid;

	}

	public String getDeviceClassrFomMaster510kpmaLogBasedOnSubmissionInGudid() {
		return deviceClassrFomMaster510kpmaLogBasedOnSubmissionInGudid;
	}

	public void setDeviceClassrFomMaster510kpmaLogBasedOnSubmissionInGudid(GUDID itemInGUDID,
			Map<String, Master510k> master510kList, Map<String, FdaProCode> fdaProCodeList) {
		if (itemInGUDID != null) {
			Set<String> knoGudid = itemInGUDID.getFdaPremarketSubmissionNo();

			// REVISIT
			Set<String> kExempt = itemInGUDID.getDeviceExempt();

			Set<String> proCodeInGudid = itemInGUDID.getFdaProductCodes();
			Set<String> deviceClass = new HashSet<>();

			if (knoGudid != null) {
				for (String knoG : knoGudid) {
					deviceClass
							.add(master510kList.containsKey(knoG) ? master510kList.get(knoG).getDeviceClass() : null);
				}

			} else if (kExempt != null && kExempt.size() == 1 && kExempt.contains(exempt) && proCodeInGudid != null) {
				for (String proCode : proCodeInGudid) {
					deviceClass.add(fdaProCodeList.get(proCode).getDeviceclass());

				}

			}

			deviceClassrFomMaster510kpmaLogBasedOnSubmissionInGudid = deviceClass != null
					? StringUtils.join(deviceClass, separator)
					: na;
		} else
			deviceClassrFomMaster510kpmaLogBasedOnSubmissionInGudid = itemNotInGudid;

	}

	public String getIls510k20() {
		return ils510k20;
	}

	public void setIls510k20(OracleMaster itemInOracle) {
		if (itemInOracle != null)
			ils510k20 = itemInOracle.getIls510K20();
		else
			ils510k20 = itemNotInOracle;

	}

	public String getIlsFda21() {
		return ilsFda21;
	}

	public void setIlsFda21(OracleMaster itemInOracle) {
		if (itemInOracle != null)
			ilsFda21 = itemInOracle.getIlsDev21();
		else
			ilsFda21 = itemNotInOracle;

	}

	public void setIlsFda21(OracleMaster itemInOracle, int flag) {
		if (itemInOracle != null)
			ilsFda21 = itemInOracle.getIlsDev21original();
		else
			ilsFda21 = itemNotInOracle;

	}

	public String getIlsDev6() {
		return ilsDev6;
	}

	public void setIlsDev6(OracleMaster itemInOracle) {
		if (itemInOracle != null)
			ilsDev6 = itemInOracle.getIlsDev6();
		else
			ilsDev6 = itemNotInOracle;
	}

	public String getDlInFurlsBasedOnSubmissionInOracle() {
		return dlInFurlsBasedOnSubmissionInOracle;
	}

	public void setDlInFurlsBasedOnSubmissionInOracle(OracleMaster itemInOracle, Map<String, FURLS> furlsListBySN) {
		Set<String> submissionNoOracleSet = null;
		String submissionNoString = itemInOracle != null ? itemInOracle.getOracleSubmissionNo() : null;
		if (itemInOracle != null) {
			List<String> deviceListingNoList = new ArrayList<>();
			if (submissionNoString != null)
				submissionNoOracleSet = new HashSet<>(Arrays.asList(submissionNoString.split(",")));
			else
				deviceListingNoList.add(na);
			if (submissionNoOracleSet != null && !submissionNoOracleSet.isEmpty()) {

				for (String submissionNo : submissionNoOracleSet) {
					if (submissionNo.matches(submissionNoPattern)) {
						FURLS furls = furlsListBySN.get(submissionNo.trim());
						if (furls == null)
							deviceListingNoList.add(NOTINFURLS);
						else
							deviceListingNoList.addAll(furls.getDeviceListingNo());
					} else
						deviceListingNoList.add(na);
				}
				dlInFurlsBasedOnSubmissionInOracle = StringUtils.join(deviceListingNoList, separator);
			} else
				dlInFurlsBasedOnSubmissionInOracle = na;
		} else
			dlInFurlsBasedOnSubmissionInOracle = itemNotInOracle;
	}

	public String getProductCodeInFurlsBasedOnSubmissionInOracle() {
		return productCodeInFurlsBasedOnSubmissionInOracle;
	}

	public void setProductCodeInFurlsBasedOnSubmissionInOracle(OracleMaster itemInOracle,
			Map<String, FURLS> furlsListBySN) {
		Set<String> submissionNoOracleSet = null;
		String submissionNoString = itemInOracle != null ? itemInOracle.getOracleSubmissionNo() : null;
		if (itemInOracle != null) {
			Set<String> productCodeList = new HashSet<>();

			if (submissionNoString != null)
				submissionNoOracleSet = new HashSet<>(Arrays.asList(submissionNoString.split(",")));
			else
				productCodeList.add(na);
			if (submissionNoOracleSet != null && !submissionNoOracleSet.isEmpty()) {
				for (String submissionNo : submissionNoOracleSet) {
					if (submissionNo.matches(submissionNoPattern)) {
						FURLS furls = furlsListBySN.get(submissionNo.trim());
						if (furls == null)
							productCodeList.add(NOTINFURLS);
						else
							productCodeList.addAll(furls.getProductCode());
					} else
						productCodeList.add(na);
				}
				productCodeInFurlsBasedOnSubmissionInOracle = StringUtils.join(productCodeList, separator);
			} else
				productCodeInFurlsBasedOnSubmissionInOracle = na;
		} else
			productCodeInFurlsBasedOnSubmissionInOracle = itemNotInOracle;
	}

	public String getSubmissionExceptionInFurlsBasedOnDlInOracle() {
		return submissionExceptionInFurlsBasedOnDlInOracle;
	}

	public void setSubmissionExceptionInFurlsBasedOnDlInOracle(OracleMaster itemInOracle,
			Map<String, FURLS> furlsListByDL) {
		List<String> submissionNoFurls = new ArrayList<>();

		String listingNoOracle = itemInOracle != null ? itemInOracle.getIlsDev6() : null;

		if (itemInOracle != null) {
			if (listingNoOracle != null) {
				List<String> listingNoOracleList = Arrays.asList(listingNoOracle.split(","));
				for (String list : listingNoOracleList) {
					if (list.matches(submissionNoPattern)) {
						FURLS furls = furlsListByDL.get(list.trim());
						if (furls != null) {
							submissionNoFurls.add(furls.getPreMarketSubmissionNo());
							if (submissionNoFurls.contains(null) || submissionNoFurls.isEmpty()) {
								submissionNoFurls.add(furls.getPreMarketException());
								submissionNoFurls.remove(null);

							}
							if (submissionNoFurls.contains(null) || submissionNoFurls.isEmpty())
								submissionNoFurls.add(furls.getItemTypeFurls());
							submissionNoFurls.remove(null);
						} else
							submissionNoFurls.add(NOTINFURLS);
					} else
						submissionNoFurls.add(na);
				}
			} else
				submissionNoFurls.add(na);
		} else
			submissionNoFurls.add(itemNotInOracle);

		this.submissionExceptionInFurlsBasedOnDlInOracle = StringUtils.join(submissionNoFurls, separator);
	}

	public String getProductCodeInFurlsBasedOnDlInOracle() {
		return this.productCodeInFurlsBasedOnDlInOracle;
	}

	public void setProductCodeInFurlsBasedOnDlInOracle(OracleMaster itemInOracle, Map<String, FURLS> furlsListByDL) {
		String listingNoOracle = itemInOracle != null ? itemInOracle.getIlsDev6() : null;

		List<String> proCodeFurls = new ArrayList<>();
		if (itemInOracle != null) {
			if (listingNoOracle != null) {
				List<String> listingNoOracleList = Arrays.asList(listingNoOracle.split(","));
				for (String listingNo : listingNoOracleList) {
					if (listingNo.matches(submissionNoPattern)) {
						FURLS furls = furlsListByDL.get(listingNoOracle.trim());
						if (furls != null)
							proCodeFurls.addAll(furls.getProductCode());
						else
							proCodeFurls.add(NOTINFURLS);
					} else
						proCodeFurls.add(na);
				}
			} else
				proCodeFurls.add(na);
		} else
			proCodeFurls.add(itemNotInOracle);
		productCodeInFurlsBasedOnDlInOracle = StringUtils.join(proCodeFurls, separator);
	}

	public String getDeviceClassFromMaster510kpmaLogBasedOnSubmissionOracle() {
		return deviceClassFromMaster510kpmaLogBasedOnSubmissionNoOracle;
	}
	/*
	 * device class If item is in GUDID, "rely on GUDID vs FURLS comaprison"; if
	 * item is not in GUDID and submission # exists in Oracle, get device class in
	 * Master 510k/PMA Log based on submission # in Oracle. If item is in Oracle and
	 * either pre-amendment, 510k exempt or enforcement discretion, get class from
	 * public FDA Pro-code Database based on pro-code in GUDID. Otherwise, N/A.
	 */

	public void setDeviceClassFromMaster510kpmaLogBasedOnSubmissionOracle(OracleMaster itemInOracle, GUDID itemInGudid,
			Map<String, Master510k> master510kList, Map<String, FdaProCode> fdaProCodeList) {
		if (itemInOracle != null) {

			String knoOracle = itemInOracle.getOracleSubmissionNo();
			String proCode = itemInOracle.getFdaProcode();
			if (itemInGudid == null) {
				if (knoOracle != null) {
					deviceClassFromMaster510kpmaLogBasedOnSubmissionNoOracle = master510kList.containsKey(knoOracle)
							? master510kList.get(knoOracle).getDeviceClass()
							: null;
				} else {
					deviceClassFromMaster510kpmaLogBasedOnSubmissionNoOracle = (proCode != null
							&& fdaProCodeList.containsKey(proCode)) ? fdaProCodeList.get(proCode).getDeviceclass() : na;
				}

			}
		}
		deviceClassFromMaster510kpmaLogBasedOnSubmissionNoOracle = na;
	}

	public String getFdaClassAgile() {
		return fdaClassAgile;
	}

	public void setFdaClassAgile(Agile itemInAgile) {
		if (itemInAgile != null)
			fdaClassAgile = itemInAgile.getFdaClass();

		else
			fdaClassAgile = itemNotInAgile;

	}

	public String getFdaProductCodeInAgile() {
		return fdaProductCodeInAgile;
	}

	public void setFdaProductCodeInAgile(Agile itemInAgile) {
		if (itemInAgile != null) {
			Set<String> proCode = itemInAgile.getFdaProductCode();
			proCode.remove(null);
			fdaProductCodeInAgile = StringUtils.join(proCode, separator);
		} else
			fdaProductCodeInAgile = itemNotInAgile;

	}

	public String getFdaRegulatoryStatus() {
		return fdaRegulatoryStatus;
	}

	public void setFdaRegulatoryStatus(Agile itemInAgile) {
		if (itemInAgile != null)
			fdaRegulatoryStatus = itemInAgile.getFdaRegulatoryStatus() == null ? na
					: itemInAgile.getFdaRegulatoryStatus();
		else
			fdaRegulatoryStatus = itemNotInAgile;

	}

	public String getDLNoInFURLSBasedOnProcodeInAgile() {
		return DLNoInFURLSBasedOnProcodeInAgile;
	}

	public void setDLNoInFURLSBasedOnProcodeInAgile(Agile itemInAgile, Map<String, FURLS> furlsListByProductCode) {
		if (itemInAgile != null) {
			Set<String> prodcutCodeInAgile = itemInAgile.getFdaProductCode();
			Set<String> dlNolist = null;
			prodcutCodeInAgile.remove(null);
			String regStatus = itemInAgile.getFdaRegulatoryStatus();
			if (prodcutCodeInAgile != null && prodcutCodeInAgile.size() == 1) {

				for (String proCode : prodcutCodeInAgile) {
					int len = proCode.length();
					proCode = len > 3 ? proCode.substring(len - 3, len) : proCode;
					if (furlsListByProductCode.containsKey(proCode.trim())) {
						dlNolist = furlsListByProductCode.get(proCode).getDeviceListingNo();
						dlNolist.remove(null);

					}
				}
				if (dlNolist == null) {
					DLNoInFURLSBasedOnProcodeInAgile = NOTINFURLS;
				} else
					DLNoInFURLSBasedOnProcodeInAgile = dlNolist.size() > 1 ? "Pro Code linked to more than 1 DL no"
							: StringUtils.join(dlNolist, separator);

			} else
				DLNoInFURLSBasedOnProcodeInAgile = na;
		} else
			DLNoInFURLSBasedOnProcodeInAgile = itemNotInAgile;
	}

	public String getSubmissionNoExceptionInFURLSBasedOnProcodeInAgile() {
		return submissionNoExceptionInFURLSBasedOnProcodeInAgile;
	}

	/**
	 * @param itemInAgile
	 * @param furlsListByProductCode
	 */
	public void setSubmissionNoExceptionInFURLSBasedOnProcodeInAgile(Agile itemInAgile,
			Map<String, FURLS> furlsListByProductCode) {

		String subException = null;
		if (itemInAgile != null) {

			Set<String> prodcutCodeInAgile = itemInAgile.getFdaProductCode();
			prodcutCodeInAgile.remove(null);
			String regStatus = itemInAgile.getFdaRegulatoryStatus();

			if (prodcutCodeInAgile.size() == 1) {

				for (String proCode : prodcutCodeInAgile) {

					int len = proCode.length();
					proCode = len > 3 ? proCode.substring(len - 3, len) : proCode;
					FURLS furls = furlsListByProductCode.get(proCode);
					if (furls != null) {
						Set<String> dlNolist = furlsListByProductCode.get(proCode).getDeviceListingNo();
						if (dlNolist != null && dlNolist.size() > 1)
							subException = "Pro Code linked to more than 1 DL no";
						else {
							subException = furls.getPreMarketSubmissionNo();
							if (subException == null)
								subException = furls.getPreMarketException();
							if (subException == null)
								subException = furls.getItemTypeFurls();
						}
					} else
						subException = NOTINFURLS;
				}

			} else {
				subException = na;
			}
		} else
			subException = itemNotInAgile;
		this.submissionNoExceptionInFURLSBasedOnProcodeInAgile = subException;
	}

	public String getDeviceClassFromFdaPublicProCodeDbBasedOnProCodeAgile() {
		return deviceClassFromFdaPublicProCodeDbBasedOnProCodeAgile;
	}
	/*
	 * 
	 * 
	 * Device Class from FDA Public Pro-Code DB based on pro-code in Agile --If item
	 * only has pro-code in Agile and pro-code only appears 1 time in FURLS, get
	 * device class in FDA Public Pro-Code DB based on pro-code in Agile. Otherwise,
	 * N/A.
	 */

	/*
	 * public void setDeviceClassFromFdaPublicProCodeDbBasedOnProCodeAgile(Agile
	 * itemInAgile, Map<String, FURLS> furlsListByProductCode, Map<String,
	 * FdaProCode> fdaProCodeList) {
	 * 
	 * String proCodeAgile = itemInAgile != null ? itemInAgile.getFdaProductCode() :
	 * null; String deviceClassAgile = proCodeAgile != null &&
	 * furlsListByProductCode.containsKey(proCodeAgile) ?
	 * fdaProCodeList.get(proCodeAgile).getDeviceclass() : "N/A";
	 * deviceClassFromFdaPublicProCodeDbBasedOnProCodeAgile = deviceClassAgile; }
	 */

	public String getMakeBuy() {
		return makeBuy;
	}

	public void setMakeBuy(OracleMaster itemInOracle) {
		if (itemInOracle != null)
			makeBuy = itemInOracle.getMakeBuy();
		else
			makeBuy = itemNotInOracle;
	}

	public String getIlsEstReg_22() {
		return ilsEstReg_22;
	}

	public void setIlsEstReg_22(OracleMaster itemInOracle) {
		if (itemInOracle != null)
			ilsEstReg_22 = itemInOracle.getIlsEstablishmentRegNo();
		else
			ilsEstReg_22 = itemNotInOracle;
	}

	public String getIlsRegAddress() {
		return ilsRegAddress;
	}

	public void setIlsRegAddress(OracleMaster itemInOracle) {
		if (itemInOracle != null)
			ilsRegAddress = itemInOracle.getIlsRegAddress();
		else
			ilsRegAddress = itemNotInOracle;
	}

	public String getVendor510kOldHtsDescrPrevious() {
		return vendor510kOldHtsDescrPrevious;
	}

	public void setVendor510kOldHtsDescrPrevious(OracleMaster itemInOracle) {
		if (itemInOracle != null)
			vendor510kOldHtsDescrPrevious = itemInOracle.getVendor510KOldHtsDescPrevious();
		else
			vendor510kOldHtsDescrPrevious = itemNotInOracle;
	}

	public String getVendEstReg25() {
		return vendEstReg25;
	}

	public void setVendEstReg25(OracleMaster itemInOracle) {
		if (itemInOracle != null)
			vendEstReg25 = itemInOracle.getVendorFDAEstablishmentRegNo();
		else
			vendEstReg25 = itemNotInOracle;
	}

	public String getVendDev23() {
		return vendDev23;
	}

	public void setVendDev23(OracleMaster itemInOracle) {
		if (itemInOracle != null)
			vendDev23 = itemInOracle.getVendorDeviceNo();
		else
			vendDev23 = itemNotInOracle;
	}

	public String getGmdn28() {
		return gmdn28;
	}

	public void setGmdn28(OracleMaster itemInOracle) {
		if (itemInOracle != null)
			gmdn28 = itemInOracle.getGmdnCode();
		else
			gmdn28 = itemNotInOracle;
	}

	public String getSubmissionTypeInFurls() {

		return submissionTypeInFurls;
	}

	public void setSubmissionTypeInFurls(String submissionTypeInFurls) {
		this.submissionTypeInFurls = submissionTypeInFurls;
	}

	public String getFdaProCodeOracle() {
		return fdaProCodeOracle;
	}

	public void setFdaProCodeOracle(OracleMaster itemInOracle) {
		this.fdaProCodeOracle = itemInOracle != null ? itemInOracle.getFdaProcode() : itemNotInOracle;
	}

	public String getDeviceListingNoOracle() {
		return this.deviceListingNoOracle;
	}

	public void setDeviceListingNoOracle(OracleMaster itemInOracle) {
		this.deviceListingNoOracle = itemInOracle != null ? itemInOracle.getIlsFDADeviceListingNo() : itemNotInOracle;
	}

	public String getSubmissionNoOracle() {
		return submissionNoOracle;
	}

	public void setSubmissionNoOracle(OracleMaster itemInOracle) {
		this.submissionNoOracle = itemInOracle != null ? itemInOracle.getOracleSubmissionNo() : itemNotInOracle;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus() {

		if (fdaProductCodeCalculatedG == FLAG || fdaClassG == FLAG || submissionTypeG == FLAG
				|| fdaMedicalDeviceListingNumberG == FLAG)
			this.status = mismatch;
		else
			this.status = match;

	}
	// green section

	public String getApprovedG() {
		return approvedG;
	}
	// approval
	/*
	 * 1. If Green section - FDA submission number is populated real number , then
	 * Populate text "YES"
	 * 
	 * 2. If Green section - FDA submission number is populated with "N/A" and Green
	 * section - Submission Type is "Exempt " or "Preamendment" or
	 * "Enforcement Discussion" populate text "Not Required" 3. If Green section -
	 * FDA submission number is populated with "N/A" and Green section - Submission
	 * Type is " 510(k)" or "PMA" populate txt "NO" 4. else discrepancy flag "Y" 5.
	 * item to discuss w/Mareana is if item status = "Inactive" and there is a
	 * discrepancy flag - if all green values will be defaulted to "N/A" since item
	 * status is inactive
	 */

	public void setApprovedG() {

		String submissionTypeGreen = submissionTypeG;
		Set<String> fdaSubNo = new HashSet<>(Arrays.asList(fdaSubmissionNumberG.split(",")));

		if (!fdaSubNo.isEmpty() && fdaSubNo.stream().allMatch(e -> e.matches(submissionNoPattern)))
			approvedG = "YES";
		else if (!fdaSubNo.isEmpty() && submissionTypeGreen != null && fdaSubNo.contains(na)
				&& (submissionTypeGreen.equals(exempt) || submissionTypeGreen.equals(preamendment)
						|| submissionTypeGreen.equals(enforcementDiscretion))) {
			approvedG = "Not Required";

		} else if (!fdaSubNo.isEmpty() && submissionTypeGreen != null && fdaSubNo.contains(na)
				&& (submissionTypeGreen.contains(k510) || submissionTypeGreen.contains(pma)))
			approvedG = "NO";
		else
			approvedG = FLAG;

	}

	public String getFdaProductCodeCalculatedG() {
		return fdaProductCodeCalculatedG;
	}

	public void setFdaProductCodeCalculatedG(GUDID itemInGUDID, OracleMaster itemInOracle, Agile itemInAgile,
			Map<String, FURLS> furlsListProCode) {

		if (itemInGUDID != null) {
			Set<String> fdaProductCodesGudid = fdaProductCodesInGudid != null
					? new HashSet<>(Arrays.asList(fdaProductCodesInGudid.split(",")))
					: null;
			Set<String> fdaProductCodesFurlsGudid = productCodeInFurlsBasedOnSubmissionInGudid != null
					? new HashSet<>(Arrays.asList(productCodeInFurlsBasedOnSubmissionInGudid.split(",")))
					: null;
			Set<String> fdaProductCodesFurlsGudidDl = productCodeInFurlsBasedOnDlInGudid != null
					? new HashSet<>(Arrays.asList(productCodeInFurlsBasedOnDlInGudid.split(",")))
					: null;
			if (fdaProductCodesGudid != null && fdaProductCodesFurlsGudid != null
					&& fdaProductCodesFurlsGudid.equals(fdaProductCodesGudid))
				fdaProductCodeCalculatedG = StringUtils.join(fdaProductCodesGudid, separator);
			else if (fdaProductCodesGudid != null && fdaProductCodesFurlsGudidDl != null
					&& fdaProductCodesGudid.equals(fdaProductCodesFurlsGudidDl))
				fdaProductCodeCalculatedG = StringUtils.join(fdaProductCodesGudid, separator);
			else
				fdaProductCodeCalculatedG = FLAG;

		} else if (itemInOracle != null) {
			String proCode = null;
			if (ilsFda21 != null) {
				int len = ilsFda21.length();
				proCode = len > 3 ? ilsFda21.substring(len - 3, len) : ilsFda21;
			}

			Set<String> fdaProductCodesOracle = ilsFda21 != null ? new HashSet<>(Arrays.asList(ilsFda21.split(",")))
					: null;
			Set<String> fdaProductCodesFurlsOracle = productCodeInFurlsBasedOnSubmissionInOracle != null
					? new HashSet<>(Arrays.asList(productCodeInFurlsBasedOnSubmissionInOracle.split(",")))
					: null;

			Set<String> fdaProductCodesOracleDl = productCodeInFurlsBasedOnDlInOracle != null
					? new HashSet<>(Arrays.asList(productCodeInFurlsBasedOnDlInOracle.split(",")))
					: null;

			if (fdaProductCodesOracle != null && fdaProductCodesFurlsOracle != null
					&& fdaProductCodesOracle.equals(fdaProductCodesFurlsOracle))
				fdaProductCodeCalculatedG = StringUtils.join(fdaProductCodesOracle, separator);
			else if (fdaProductCodesOracle != null && fdaProductCodesOracleDl != null
					&& fdaProductCodesOracle.equals(fdaProductCodesOracleDl))
				fdaProductCodeCalculatedG = StringUtils.join(fdaProductCodesOracle, separator);
			else
				fdaProductCodeCalculatedG = FLAG;

		} else if (itemInAgile != null) {
			Set<String> proCodesAgile = new HashSet<>();
			Set<String> proCode = new HashSet<>(Arrays.asList(fdaProductCodeInAgile.split(",")));
			if (!proCode.isEmpty()) {
				for (String proCod : proCode) {
					int len = proCod.length();
					String proC = proCod.length() > 3 ? proCod.substring(len - 3, len) : proCod;
					proCodesAgile.add(furlsListProCode.containsKey(proC) ? proCod : FLAG);
				}
				fdaProductCodeCalculatedG = StringUtils.join(proCodesAgile, separator);
			} else
				fdaProductCodeCalculatedG = FLAG;
		} else
			fdaProductCodeCalculatedG = FLAG;
		if (itemInGUDID == null && itemInAgile != null && na.equals(dlInFurlsBasedOnSubmissionInOracle)
				&& na.equals(productCodeInFurlsBasedOnSubmissionInOracle)
				&& na.equals(submissionExceptionInFurlsBasedOnDlInOracle)
				&& na.equals(productCodeInFurlsBasedOnDlInOracle)
				&& submissionNoExceptionInFURLSBasedOnProcodeInAgile != null && fdaRegulatoryStatus != null
				&& subTypeList.contains(submissionNoExceptionInFURLSBasedOnProcodeInAgile.toLowerCase())
				&& subTypeList.contains(fdaRegulatoryStatus.toLowerCase()) && fdaProductCodeInAgile != null) {
			int len = fdaProductCodeInAgile.length();
			fdaProductCodeCalculatedG = len > 3 ? fdaProductCodeInAgile.substring(len - 3, len)
					: submissionNoExceptionInFURLSBasedOnProcodeInAgile;
		}

	}

	public String getFdaClassG() {
		return fdaClassG;
	}

	public void setFdaClassG(Map<String, Master510k> master510kList, Map<String, FdaProCode> fdaProCodeList) {
		Set<String> fdaClass = new HashSet<>();
		Set<String> fdaClassList = new HashSet<>();
		if (fdaSubmissionNumberG != null) {
			Set<String> fdaSubNo = new HashSet<>(Arrays.asList(fdaSubmissionNumberG.trim().split(",")));
			List<String> proCodeList = Arrays
					.asList(fdaProductCodeCalculatedG != null ? fdaProductCodeCalculatedG.split(",") : null);
			if (fdaSubNo.size() == 1)
				fdaClassG = master510kList.containsKey(fdaSubmissionNumberG)
						? master510kList.get(fdaSubmissionNumberG).getDeviceClass()
						: (master510kList
								.containsKey(fdaSubmissionNumberG + String.valueOf(fdaSubmissionSupplementNumberG))
										? master510kList
												.get(fdaSubmissionNumberG
														+ String.valueOf(fdaSubmissionSupplementNumberG))
												.getDeviceClass()
										: null);

			// revisit
			else if (fdaSubNo.size() > 1) {
				for (String subNo : fdaSubNo)
					fdaClass.add(master510kList.containsKey(subNo) ? master510kList.get(subNo).getDeviceClass()
							: (master510kList.containsKey(subNo + String.valueOf(fdaSubmissionSupplementNumberG))
									? master510kList.get(subNo + String.valueOf(fdaSubmissionSupplementNumberG))
											.getDeviceClass()
									: null));
				fdaClassG = StringUtils.join(fdaClass, separator);
			}
			if (proCodeList != null && fdaSubmissionNumberG.equalsIgnoreCase(na)
					&& (exempt.equalsIgnoreCase(submissionTypeG) || preamendment.equalsIgnoreCase(submissionTypeG)
							|| enforcementDiscretion.equalsIgnoreCase(submissionTypeG))) {
				for (String proCode : proCodeList) {
					String code = proCode.length() > 3 ? proCode.substring(proCode.length() - 3, proCode.length())
							: proCode;
					fdaClassList
							.add(fdaProCodeList.containsKey(code) ? fdaProCodeList.get(code).getDeviceclass() : null);

				}
				fdaClassG = StringUtils.join(fdaClassList, separator);

			}
		}
		if ((notReq.equalsIgnoreCase(approvedG) || na.equalsIgnoreCase(approvedG)) && (fdaProductCodeCalculatedG != null
				&& fdaProductCodeCalculatedG.toLowerCase().matches("n/a|ncmd|vet d")))
			fdaClassG = na;

		if (fdaClassG == null)
			fdaClassG = FLAG;
		else if (fdaClassG.matches("[0-9]+"))
			fdaClassG = Util.intToRoman(Integer.parseInt(fdaClassG));

	}

	public String getSubmissionTypeG() {
		return submissionTypeG;
	}

	public void setSubmissionTypeG(GUDID itemInGUDID, OracleMaster itemInOracle, Map<String, FURLS> furlsListBySN,
			Agile itemInAgile, Map<String, FURLS> furlsListByDL) {

		FURLS furls = null;
		Set<String> submissionType = new HashSet<>();
		if (itemInGUDID != null) {
			Set<String> knoGudidList = itemInGUDID.getFdaPremarketSubmissionNo();
			Set<String> dnoGudidList = itemInGUDID.getFdaMedicalDeviceListingNo();
			dnoGudidList.remove(null);
			knoGudidList.remove(null);
			// revisit to make submission type set
			Set<String> dmExempt = new HashSet<>(
					Arrays.asList(deviceExemptFromPremarkeSubmissionRequirementsGudid.split(",")));
			dmExempt.remove("false");
			String dmExemptval = dmExempt.toArray(new String[1])[0];
			if (dmExempt.size() == 1) {
				if (notExempt.equalsIgnoreCase(dmExemptval)) {
					if (knoGudidList != null && !knoGudidList.isEmpty()) {
						for (String knoGudid : knoGudidList) {
							furls = furlsListBySN.containsKey(knoGudid) ? furlsListBySN.get(knoGudid) : null;
							if (furls != null) {
								String furlsSubmissionType = furls.getItemTypeFurls();
								submissionType.add(furlsSubmissionType);
							} else
								submissionType.add(FLAG);

						}

					} else
						submissionType.add(FLAG);
				} else if (dmExemptval.equalsIgnoreCase(exempt)) {
					if (knoGudidList != null && !knoGudidList.isEmpty()) {
						submissionType.add(FLAG);
					} else {
						if (dnoGudidList == null || dnoGudidList.isEmpty()) {
							submissionType.add(FLAG);
						} else {
							for (String dno : dnoGudidList) {
								String subType = furlsListByDL.containsKey(dno)
										? furlsListByDL.get(dno).getItemTypeFurls()
										: null;
								if (subType == null)
									submissionType.add(FLAG);
								else {
									if (subType.equals(exempt) || subType.equalsIgnoreCase(preamendment)
											|| subType.equalsIgnoreCase(enforcementDiscretion))

										submissionType.add(subType);
									else
										submissionType.add(FLAG);

								}
							}

						}

					}

				}
			} else
				submissionType.add(FLAG);
		} else if (itemInOracle != null) {
			Set<String> knoOracleList = new HashSet<>(Arrays.asList(String.valueOf(ils510k20).split(",")));
			Set<String> dnoOracleList = new HashSet<>(Arrays.asList(String.valueOf(ilsDev6).split(",")));
			// revisit to make submission type set
			Set<String> dmExempt = knoOracleList.stream().map(e -> e.matches(submissionNoPattern) ? notExempt : e)
					.collect(Collectors.toSet());
			dmExempt.remove("false");
			String dmExemptval = dmExempt.toArray(new String[1])[0];
			if (dmExempt.size() == 1) {
				if (notExempt.equalsIgnoreCase(dmExemptval)) {
					if (knoOracleList != null) {
						for (String knoOracle : knoOracleList) {
							furls = furlsListBySN.containsKey(knoOracle) ? furlsListBySN.get(knoOracle) : null;
							if (furls != null) {
								String furlsSubmissionType = furls.getItemTypeFurls();
								submissionType.add(furlsSubmissionType);
							} else
								submissionType.add(FLAG);

						}

					} else
						submissionType.add(FLAG);
				} else if (dmExemptval.equalsIgnoreCase(exempt)
						|| preamendment.equalsIgnoreCase(dmExemptval.replace("-", ""))
						|| enforcementDiscretion.equalsIgnoreCase(dmExemptval)) {
					if (dnoOracleList == null || dnoOracleList.isEmpty()) {
						submissionType.add("TBD");
					} else {
						for (String dno : dnoOracleList) {
							String subType = furlsListByDL.containsKey(dno) ? furlsListByDL.get(dno).getItemTypeFurls()
									: null;
							if (subType == null)
								submissionType.add(FLAG);
							else {
								if (exempt.equalsIgnoreCase(subType)
										|| preamendment.equalsIgnoreCase(subType.replace("-", ""))
										|| enforcementDiscretion.equalsIgnoreCase(subType))
									submissionType.add(subType);
								else
									submissionType.add(FLAG);

							}
						}

					}

				} else
					submissionType.add(FLAG);
			} else
				submissionType.add(FLAG);

		} else if (itemInAgile != null) {
			String subTypeAgile = itemInAgile.getFdaRegulatoryStatus();
			if (fdaRegulatoryStatus != null && fdaRegulatoryStatus.equalsIgnoreCase(subTypeAgile))
				submissionType.add(fdaRegulatoryStatus);

			else {
				String knoAgile = itemInAgile.getSubMissionNo();
				furls = furlsListBySN.containsKey(knoAgile) ? furlsListBySN.get(knoAgile) : null;
				if (furls != null && subTypeAgile != null) {
					submissionType.add(subTypeAgile.equalsIgnoreCase(furls.getItemTypeFurls()) ? subTypeAgile : FLAG);

				} else
					submissionType.add(FLAG);
			}
		}

		if (itemInGUDID == null && itemInAgile != null && na.equals(dlInFurlsBasedOnSubmissionInOracle)
				&& na.equals(productCodeInFurlsBasedOnSubmissionInOracle)
				&& na.equals(submissionExceptionInFurlsBasedOnDlInOracle)
				&& na.equals(productCodeInFurlsBasedOnDlInOracle)
				&& submissionNoExceptionInFURLSBasedOnProcodeInAgile != null && fdaRegulatoryStatus != null
				&& subTypeList.contains(submissionNoExceptionInFURLSBasedOnProcodeInAgile.toLowerCase())
				&& subTypeList.contains(fdaRegulatoryStatus.toLowerCase())) {
			submissionType = new HashSet<>();
			submissionType.add(submissionNoExceptionInFURLSBasedOnProcodeInAgile);
		}

		if (submissionType.contains(FLAG))
			submissionTypeG = FLAG;
		else
			submissionTypeG = StringUtils.join(submissionType, separator);
	}

	public String getFdaSubmissionNumberG() {
		return fdaSubmissionNumberG;
	}

	public void setFdaSubmissionNumberG(GUDID itemInGudid, OracleMaster itemInOracle, Agile itemInAgile,
			Map<String, FURLS> furlsListsn) {

		String knofurls = null;

		Set<String> knoListG = new HashSet<>();
		Set<String> exceptions = new HashSet<>(Arrays.asList(exempt, preamendment, enforcementDiscretion));
		Set<String> exception = exceptions.stream().map(e -> e.toLowerCase().replaceAll("\\s+", ""))
				.collect(Collectors.toSet());
		if (itemInGudid != null) {
			Set<String> knoGudid = new HashSet<>(
					Arrays.asList(String.valueOf(fdaPremarketSubmissionNumbersGudid).split(",")));
			Set<String> kNoFurlsGudid = new HashSet<>(
					Arrays.asList(String.valueOf(submissionExceptionInFurlsBasedOnDlInGudid).split(",")));
			if (!knoGudid.isEmpty() && knoGudid.equals(kNoFurlsGudid))
				knoListG.addAll(knoGudid);
			else if (exception
					.contains(String.valueOf(deviceExemptFromPremarkeSubmissionRequirementsGudid).toLowerCase()
							.replaceAll("\\s+", ""))
					&& exception.contains(String.valueOf(submissionExceptionInFurlsBasedOnDlInGudid).toLowerCase()
							.replaceAll("\\s+", "")))
				knoListG.add(na);
			else
				knoListG.add(FLAG);
		} else if (itemInOracle != null) {
			Set<String> kNoOracle = new HashSet<>(Arrays.asList(String.valueOf(ils510k20).split(",")));
			Set<String> kNoFurlsOracle = new HashSet<>(
					Arrays.asList(String.valueOf(submissionExceptionInFurlsBasedOnDlInOracle).split(",")));

			if (!kNoOracle.isEmpty() && kNoOracle.equals(kNoFurlsOracle)
					&& kNoOracle.stream().allMatch(e -> e.matches(submissionNoPattern)))
				knoListG.addAll(kNoOracle);
			// to do more on this part to match string
			else if (((ils510k20 != null && exception.contains(ils510k20.toLowerCase().replace("-", "")))
					&& exception.contains(String.valueOf(submissionExceptionInFurlsBasedOnDlInOracle).toLowerCase()
							.replaceAll("\\s+", "")))
					|| ils510k20 != null && !ils510k20.matches(submissionNoPattern))
				knoListG.add(na);
			else
				knoListG.add(FLAG);

		} else if (itemInAgile != null) {
			if (exception.contains(fdaRegulatoryStatus.toLowerCase())
					&& exception.contains(submissionNoExceptionInFURLSBasedOnProcodeInAgile.toLowerCase()))
				knoListG.add(na);
			else
				knoListG.add(FLAG);
		} else
			knoListG.add(FLAG);
		fdaSubmissionNumberG = StringUtils.join(knoListG, separator);
	}

	public String getFdaSubmissionSupplementNumberG() {
		return this.fdaSubmissionSupplementNumberG;
	}

	public void setFdaSubmissionSupplementNumberG(GUDID itemInGudid) {
		String submissionPattern = "[pPHh][0-9]+";
		if (fdaSubmissionSupplementNumberGudid != null && fdaSubmissionNumberG.toLowerCase().matches(submissionPattern)
				&& fdaSubmissionSupplementNumberGudid.matches("[0-9]+"))
			fdaSubmissionSupplementNumberG = fdaSubmissionSupplementNumberGudid;
		else
			fdaSubmissionSupplementNumberG = na;
	}

	public String getSubmissionStatusG() {
		return submissionStatusG;
	}

	public void setSubmissionStatusG() {

		if (fdaSubmissionNumberG != null && Arrays.asList(fdaSubmissionNumberG.split(",")).stream()
				.allMatch(e -> e.trim().matches(submissionNoPattern)))
			this.submissionStatusG = "Approved/ Cleared";
		else
			submissionStatusG = na;
	}

	public String getSubmissionDateG() {
		return submissionDateG;
	}

	public void setSubmissionDateG(Map<String, Master510k> master510kList) {
		Set<String> fdasubNo = new HashSet<>(Arrays.asList(fdaSubmissionNumberG.split(",")));
		List<String> fdasubNoList = Util.sortHashSet(fdasubNo);
		List<String> approvalDateList = new ArrayList<>();
		if (!fdasubNoList.isEmpty() && fdasubNoList.stream().allMatch(e -> e.matches(submissionNoPattern)))
			approvalDateList = fdasubNoList.stream().map(e -> {
				if (master510kList.containsKey(e))
					return master510kList.get(e).getSubmissionDate();
				else if (master510kList.containsKey(e + fdaSubmissionSupplementNumberG))
					return master510kList.get(e + fdaSubmissionSupplementNumberG).getSubmissionDate();
				else
					return na;
			}).collect(Collectors.toList());
		else
			approvalDateList.add(na);

		submissionDateG = StringUtils.join(approvalDateList, separator);
	}

	public String getApprovalDateG() {
		return approvalDateG;
	}

	public void setApprovalDateG(Map<String, Master510k> master510kList) {
		Set<String> fdasubNo = new HashSet<>(Arrays.asList(fdaSubmissionNumberG.split(",")));
		List<String> fdasubNoList = Util.sortHashSet(fdasubNo);
		List<String> approvalDateList = new ArrayList<>();
		if (!fdasubNoList.isEmpty() && fdasubNoList.stream().allMatch(e -> e.matches(submissionNoPattern)))
			approvalDateList = fdasubNoList.stream().map(e -> {
				System.out.println(e);
				if (master510kList.containsKey(e))
					return master510kList.get(e).getClearanceApprovalDate();
				else if (master510kList.containsKey(e + fdaSubmissionSupplementNumberG))
					return master510kList.get(e + fdaSubmissionSupplementNumberG).getClearanceApprovalDate();
				else
					return na;

			}).collect(Collectors.toList());
		else
			approvalDateList.add(na);

		approvalDateG = StringUtils.join(approvalDateList, separator);
	}

	public String getApprovalClearanceLetterFiledInternallyG() {
		return approvalClearanceLetterFiledInternallyG;
	}

	public void setApprovalClearanceLetterFiledInternallyG(Map<String, Master510k> master510kList) {
		approvalClearanceLetterFiledInternallyG = fdaSubmissionNumberG != null
				&& this.fdaSubmissionNumberG.matches(submissionNoPattern)
				&& master510kList.containsKey(fdaSubmissionNumberG)
						? master510kList.get(fdaSubmissionNumberG).getClearanceApprovalDate()
						: "N/A";
	}

	public String getPermissionToUseLetterFiledInternallyG() {
		return permissionToUseLetterFiledInternallyG;
	}

	public void setPermissionToUseLetterFiledInternallyG(String permissionToUseLetterFiledInternallyG) {
		this.permissionToUseLetterFiledInternallyG = permissionToUseLetterFiledInternallyG;
	}

	public String getK510dtIfUsedToBringSkuToMarketG() {
		return k510dtIfUsedToBringSkuToMarketG;
	}

	public void setK510dtIfUsedToBringSkuToMarketG(String k510dtIfUsedToBringSkuToMarketG) {
		this.k510dtIfUsedToBringSkuToMarketG = k510dtIfUsedToBringSkuToMarketG;
	}

	public String getFdaMedicalDeviceListingNumberG() {
		return fdaMedicalDeviceListingNumberG;
	}

	public void setFdaMedicalDeviceListingNumberG(GUDID itemInGUDID, OracleMaster itemInOracle, Agile itemInAgile) {
		String dfurls = null;
		Set<String> dlListG = new HashSet<>();
		Set<String> exceptions = new HashSet<>(Arrays.asList(exempt, preamendment, enforcementDiscretion));
		Set<String> exception = exceptions.stream().map(e -> e.toLowerCase().replaceAll("\\s+", ""))
				.collect(Collectors.toSet());
		if (itemInGUDID != null) {
			Set<String> dlGudid = new HashSet<>(Arrays.asList(fdaMedicalDeviceListingNumberGudid.split(",")));
			Set<String> dlFurlsGudid = new HashSet<>(Arrays.asList(dlInFurlsBasedOnSubmissionInGudid.split(",")));
			Set<String> proCodeGudid = new HashSet<>(Arrays.asList(fdaProductCodesInGudid.split(",")));
			Set<String> proCodeFurlsGudid = new HashSet<>(Arrays.asList(productCodeInFurlsBasedOnDlInGudid.split(",")));
			if (!dlGudid.isEmpty() && dlGudid.equals(dlFurlsGudid))
				dlListG.addAll(dlGudid);
			else if (exception
					.contains(String.valueOf(deviceExemptFromPremarkeSubmissionRequirementsGudid).toLowerCase()
							.replaceAll("\\s+", ""))
					&& exception.contains(String.valueOf(submissionExceptionInFurlsBasedOnDlInGudid).toLowerCase()
							.replaceAll("\\s+", ""))
					&& proCodeGudid.equals(proCodeFurlsGudid))
				dlListG.addAll(dlGudid);
			else
				dlListG.add(FLAG);
		} else if (itemInOracle != null) {
			Set<String> listingNoOracle = new HashSet<>(Arrays.asList(String.valueOf(ilsDev6).split(",")));
			Set<String> listingNoFurlsOracle = new HashSet<>(
					Arrays.asList(String.valueOf(dlInFurlsBasedOnSubmissionInOracle).split(",")));
			Set<String> proCodeOracle = new HashSet<>(Arrays.asList(String.valueOf(ilsFda21).split(",")));
			Set<String> proCodeNoFurlsOracle = new HashSet<>(
					Arrays.asList(String.valueOf(productCodeInFurlsBasedOnDlInOracle).split(",")));

			if (!listingNoOracle.isEmpty() && listingNoOracle.equals(listingNoFurlsOracle))
				dlListG.addAll(listingNoOracle);
			// to do more on this part to match string
			else if ((ils510k20 != null && exception.contains(ils510k20.toLowerCase().replace("-", "")))
					&& exception.contains(String.valueOf(submissionExceptionInFurlsBasedOnDlInOracle).toLowerCase()
							.replaceAll("\\s+", ""))
					&& proCodeOracle.equals(proCodeNoFurlsOracle))
				dlListG.addAll(listingNoOracle);
			else
				dlListG.add(FLAG);

		} else if (itemInAgile != null) {
			if (exception.contains(fdaRegulatoryStatus.toLowerCase())
					&& exception.contains(submissionNoExceptionInFURLSBasedOnProcodeInAgile.toLowerCase()))
				dlListG.add(DLNoInFURLSBasedOnProcodeInAgile);
			else
				dlListG.add(FLAG);
		}
		if (itemInGUDID == null && itemInAgile != null && na.equals(dlInFurlsBasedOnSubmissionInOracle)
				&& na.equals(productCodeInFurlsBasedOnSubmissionInOracle)
				&& na.equals(submissionExceptionInFurlsBasedOnDlInOracle)
				&& na.equals(productCodeInFurlsBasedOnDlInOracle)
				&& submissionNoExceptionInFURLSBasedOnProcodeInAgile != null && fdaRegulatoryStatus != null
				&& subTypeList.contains(submissionNoExceptionInFURLSBasedOnProcodeInAgile.toLowerCase())
				&& subTypeList.contains(fdaRegulatoryStatus.toLowerCase()) && DLNoInFURLSBasedOnProcodeInAgile != null
				&& DLNoInFURLSBasedOnProcodeInAgile.matches(submissionNoPattern)) {
			dlListG = new HashSet<>();
			dlListG.add(DLNoInFURLSBasedOnProcodeInAgile);
		}

		fdaMedicalDeviceListingNumberG = StringUtils.join(dlListG, separator);
	}

	public String getGmdnCodesG() {
		return gmdnCodesG;
	}

	public void setGmdnCodesG(OracleMaster itemInOracle, GUDID itemInGudid) {

		gmdnCodesG = itemInGudid != null ? StringUtils.join(itemInGudid.getGmdnCodes(), separator)
				: (itemInOracle != null ? itemInOracle.getGmdnCode() : "TBD");
	}

	public String getCommentsG() {
		return commentsG;
	}

	public void setCommentsG(String commentsG) {
		this.commentsG = commentsG;
	}

	public String getAction() {
		return action;
	}

	public void setAction() {
		if (this.status == match)
			action = CLOSE;
		if (this.status == mismatch)
			action = OPEN;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	public String getUpdated_by() {
		return updatedBy;
	}

	public void setUpdated_by() {
		this.updatedBy = "systemgenerated";
	}

	public void setUpdated_by(String user) {
		this.updatedBy = user;
	}

	public String getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn() {
		this.updatedOn = LocalDateTime.now().toString();
	}

	public void setCatNOnLabel(String catNOnLabel) {
		this.catNOnLabel = catNOnLabel;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public void setSiteRollup(String siteRollup) {
		this.siteRollup = siteRollup;
	}

	public String getInvoiceFlag() {
		return invoiceFlag;
	}

	public void setInvoiceFlag(Set<String> invoiceItemList, String itemNo) {

		if (invoiceItemList.contains(itemNo) && approvedG == FLAG)
			this.invoiceFlag = FLAG;
		else
			this.invoiceFlag = na;
	}
}
