package com.relevancelab.ra.data;

public class OracleMaster {

	private String itemNo;
	private String itemStatus;
	private String itemType;
	private String prodcutName;
	private String ipOwnerName;
	private String integraManufactured;
	private String integraManufacturerName;
	private String integraManufacturerNameDesc;
	private String ils510K20;
	private String fdaProcode;
	private String ilsFDADeviceListingNo;
	private String makeBuy;
	private String ilsEstablishmentRegNo;
	private String ilsRegAddress;
	private String vendor510KOldHtsDescPrevious;
	private String vendorFDAEstablishmentRegNo;
	private String vendorDeviceNo;
	private String gmdnCode;
	private String siteDesc;
	private String oracleSubmissionNo;
	private String ilsDev21;
	private String ilsDev21original;
	public String getIlsDev21original() {
		return ilsDev21original;
	}

	public void setIlsDev21original(String ilsDev21original) {
		this.ilsDev21original = ilsDev21original;
	}

	private String ilsDev6;


	public String getIlsDev21() {
		return ilsDev21;
	}

	public void setIlsDev21(String ilsDev21) {

		if (ilsDev21 != null && ilsDev21.length() > 3) {
			int len = ilsDev21.length();
			this.ilsDev21 = ilsDev21.substring(len - 3, len);
		}
		else
		this.ilsDev21 = ilsDev21;
	}

	public String getIlsDev6() {
		return ilsDev6;
	}

	public void setIlsDev6(String ilsDev6) {
		this.ilsDev6 = ilsDev6;
	}

	
	public String getItemNo() {
		return itemNo;
	}

	public void setItemNo(String itemNo) {
		this.itemNo = itemNo;
	}

	public String getItemStatus() {
		return itemStatus;
	}

	public void setItemStatus(String itemStatus) {
		this.itemStatus = itemStatus;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public String getProdcutName() {
		return prodcutName;
	}

	public void setProdcutName(String prodcutName) {
		this.prodcutName = prodcutName;
	}

	public String getIpOwnerName() {
		return ipOwnerName;
	}

	public void setIpOwnerName(String ipOwnerName) {
		this.ipOwnerName = ipOwnerName;
	}

	public String getIntegraManufactured() {
		return integraManufactured;
	}

	public void setIntegraManufactured(String integraManufactured) {
		this.integraManufactured = integraManufactured;
	}

	public String getIntegraManufacturerName() {
		return integraManufacturerName;
	}

	public void setIntegraManufacturerName(String integraManufacturerName) {
		this.integraManufacturerName = integraManufacturerName;
	}

	public String getIntegraManufacturerNameDesc() {
		return integraManufacturerNameDesc;
	}

	public void setIntegraManufacturerNameDesc(String integraManufacturerNameDesc) {
		this.integraManufacturerNameDesc = integraManufacturerNameDesc;
	}

	public String getIls510K20() {
		return ils510K20;
	}

	public void setIls510K20(String ils510k20) {
		ils510K20 = ils510k20;
	}

	public String getFdaProcode() {
		return fdaProcode;
	}

	public void setFdaProcode(String fdaProcode) {
		this.fdaProcode = fdaProcode;
	}

	public String getIlsFDADeviceListingNo() {
		return ilsFDADeviceListingNo;
	}

	public void setIlsFDADeviceListingNo(String ilsFDADeviceListingNo) {
		this.ilsFDADeviceListingNo = ilsFDADeviceListingNo;
	}

	public String getMakeBuy() {
		return makeBuy;
	}

	public void setMakeBuy(String makeBuy) {
		this.makeBuy = makeBuy;
	}

	public String getIlsEstablishmentRegNo() {
		return ilsEstablishmentRegNo;
	}

	public void setIlsEstablishmentRegNo(String ilsEstablishmentRegNo) {
		this.ilsEstablishmentRegNo = ilsEstablishmentRegNo;
	}

	public String getIlsRegAddress() {
		return ilsRegAddress;
	}

	public void setIlsRegAddress(String ilsRegAddress) {
		this.ilsRegAddress = ilsRegAddress;
	}

	public String getVendor510KOldHtsDescPrevious() {
		return vendor510KOldHtsDescPrevious;
	}

	public void setVendor510KOldHtsDescPrevious(String vendor510kOldHtsDescPrevious) {
		vendor510KOldHtsDescPrevious = vendor510kOldHtsDescPrevious;
	}

	public String getVendorFDAEstablishmentRegNo() {
		return vendorFDAEstablishmentRegNo;
	}

	public void setVendorFDAEstablishmentRegNo(String vendorFDAEstablishmentRegNo) {
		this.vendorFDAEstablishmentRegNo = vendorFDAEstablishmentRegNo;
	}

	public String getVendorDeviceNo() {
		return vendorDeviceNo;
	}

	public void setVendorDeviceNo(String vendorDeviceNo) {
		this.vendorDeviceNo = vendorDeviceNo;
	}

	public String getGmdnCode() {
		return gmdnCode;
	}

	public void setGmdnCode(String gmdnCode) {
		this.gmdnCode = gmdnCode;
	}

	public String getSiteDesc() {
		return siteDesc;
	}

	public void setSiteDesc(String siteDesc) {
		this.siteDesc = siteDesc;
	}

	public String getOracleSubmissionNo() {
		return oracleSubmissionNo;
	}

	public void setOracleSubmissionNo(String oracleSubmissionNo) {
		this.oracleSubmissionNo = oracleSubmissionNo;
	}

}
