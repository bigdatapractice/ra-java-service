package com.relevancelab.ra.data;

import java.util.Set;

public class Agile {
	private String itemNo;
	private String itemStatus;
	private String itemType;
	private String productName;
	private String designSpecificationOwner;
	private String fdaClass;
	private Set<String> fdaProductCode;
	private String fdaRegulatoryStatus;
	private String subMissionNo;

	public String getSubMissionNo() {
		return subMissionNo;
	}

	public void setSubMissionNo(String subMissionNo) {
		this.subMissionNo = subMissionNo;
	}

	public String getItemNo() {
		return itemNo;
	}

	public void setItemNo(String itemNo) {
		this.itemNo = itemNo;
	}

	public String getItemStatus() {
		return itemStatus;
	}

	public void setItemStatus(String itemStatus) {
		this.itemStatus = itemStatus;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getDesignSpecificationOwner() {
		return designSpecificationOwner;
	}

	public void setDesignSpecificationOwner(String designSpecificationOwner) {
		this.designSpecificationOwner = designSpecificationOwner;
	}

	public String getFdaClass() {
		return fdaClass;
	}

	public void setFdaClass(String fdaClass) {
		this.fdaClass = fdaClass;
	}


	public Set<String> getFdaProductCode() {
		return fdaProductCode;
	}

	public void setFdaProductCode(Set<String> fdaProductCode) {
		this.fdaProductCode = fdaProductCode;
	}

	public String getFdaRegulatoryStatus() {
		return fdaRegulatoryStatus;
	}

	public void setFdaRegulatoryStatus(String fdaRegulatoryStatus) {
		this.fdaRegulatoryStatus = fdaRegulatoryStatus;
	}
}
