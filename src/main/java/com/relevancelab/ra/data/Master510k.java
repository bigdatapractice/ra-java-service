package com.relevancelab.ra.data;

public class Master510k {
	 private String clearanceApprovalDate;
	 private String submissionDate;
	 private String submissionNo;
	 private String deviceClass;
	public String getDeviceClass() {
		return deviceClass;
	}
	public void setDeviceClass(String deviceClass) {
		this.deviceClass = deviceClass;
	}
	public String getClearanceApprovalDate() {
		return clearanceApprovalDate;
	}
	public void setClearanceApprovalDate(String clearanceApprovalDate) {
		this.clearanceApprovalDate = clearanceApprovalDate;
	}
	public String getSubmissionDate() {
		return submissionDate;
	}
	public void setSubmissionDate(String getSubmissionDate) {
		this.submissionDate = getSubmissionDate;
	}
	public String getSubmissionNo() {
		return submissionNo;
	}
	public void setSubmissionNo(String submissionNo) {
		this.submissionNo = submissionNo;
	}
	
	
	

}
