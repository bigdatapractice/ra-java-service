package com.relevancelab.ra.data;

import java.util.Map;

public class AgileMasterList {
	private Map<String, Agile> agileList;
	private Map<String, Agile> agileListWODash;

	public Map<String, Agile> getAgileList() {
		return agileList;
	}

	public void setAgileList(Map<String, Agile> agileList) {
		this.agileList = agileList;
	}

	public Map<String, Agile> getAgileListWODash() {
		return agileListWODash;
	}

	public void setAgileListWODash(Map<String, Agile> agileListWODash) {
		this.agileListWODash = agileListWODash;
	}
}
