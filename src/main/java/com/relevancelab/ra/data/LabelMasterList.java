package com.relevancelab.ra.data;

import java.util.Map;

public class LabelMasterList {
	private Map<String, Label> labelList;
	private Map<String, Label> labelListWODash;

	public Map<String, Label> getLabelList() {
		return labelList;
	}

	public void setLabelList(Map<String, Label> labelList) {
		this.labelList = labelList;
	}

	public Map<String, Label> getLabelListWODash() {
		return labelListWODash;
	}

	public void setLabelListWODash(Map<String, Label> labelListWODash) {
		this.labelListWODash = labelListWODash;
	}
}
