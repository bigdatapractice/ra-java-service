package com.relevancelab.ra.data;

public class Audit {
	private String itemNo;
	private String itemInGudid;
	private String modelVersion;
	private String fdaProCodes;
	private String deviceExemptFromPremarketSubmissionRequirements;
	private String fdaPremarketSubmissionNumbers;
	private String fdaSupplementNumbers;
	private String fdaMedicalDeviceLisntingNodl;
	private String dlInFurlsBasedOnSubmissionInGudid;
	private String productCodeInFurlsBasedOnSubmissionInGudid;
	private String submissionExceptionInFurlsBasedOnDlInGudid;
	private String productCodeInFurlsBasedOndLInGudid;
	private String ils510k20;
	private String ilsFda21;
	private String ilsDev6;
	private String dLInFurlsBasedOnSubmissionInOracle;
	private String productCodeInFurlsBasedOnSubmissionInOracle;
	private String submissionExceptionInFurlsBasedOnDLInOracle1;
	private String productCodeInFurlsBasedOnDLInOracle;
	private String fdaClass1;
	private String fdaProductCode1;
	private String fdaRegulatoryStatus;
	private String dlInFurlsBasedOnProCodeInAgile;
	private String submissionExceptionInFurlsbasedonproductCodeInAgile;

	private String divison;
	private String siteRollup;

	// green section
	private String approved;
	private String fdaProCode;
	private String fdaClass;
	private String submissionType;
	private String fdaSubmissionNumber;
	private String fdaSubmissionSupplementNumber;
	private String submissionStatus;
	private String submissionDate;
	private String approvalDate;
	private String fdaMedicalDLnumber;
	private String k510DtIfUsedToBringSkuToMarket;
	private String gmdnCodes;

	private String catNoOnLabel;
	private String itemStatus;
	private String itemType;
	private String productName;
	private String productNameOnTheLabel;
	private String legalManufacturer;
	private String comments;
	private String updatedBy;
	private String action;
	private String userStatus;
	

	public String getFdaProCodes() {
		return fdaProCodes;
	}

	public void setFdaProCodes(String fdaProCodes) {
		this.fdaProCodes = fdaProCodes;
	}

	public String getFdaSupplementNumbers() {
		return fdaSupplementNumbers;
	}

	public void setFdaSupplementNumbers(String fdaSupplementNumbers) {
		this.fdaSupplementNumbers = fdaSupplementNumbers;
	}

	public String getFdaMedicalDeviceLisntingNodl() {
		return fdaMedicalDeviceLisntingNodl;
	}

	public void setFdaMedicalDeviceLisntingNodl(String fdaMedicalDeviceLisntingNodl) {
		this.fdaMedicalDeviceLisntingNodl = fdaMedicalDeviceLisntingNodl;
	}

	public String getProductCodeInFurlsBasedOnSubmissionInGudid() {
		return productCodeInFurlsBasedOnSubmissionInGudid;
	}

	public void setProductCodeInFurlsBasedOnSubmissionInGudid(String productCodeInFurlsBasedOnSubmissionInGudid) {
		this.productCodeInFurlsBasedOnSubmissionInGudid = productCodeInFurlsBasedOnSubmissionInGudid;
	}

	public String getProductCodeInFurlsBasedOndLInGudid() {
		return productCodeInFurlsBasedOndLInGudid;
	}

	public void setProductCodeInFurlsBasedOndLInGudid(String productCodeInFurlsBasedOndLInGudid) {
		this.productCodeInFurlsBasedOndLInGudid = productCodeInFurlsBasedOndLInGudid;
	}

	public String getIlsFda21() {
		return ilsFda21;
	}

	public void setIlsFda21(String ilsFda21) {
		this.ilsFda21 = ilsFda21;
	}

	public String getIlsDev6() {
		return ilsDev6;
	}

	public void setIlsDev6(String ilsDev6) {
		this.ilsDev6 = ilsDev6;
	}

	public String getdLInFurlsBasedOnSubmissionInOracle() {
		return dLInFurlsBasedOnSubmissionInOracle;
	}

	public void setdLInFurlsBasedOnSubmissionInOracle(String dLInFurlsBasedOnSubmissionInOracle) {
		this.dLInFurlsBasedOnSubmissionInOracle = dLInFurlsBasedOnSubmissionInOracle;
	}

	public String getProductCodeInFurlsBasedOnSubmissionInOracle() {
		return productCodeInFurlsBasedOnSubmissionInOracle;
	}

	public void setProductCodeInFurlsBasedOnSubmissionInOracle(String productCodeInFurlsBasedOnSubmissionInOracle) {
		this.productCodeInFurlsBasedOnSubmissionInOracle = productCodeInFurlsBasedOnSubmissionInOracle;
	}

	public String getSubmissionExceptionInFurlsBasedOnDLInOracle1() {
		return submissionExceptionInFurlsBasedOnDLInOracle1;
	}

	public void setSubmissionExceptionInFurlsBasedOnDLInOracle1(String submissionExceptionInFurlsBasedOnDLInOracle1) {
		this.submissionExceptionInFurlsBasedOnDLInOracle1 = submissionExceptionInFurlsBasedOnDLInOracle1;
	}

	public String getProductCodeInFurlsBasedOnDLInOracle() {
		return productCodeInFurlsBasedOnDLInOracle;
	}

	public void setProductCodeInFurlsBasedOnDLInOracle(String productCodeInFurlsBasedOnDLInOracle) {
		this.productCodeInFurlsBasedOnDLInOracle = productCodeInFurlsBasedOnDLInOracle;
	}

	public String getFdaClass1() {
		return fdaClass1;
	}

	public void setFdaClass1(String fdaClass1) {
		this.fdaClass1 = fdaClass1;
	}

	public String getFdaProductCode1() {
		return fdaProductCode1;
	}

	public void setFdaProductCode1(String fdaProductCode1) {
		this.fdaProductCode1 = fdaProductCode1;
	}

	public String getFdaRegulatoryStatus() {
		return fdaRegulatoryStatus;
	}

	public void setFdaRegulatoryStatus(String fdaRegulatoryStatus) {
		this.fdaRegulatoryStatus = fdaRegulatoryStatus;
	}

	public String getDlInFurlsBasedOnProCodeInAgile() {
		return dlInFurlsBasedOnProCodeInAgile;
	}

	public void setDlInFurlsBasedOnProCodeInAgile(String dlInFurlsBasedOnProCodeInAgile) {
		this.dlInFurlsBasedOnProCodeInAgile = dlInFurlsBasedOnProCodeInAgile;
	}

	public String getSubmissionExceptionInFurlsbasedonproductCodeInAgile() {
		return submissionExceptionInFurlsbasedonproductCodeInAgile;
	}

	public void setSubmissionExceptionInFurlsbasedonproductCodeInAgile(
			String submissionExceptionInFurlsbasedonproductCodeInAgile) {
		this.submissionExceptionInFurlsbasedonproductCodeInAgile = submissionExceptionInFurlsbasedonproductCodeInAgile;
	}

	public String getK510DtIfUsedToBringSkuToMarket() {
		return k510DtIfUsedToBringSkuToMarket;
	}

	public void setK510DtIfUsedToBringSkuToMarket(String k510DtIfUsedToBringSkuToMarket) {
		this.k510DtIfUsedToBringSkuToMarket = k510DtIfUsedToBringSkuToMarket;
	}

	public String getItemNo() {
		return itemNo;
	}

	public void setItemNo(String itemNo) {
		this.itemNo = itemNo;
	}

	public String getDivison() {
		return divison;
	}

	public void setDivison(String divison) {
		this.divison = divison;
	}

	public String getSiteRollup() {
		return siteRollup;
	}

	public void setSiteRollup(String siteRollup) {
		this.siteRollup = siteRollup;
	}

	public String getApproved() {
		return approved;
	}

	public void setApproved(String approved) {
		this.approved = approved;
	}

	public String getFdaProCode() {
		return fdaProCode;
	}

	public void setFdaProCode(String fdaProCode) {
		this.fdaProCode = fdaProCode;
	}

	public String getFdaClass() {
		return fdaClass;
	}

	public void setFdaClass(String fdaClass) {
		this.fdaClass = fdaClass;
	}

	public String getSubmissionType() {
		return submissionType;
	}

	public void setSubmissionType(String submissionType) {
		this.submissionType = submissionType;
	}

	public String getFdaSubmissionNumber() {
		return fdaSubmissionNumber;
	}

	public void setFdaSubmissionNumber(String fdaSubmissionNumber) {
		this.fdaSubmissionNumber = fdaSubmissionNumber;
	}

	public String getFdaMedicalDLnumber() {
		return fdaMedicalDLnumber;
	}

	public void setFdaMedicalDLnumber(String fdaMedicalDLnumber) {
		this.fdaMedicalDLnumber = fdaMedicalDLnumber;
	}

	public String getFdaSubmissionSupplementNumber() {
		return fdaSubmissionSupplementNumber;
	}

	public void setFdaSubmissionSupplementNumber(String fdaSubmissionSupplementNumber) {
		this.fdaSubmissionSupplementNumber = fdaSubmissionSupplementNumber;
	}

	public String getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(String approvalDate) {
		this.approvalDate = approvalDate;
	}

	public String getSubmissionStatus() {
		return submissionStatus;
	}

	public void setSubmissionStatus(String submissionStatus) {
		this.submissionStatus = submissionStatus;
	}

	public String getSubmissionDate() {
		return submissionDate;
	}

	public void setSubmissionDate(String submissionDate) {
		this.submissionDate = submissionDate;
	}

	public String getIls510k20() {
		return ils510k20;
	}

	public void setIls510k20(String ils510k20) {
		this.ils510k20 = ils510k20;
	}

	public String getGmdnCodes() {
		return gmdnCodes;
	}

	public void setGmdnCodes(String gmdnCodes) {
		this.gmdnCodes = gmdnCodes;
	}

	public String getCatNoOnLabel() {
		return catNoOnLabel;
	}

	public void setCatNoOnLabel(String catNoOnLabel) {
		this.catNoOnLabel = catNoOnLabel;
	}

	public String getItemStatus() {
		return itemStatus;
	}

	public void setItemStatus(String itemStatus) {
		this.itemStatus = itemStatus;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductNameOnTheLabel() {
		return productNameOnTheLabel;
	}

	public void setProductNameOnTheLabel(String productNameOnTheLabel) {
		this.productNameOnTheLabel = productNameOnTheLabel;
	}

	public String getLegalManufacturer() {
		return legalManufacturer;
	}

	public void setLegalManufacturer(String legalManufacturer) {
		this.legalManufacturer = legalManufacturer;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getItemInGudid() {
		return itemInGudid;
	}

	public void setItemInGudid(String itemInGudid) {
		this.itemInGudid = itemInGudid;
	}

	public String getModelVersion() {
		return modelVersion;
	}

	public void setModelVersion(String modelVersion) {
		this.modelVersion = modelVersion;
	}

	public String getDeviceExemptFromPremarketSubmissionRequirements() {
		return deviceExemptFromPremarketSubmissionRequirements;
	}

	public void setDeviceExemptFromPremarketSubmissionRequirements(
			String deviceExemptFromPremarketSubmissionRequirements) {
		this.deviceExemptFromPremarketSubmissionRequirements = deviceExemptFromPremarketSubmissionRequirements;
	}

	public String getFdaPremarketSubmissionNumbers() {
		return fdaPremarketSubmissionNumbers;
	}

	public void setFdaPremarketSubmissionNumbers(String fdaPremarketSubmissionNumbers) {
		this.fdaPremarketSubmissionNumbers = fdaPremarketSubmissionNumbers;
	}

	public String getDlInFurlsBasedOnSubmissionInGudid() {
		return dlInFurlsBasedOnSubmissionInGudid;
	}

	public void setDlInFurlsBasedOnSubmissionInGudid(String dlInFurlsBasedOnSubmissionInGudid) {
		this.dlInFurlsBasedOnSubmissionInGudid = dlInFurlsBasedOnSubmissionInGudid;
	}

	public String getSubmissionExceptionInFurlsBasedOnDlInGudid() {
		return submissionExceptionInFurlsBasedOnDlInGudid;
	}

	public void setSubmissionExceptionInFurlsBasedOnDlInGudid(String submissionExceptionInFurlsBasedOnDlInGudid) {
		this.submissionExceptionInFurlsBasedOnDlInGudid = submissionExceptionInFurlsBasedOnDlInGudid;
	}

	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}
}
