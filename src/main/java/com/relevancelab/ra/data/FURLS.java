package com.relevancelab.ra.data;

import java.util.Set;

public class FURLS {

	private Set<String> deviceListingNo;
	private String preMarketSubmissionNo;
	private String preMarketException;
	private Set<String> productCode;
	private String itemTypeFurls;

	public String getItemTypeFurls() {
		return itemTypeFurls;
	}

	public void setItemTypeFurls(String itemTypeFurls) {
		this.itemTypeFurls = itemTypeFurls;
	}

	

	public Set<String> getDeviceListingNo() {
		return deviceListingNo;
	}

	public void setDeviceListingNo(Set<String> deviceListingNo) {
		this.deviceListingNo = deviceListingNo;
	}

	public String getPreMarketSubmissionNo() {
		return preMarketSubmissionNo;
	}

	public void setPreMarketSubmissionNo(String preMarketSubmissionNo) {
		this.preMarketSubmissionNo = preMarketSubmissionNo;
	}

	public String getPreMarketException() {
		return preMarketException;
	}

	public void setPreMarketException(String preMarketException) {
		this.preMarketException = preMarketException;
	}

	public Set<String> getProductCode() {
		return productCode;
	}

	public void setProductCode(Set<String> productCode) {
		this.productCode = productCode;
	}

}
