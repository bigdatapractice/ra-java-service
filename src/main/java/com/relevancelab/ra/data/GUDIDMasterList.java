package com.relevancelab.ra.data;

import java.util.Map;

public class GUDIDMasterList {
	private Map<String, GUDID> gudidList;
	private Map<String, GUDID> gudidListWODash;
	public Map<String, GUDID> getGudidList() {
		return gudidList;
	}
	public void setGudidList(Map<String, GUDID> gudidList) {
		this.gudidList = gudidList;
	}
	public Map<String, GUDID> getGudidListWODash() {
		return gudidListWODash;
	}
	public void setGudidListWODash(Map<String, GUDID> gudidListWODash) {
		this.gudidListWODash = gudidListWODash;
	}
}
