package com.relevancelab.ra.data;

public class SourceComparison {
	private String match;
	private String misMatch;
	private String open;
	private String close;
	private String description;

	public String getMatch() {
		return match;
	}

	public void setMatch(String match) {
		this.match = match;
	}

	public String getMisMatch() {
		return misMatch;
	}

	public void setMisMatch(String misMatch) {
		this.misMatch = misMatch;
	}

	public String getOpen() {
		return open;
	}

	public void setOpen(String open) {
		this.open = open;
	}

	public String getClose() {
		return close;
	}

	public void setClose(String close) {
		this.close = close;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
