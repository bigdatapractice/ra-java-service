package com.relevancelab.ra.data;

import java.util.Map;
import java.util.Set;

public class FURLSMasterList {
	private Map<String, FURLS> furlsListBySN;
	private Map<String, FURLS> furlsListByDL;
	private Map<String, FURLS> furlsListByProductCode;

	

	

	public Map<String, FURLS> getFurlsListByProductCode() {
		return furlsListByProductCode;
	}

	public void setFurlsListByProductCode(Map<String, FURLS> furlsListByProductCode) {
		this.furlsListByProductCode = furlsListByProductCode;
	}

	public Map<String, FURLS> getFurlsListBySN() {
		return furlsListBySN;
	}

	public void setFurlsListBySN(Map<String, FURLS> furlsListBySN) {
		this.furlsListBySN = furlsListBySN;
	}

	public Map<String, FURLS> getFurlsListByDL() {
		return furlsListByDL;
	}

	public void setFurlsListByDL(Map<String, FURLS> furlsListByDL) {
		this.furlsListByDL = furlsListByDL;
	}
}
