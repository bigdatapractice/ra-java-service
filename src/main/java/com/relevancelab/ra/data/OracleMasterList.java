package com.relevancelab.ra.data;

import java.util.Map;

public class OracleMasterList {
	private Map<String, OracleMaster> oracleItemList;
	private Map<String, OracleMaster> oracleItemListWODash;
	public Map<String, OracleMaster> getOracleItemList() {
		return oracleItemList;
	}
	public void setOracleItemList(Map<String, OracleMaster> oracleItemList) {
		this.oracleItemList = oracleItemList;
	}
	public Map<String, OracleMaster> getOracleItemListWODash() {
		return oracleItemListWODash;
	}
	public void setOracleItemListWODash(Map<String, OracleMaster> oracleItemListWODash) {
		this.oracleItemListWODash = oracleItemListWODash;
	}
}
