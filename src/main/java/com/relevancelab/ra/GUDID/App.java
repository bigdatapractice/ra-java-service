package com.relevancelab.ra.GUDID;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.relevancelab.ra.GUDID.data.CompanyAddress;
import com.relevancelab.ra.GUDID.data.Contact;
import com.relevancelab.ra.GUDID.data.Device;
import com.relevancelab.ra.GUDID.data.DeviceSize;
import com.relevancelab.ra.GUDID.data.EnvironMentalCondiation;
import com.relevancelab.ra.GUDID.data.FDAListing;
import com.relevancelab.ra.GUDID.data.GmdnTerm;
import com.relevancelab.ra.GUDID.data.Identifier;
import com.relevancelab.ra.GUDID.data.PreMarketSubmission;
import com.relevancelab.ra.GUDID.data.ProductCode;
import com.relevancelab.ra.GUDID.data.Sterilization;
import com.relevancelab.ra.GUDID.util.Util;

/**
 * Hello world!
 *
 */
public class App {
	static Boolean tuncateCheck = false;

	public static void main(String args[]) {
		String gudidLocation = null;
		try {
			FileReader fr = new FileReader("config.properties");
			Properties properties = new Properties();
			properties.load(fr);
			gudidLocation = properties.getProperty("GUDID");

		} catch (Exception e) {
			e.printStackTrace();

		}

		File folder = new File(gudidLocation);
		File[] listOfFiles = folder.listFiles();

		for (File file : listOfFiles) {
			if (file.isFile()) {
				processXML(file.getPath());
			}
		}
	}

	public static void processXML(String file) {
		Device device = null;
		List<Device> devices = new ArrayList<>();
		try {
			File fXmlFile = new File(file);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);

			// optional, but recommended
			// read this -
			// http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
			doc.getDocumentElement().normalize();
			NodeList nList = doc.getElementsByTagName("device");
			for (int temp = 0; temp < nList.getLength(); temp++) {
				device = new Device();
				Node nNode = nList.item(temp);
				if (nNode != null && nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;
					device.setDeviceRecordStatus(
							eElement.getElementsByTagName("deviceRecordStatus").item(0).getTextContent());
					device.setDevicePublishDate(
							eElement.getElementsByTagName("devicePublishDate").item(0).getTextContent());
					device.setDeviceCommDistributionEndDate(
							eElement.getElementsByTagName("deviceCommDistributionEndDate").item(0).getTextContent());
					device.setDeviceCommDistributionStatus(
							eElement.getElementsByTagName("deviceCommDistributionStatus").item(0).getTextContent());
					device.setBrandName(eElement.getElementsByTagName("brandName").item(0).getTextContent());
					device.setVersionModelNumber(
							eElement.getElementsByTagName("versionModelNumber").item(0).getTextContent());
					device.setCatalogNumber(eElement.getElementsByTagName("catalogNumber").item(0).getTextContent());
					device.setDunsNumber(eElement.getElementsByTagName("dunsNumber").item(0).getTextContent());
					device.setCompanyName(eElement.getElementsByTagName("companyName").item(0).getTextContent());
					device.setDeviceCount(eElement.getElementsByTagName("deviceCount").item(0).getTextContent());
					device.setDeviceDescription(
							eElement.getElementsByTagName("deviceDescription").item(0).getTextContent());
					device.setDmExempt(eElement.getElementsByTagName("DMExempt").item(0).getTextContent());
					device.setPremarketExempt(
							eElement.getElementsByTagName("premarketExempt").item(0).getTextContent());
					device.setDeviceHCTP(eElement.getElementsByTagName("deviceHCTP").item(0).getTextContent());
					device.setDeviceKit(eElement.getElementsByTagName("deviceKit").item(0).getTextContent());
					device.setDeviceCombinationProduct(
							eElement.getElementsByTagName("deviceCombinationProduct").item(0).getTextContent());
					device.setSingleUse(eElement.getElementsByTagName("singleUse").item(0).getTextContent());
					device.setLotBatch(eElement.getElementsByTagName("lotBatch").item(0).getTextContent());
					device.setSerialNumber(eElement.getElementsByTagName("serialNumber").item(0).getTextContent());
					device.setManufacturingDate(
							eElement.getElementsByTagName("manufacturingDate").item(0).getTextContent());
					device.setExpirationDate(eElement.getElementsByTagName("expirationDate").item(0).getTextContent());
					device.setDonationIdNumber(
							eElement.getElementsByTagName("donationIdNumber").item(0).getTextContent());
					device.setLabeledContainsNRL(
							eElement.getElementsByTagName("labeledContainsNRL").item(0).getTextContent());
					device.setLabeledNoNRL(eElement.getElementsByTagName("labeledNoNRL").item(0).getTextContent());
					device.setMriSafetyStatus(
							eElement.getElementsByTagName("MRISafetyStatus").item(0).getTextContent());
					device.setRx(eElement.getElementsByTagName("rx").item(0).getTextContent());
					device.setOtc(eElement.getElementsByTagName("otc").item(0).getTextContent());

					// -----identifiers--
					Node identifierListNode = eElement.getElementsByTagName("identifiers").item(0);
					if (identifierListNode != null && identifierListNode.getNodeType() == Node.ELEMENT_NODE) {
						Element identifierListElement = (Element) identifierListNode;
						NodeList identifierList = identifierListElement.getElementsByTagName("identifier");

						List<Identifier> identifiers = new ArrayList<>();
						for (int identifiersCount = 0; identifiersCount < identifierList
								.getLength(); identifiersCount++) {
							Identifier identifier = new Identifier();
							Node identifierNode = identifierList.item(identifiersCount);
							if (nNode.getNodeType() == Node.ELEMENT_NODE) {
								Element identifierElement = (Element) identifierNode;
								identifier.setDeviceId(
										identifierElement.getElementsByTagName("deviceId").item(0).getTextContent());
								identifier.setDeviceIdType(identifierElement.getElementsByTagName("deviceIdType")
										.item(0).getTextContent());
								identifier.setDeviceIdIssuingAgency(identifierElement
										.getElementsByTagName("deviceIdIssuingAgency").item(0).getTextContent());
								identifier.setContainsDINumber(identifierElement
										.getElementsByTagName("containsDINumber").item(0).getTextContent());
								identifier.setPkgQuantity(
										identifierElement.getElementsByTagName("pkgQuantity").item(0).getTextContent());
								identifier.setPkgDiscontinueDate(identifierElement
										.getElementsByTagName("pkgDiscontinueDate").item(0).getTextContent());
								identifier.setPkgStatus(
										identifierElement.getElementsByTagName("pkgStatus").item(0).getTextContent());
								identifier.setPkgType(
										identifierElement.getElementsByTagName("pkgType").item(0).getTextContent());
								identifiers.add(identifier);
							}
						}
						device.setIdentifiers(identifiers);
					}

					// -----companyAddress--
					NodeList companyAddressNodeList = eElement.getElementsByTagName("companyAddress");
					for (int companyAddresssCount = 0; companyAddresssCount < companyAddressNodeList
							.getLength(); companyAddresssCount++) {
						CompanyAddress companyAddress = new CompanyAddress();
						Node companyAddressNode = companyAddressNodeList.item(companyAddresssCount);
						if (nNode.getNodeType() == Node.ELEMENT_NODE) {
							Element companyAddressElement = (Element) companyAddressNode;
							companyAddress.setStreet1(
									companyAddressElement.getElementsByTagName("street1").item(0).getTextContent());
							companyAddress.setStreet2(
									companyAddressElement.getElementsByTagName("street2").item(0).getTextContent());
							companyAddress.setCity(
									companyAddressElement.getElementsByTagName("city").item(0).getTextContent());
							companyAddress.setState(
									companyAddressElement.getElementsByTagName("state").item(0).getTextContent());
							companyAddress
									.setZip(companyAddressElement.getElementsByTagName("zip").item(0).getTextContent());
							companyAddress.setCountry(
									companyAddressElement.getElementsByTagName("country").item(0).getTextContent());
							device.setCompanyAddress(companyAddress);
						}
					}

					// -----contacts--
					Node contactParentNode = eElement.getElementsByTagName("contacts").item(0);
					if (contactParentNode != null && contactParentNode.getNodeType() == Node.ELEMENT_NODE) {
						Element contactNodeListElement = (Element) contactParentNode;
						NodeList contactNodeList = contactNodeListElement.getElementsByTagName("customerContact");
						List<Contact> contacts = new ArrayList<>();
						for (int contactsCount = 0; contactsCount < contactNodeList.getLength(); contactsCount++) {
							Contact contact = new Contact();
							Node contactNode = contactNodeList.item(contactsCount);
							if (nNode.getNodeType() == Node.ELEMENT_NODE) {
								Element contactElement = (Element) contactNode;
								contact.setPhone(contactElement.getElementsByTagName("phone").item(0).getTextContent());
								contact.setPhoneExtension(
										contactElement.getElementsByTagName("phoneExtension").item(0).getTextContent());
								contact.setEmail(contactElement.getElementsByTagName("email").item(0).getTextContent());
								contacts.add(contact);
							}
						}
						device.setContacts(contacts);
					}
					// -----preMarketSubmissions--
					Node preMarketSubmissionParentNode = eElement.getElementsByTagName("premarketSubmissions").item(0);
					if (preMarketSubmissionParentNode != null
							&& preMarketSubmissionParentNode.getNodeType() == Node.ELEMENT_NODE) {
						Element preMarketSubmissionListElement = (Element) preMarketSubmissionParentNode;
						NodeList preMarketSubmissionNodeList = preMarketSubmissionListElement
								.getElementsByTagName("premarketSubmission");
						List<PreMarketSubmission> preMarketSubmissions = new ArrayList<>();
						for (int preMarketSubmissionsCount = 0; preMarketSubmissionsCount < preMarketSubmissionNodeList
								.getLength(); preMarketSubmissionsCount++) {
							PreMarketSubmission preMarketSubmission = new PreMarketSubmission();
							Node preMarketSubmissionNode = preMarketSubmissionNodeList.item(preMarketSubmissionsCount);
							if (nNode.getNodeType() == Node.ELEMENT_NODE) {
								Element preMarketSubmissionElement = (Element) preMarketSubmissionNode;
								preMarketSubmission.setSubmissionNumber(preMarketSubmissionElement
										.getElementsByTagName("submissionNumber").item(0).getTextContent());
								preMarketSubmission.setSupplementNumber(preMarketSubmissionElement
										.getElementsByTagName("supplementNumber").item(0).getTextContent());
								preMarketSubmissions.add(preMarketSubmission);
							}
						}
						device.setPreMarketSubmissions(preMarketSubmissions);
					}
					// -----fdaListings--
					Node fdaListingParentNode = eElement.getElementsByTagName("fdaListings").item(0);
					if (fdaListingParentNode != null && fdaListingParentNode.getNodeType() == Node.ELEMENT_NODE) {
						Element fdaListingParentListElement = (Element) fdaListingParentNode;
						NodeList fdaListingNodeList = fdaListingParentListElement
								.getElementsByTagName("fdaListingNumber");
						List<FDAListing> fdaListings = new ArrayList<>();
						for (int fdaListingsCount = 0; fdaListingsCount < fdaListingNodeList
								.getLength(); fdaListingsCount++) {
							FDAListing fdaListing = new FDAListing();
							Element fdaListingElement = (Element) fdaListingNodeList.item(fdaListingsCount);
							fdaListing.setFdaListingNumber(fdaListingElement.getTextContent());
							fdaListings.add(fdaListing);
							/*
							 * if (nNode.getNodeType() == Node.ELEMENT_NODE) { Element fdaListingElement =
							 * (Element) fdaListingNode.getTextContent(); if
							 * (fdaListingElement.getElementsByTagName( "fdaListingNumber").item(0) != null)
							 * { fdaListing.setFdaListingNumber(fdaListingElement
							 * .getElementsByTagName("fdaListingNumber").item(0) .getTextContent());
							 * fdaListings.add(fdaListing); } }
							 */
						}
						device.setFdaListings(fdaListings);
					}

					// -----gmdnTerms--
					Node gmdnTermParentNode = eElement.getElementsByTagName("gmdnTerms").item(0);
					if (gmdnTermParentNode != null && gmdnTermParentNode.getNodeType() == Node.ELEMENT_NODE) {
						Element gmdnTermParentListElement = (Element) gmdnTermParentNode;
						NodeList gmdnTermNodeList = gmdnTermParentListElement.getElementsByTagName("gmdn");
						List<GmdnTerm> gmdnTerms = new ArrayList<>();
						for (int gmdnTermsCount = 0; gmdnTermsCount < gmdnTermNodeList.getLength(); gmdnTermsCount++) {
							GmdnTerm gmdnTerm = new GmdnTerm();
							Node gmdnTermNode = gmdnTermNodeList.item(gmdnTermsCount);
							if (nNode.getNodeType() == Node.ELEMENT_NODE) {
								Element gmdnTermElement = (Element) gmdnTermNode;
								gmdnTerm.setGmdnPTCode(
										gmdnTermElement.getElementsByTagName("gmdnPTCode").item(0).getTextContent());
								gmdnTerm.setGmdnPTName(
										gmdnTermElement.getElementsByTagName("gmdnPTName").item(0).getTextContent());
								gmdnTerm.setGmdnPTDefinition(gmdnTermElement.getElementsByTagName("gmdnPTDefinition")
										.item(0).getTextContent());
								gmdnTerms.add(gmdnTerm);
							}
						}
						device.setGmdnTerms(gmdnTerms);
					}

					// -----productCodes--
					Node productCodeParentNode = eElement.getElementsByTagName("productCodes").item(0);
					if (productCodeParentNode != null && productCodeParentNode.getNodeType() == Node.ELEMENT_NODE) {
						Element productCodeListElement = (Element) productCodeParentNode;
						NodeList productCodeNodeList = productCodeListElement.getElementsByTagName("fdaProductCode");
						List<ProductCode> productCodes = new ArrayList<>();
						for (int productCodesCount = 0; productCodesCount < productCodeNodeList
								.getLength(); productCodesCount++) {
							ProductCode productCode = new ProductCode();
							Node productCodeNode = productCodeNodeList.item(productCodesCount);
							if (nNode.getNodeType() == Node.ELEMENT_NODE) {
								Element productCodeElement = (Element) productCodeNode;
								productCode.setProductCode(productCodeElement.getElementsByTagName("productCode")
										.item(0).getTextContent());
								productCode.setProductCodeName(productCodeElement
										.getElementsByTagName("productCodeName").item(0).getTextContent());
								productCodes.add(productCode);
							}
						}
						device.setProductCodes(productCodes);
					}
					// -----deviceSizes--
					Node deviceSizeParentNode = eElement.getElementsByTagName("deviceSizes").item(0);
					if (deviceSizeParentNode != null && deviceSizeParentNode.getNodeType() == Node.ELEMENT_NODE) {
						Element deviceSizeListElement = (Element) deviceSizeParentNode;
						NodeList deviceSizeNodeList = deviceSizeListElement.getElementsByTagName("deviceSize");
						List<DeviceSize> deviceSizes = new ArrayList<>();
						for (int deviceSizesCount = 0; deviceSizesCount < deviceSizeNodeList
								.getLength(); deviceSizesCount++) {
							DeviceSize deviceSize = new DeviceSize();
							Node deviceSizeNode = deviceSizeNodeList.item(deviceSizesCount);
							if (nNode.getNodeType() == Node.ELEMENT_NODE) {
								Element deviceSizeElement = (Element) deviceSizeNode;
								deviceSize.setSizeType(
										deviceSizeElement.getElementsByTagName("sizeType").item(0).getTextContent());
								deviceSize.setSizeText(
										deviceSizeElement.getElementsByTagName("sizeText").item(0).getTextContent());
								deviceSize.setSize(
										deviceSizeElement.getElementsByTagName("size").item(0).getTextContent());
								deviceSizes.add(deviceSize);
							}
						}
						device.setDeviceSizes(deviceSizes);
					}

					// -----environmentalConditions--
					Node environmentalConditionParentNode = eElement.getElementsByTagName("environmentalConditions")
							.item(0);
					if (environmentalConditionParentNode != null
							&& environmentalConditionParentNode.getNodeType() == Node.ELEMENT_NODE) {
						Element environmentalConditionListElement = (Element) environmentalConditionParentNode;
						NodeList environmentalConditionNodeList = environmentalConditionListElement
								.getElementsByTagName("storageHandling");
						List<EnvironMentalCondiation> environmentalConditions = new ArrayList<>();
						for (int environmentalConditionsCount = 0; environmentalConditionsCount < environmentalConditionNodeList
								.getLength(); environmentalConditionsCount++) {
							EnvironMentalCondiation environmentalCondition = new EnvironMentalCondiation();
							Node environmentalConditionNode = environmentalConditionNodeList
									.item(environmentalConditionsCount);
							if (nNode.getNodeType() == Node.ELEMENT_NODE) {
								Element environmentalConditionElement = (Element) environmentalConditionNode;
								environmentalCondition.setStorageHandlingType(environmentalConditionElement
										.getElementsByTagName("storageHandlingType").item(0).getTextContent());
								environmentalCondition
										.setStorageHandlingSpecialConditionText(environmentalConditionElement
												.getElementsByTagName("storageHandlingSpecialConditionText").item(0)
												.getTextContent());
								environmentalConditions.add(environmentalCondition);
							}
						}
						device.setEnvironMentalCondiations(environmentalConditions);
					}

					// -----sterilization--
					NodeList sterilizationNodeList = eElement.getElementsByTagName("sterilization");
					List<Sterilization> sterilizations = new ArrayList<>();
					for (int sterilizationCount = 0; sterilizationCount < sterilizationNodeList
							.getLength(); sterilizationCount++) {
						Sterilization sterilization = new Sterilization();
						Node sterilizationNode = sterilizationNodeList.item(sterilizationCount);
						if (nNode.getNodeType() == Node.ELEMENT_NODE) {
							Element sterilizationElement = (Element) sterilizationNode;
							sterilization.setDeviceSterile(sterilizationElement.getElementsByTagName("deviceSterile")
									.item(0).getTextContent());
							sterilization.setSterilizationPriorToUse(sterilizationElement
									.getElementsByTagName("sterilizationPriorToUse").item(0).getTextContent());
							Node methodTypesParentNode = sterilizationElement.getElementsByTagName("methodTypes")
									.item(0);
							if (methodTypesParentNode != null
									&& methodTypesParentNode.getNodeType() == Node.ELEMENT_NODE) {
								Element methodTypesListElement = (Element) methodTypesParentNode;
								Node methodTypesNode = methodTypesListElement
										.getElementsByTagName("sterilizationMethod").item(0);
								if (methodTypesNode != null && methodTypesNode.getNodeType() == Node.ELEMENT_NODE)
									sterilization.setSterilizationMethod(methodTypesNode.getTextContent());
							}
							sterilizations.add(sterilization);
						}
					}
					device.setSterilization(sterilizations);
				}
				devices.add(device);
				System.out.println(temp);
			}
			Util util = new Util();
			if (!tuncateCheck) {
				tuncateCheck = true;
				util.empltyTablesBeforeInsert();
			}
			util.insertRecordsInPostgresTable(devices);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
