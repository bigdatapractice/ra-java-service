package com.relevancelab.ra.GUDID.util;

import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

import com.relevancelab.ra.GUDID.data.CompanyAddress;
import com.relevancelab.ra.GUDID.data.Contact;
import com.relevancelab.ra.GUDID.data.Device;
import com.relevancelab.ra.GUDID.data.DeviceSize;
import com.relevancelab.ra.GUDID.data.EnvironMentalCondiation;
import com.relevancelab.ra.GUDID.data.FDAListing;
import com.relevancelab.ra.GUDID.data.GmdnTerm;
import com.relevancelab.ra.GUDID.data.Identifier;
import com.relevancelab.ra.GUDID.data.PreMarketSubmission;
import com.relevancelab.ra.GUDID.data.ProductCode;
import com.relevancelab.ra.GUDID.data.Sterilization;
import java.util.Properties;
public class Util {
	public Connection getPostGresDBConnection() throws Exception {
		Connection connection = null;
		try {
			Class.forName("org.postgresql.Driver");
			FileReader fr=new FileReader("config.properties");
			Properties prop=new Properties();
			prop.load(fr);
			connection = DriverManager.getConnection(prop.getProperty("PGURL"),prop.getProperty("PGUSER"), prop.getProperty("PGPASS"));
		} catch (Exception ex) {

		}
		return connection;
	}
	public void empltyTablesBeforeInsert() throws Exception{
		Statement truncateTables=null;
		Connection conn=null;
		try{
		conn=this.getPostGresDBConnection();
		truncateTables=conn.createStatement();
		System.out.println("trucating tables before inserting new records");
		truncateTables.execute("truncate table public.l0_gudid_device");
		truncateTables.execute("truncate table public.l0_gudid_company_address");
		truncateTables.execute("truncate table public.l0_gudid_contact");
		truncateTables.execute("truncate table public.l0_gudid_pre_market_submission");
		truncateTables.execute("truncate table public.l0_gudid_fda_listing");
		truncateTables.execute("truncate table public.l0_gudid_gmdn_term");
		truncateTables.execute("truncate table public.l0_gudid_product_code");
		truncateTables.execute("truncate table public.l0_gudid_device_size");
		truncateTables.execute("truncate table public.l0_gudid_environmental_condiations");
		truncateTables.execute("truncate table public.l0_gudid_sterilization");
		
		}
		catch(Exception e){
			e.printStackTrace();
			
		}
		finally{
			if(conn!=null)
				conn.close();
		}
	}
	public void insertRecordsInPostgresTable(List<Device> deviceList) throws Exception{
		Connection conn = null;
		PreparedStatement pstmt = null;
		PreparedStatement identiferSmt = null;
		PreparedStatement companySmt = null;
		PreparedStatement contactSmt = null;
		PreparedStatement preMarketSubmissionSmt = null;
		PreparedStatement fdaListingSmt =null;
		PreparedStatement gmdnSmt = null;
		PreparedStatement productCodeSmt = null;
		PreparedStatement deviceSizeSmt = null;
		PreparedStatement environMentalCondiationSmt = null;
		PreparedStatement sterilizationSmt = null;
		
		
		try{
			conn = getPostGresDBConnection();
			
			pstmt = conn.prepareStatement("INSERT INTO l0_gudid_device(device_record_status,device_publish_date,device_comm_distribution_enddate,device_comm_distribution_status,brand_name,version_model_number,catalog_number,company_name,device_count,device_description,dm_exempt,premarket_exempt,device_hctp,device_kit,device_combination_product,single_use,lot_batch,serial_number,manufacturing_date,expiration_date,donation_id_number,labeled_contains_nrl,labeled_no_nrl,mri_safety_status,rx,otc,id,duns_number)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			identiferSmt = conn.prepareStatement("INSERT INTO l0_gudid_identifier(device_id,device_id_type,device_id_issuing_agency,contains_di_number,pkg_quantity,pkg_discontinue_date,pkg_status,pkg_type,refid)VALUES(?,?,?,?,?,?,?,?,?)");
			companySmt = conn.prepareStatement("INSERT INTO public.l0_gudid_company_address(street1,street2,city,state,zip,country,refid)VALUES(?,?,?,?,?,?,?)");
			contactSmt = conn.prepareStatement("INSERT INTO public.l0_gudid_contact(phone, phone_extension, email, refid)VALUES (?, ?, ?, ?)");
			preMarketSubmissionSmt = conn.prepareStatement("INSERT INTO public.l0_gudid_pre_market_submission(submission_number, supplement_number, refid)VALUES (?, ?, ?)");
			fdaListingSmt = conn.prepareStatement("INSERT INTO public.l0_gudid_fda_listing(fda_listing_number, refid)VALUES (?, ?)");
			gmdnSmt = conn.prepareStatement("INSERT INTO public.l0_gudid_gmdn_term(gmdn_pt_code,gmdn_pt_name, gmdn_pt_definition, refid)VALUES (?, ?, ?,?)");
			productCodeSmt = conn.prepareStatement("INSERT INTO public.l0_gudid_product_code(product_code, product_code_name,refid)VALUES (?, ?, ?)");
			deviceSizeSmt = conn.prepareStatement("INSERT INTO public.l0_gudid_device_size(size_type, size, size_text, refid)VALUES (?, ?, ?, ?)");
			environMentalCondiationSmt = conn.prepareStatement("INSERT INTO public.l0_gudid_environmental_condiations(storage_handling_type, value2, unit3, value4, unit5, storage_handling_special_condition_text, refid)VALUES (?, ?, ?, ?, ?, ?, ?)");
			sterilizationSmt = conn.prepareStatement("INSERT INTO public.l0_gudid_sterilization(device_sterile, sterilization_prior_to_use, method_types, sterilization_method,refid)VALUES (?, ?, ?, ?,?)");
			
			
			
			
			
			
			int i =0;
			int counter = 0;
			String selectMaxQuery = "select max(id) from l0_gudid_device";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(selectMaxQuery);
			while (rs.next()) {
				i = rs.getInt(1);
			}
			rs.close();
			stmt.close();
			
			int deviceLength = deviceList.size();
			for(Device device : deviceList){
				counter++;
				i++;
				pstmt.setString(1, device.getDeviceRecordStatus());
				pstmt.setString(2, device.getDevicePublishDate());
				pstmt.setString(3, device.getDeviceCommDistributionEndDate());
				pstmt.setString(4, device.getDeviceCommDistributionStatus());		
				pstmt.setString(5, device.getBrandName());
				pstmt.setString(6, device.getVersionModelNumber());		
				pstmt.setString(7, device.getCatalogNumber());
				pstmt.setString(8, device.getCompanyName());	
				pstmt.setString(9, device.getDeviceCount());
				pstmt.setString(10, device.getDeviceDescription());
				pstmt.setString(11, device.getDmExempt());
				pstmt.setString(12, device.getPremarketExempt());		
				pstmt.setString(13, device.getDeviceHCTP());
				pstmt.setString(14, device.getDeviceKit());		
				pstmt.setString(15, device.getDeviceCombinationProduct());
				pstmt.setString(16, device.getSingleUse());
				pstmt.setString(17, device.getLotBatch());		
				pstmt.setString(18, device.getSerialNumber());
				pstmt.setString(19, device.getManufacturingDate());		
				pstmt.setString(20, device.getExpirationDate());
				pstmt.setString(21, device.getDonationIdNumber());
				pstmt.setString(22, device.getLabeledContainsNRL());
				pstmt.setString(23, device.getLabeledNoNRL());
				pstmt.setString(24, device.getMriSafetyStatus());
				pstmt.setString(25, device.getRx());
				pstmt.setString(26, device.getOtc());
				pstmt.setInt(27, i);
				pstmt.setString(28, device.getDunsNumber());
				pstmt.addBatch();
				
				List<Identifier> identifiers = device.getIdentifiers();
				if (identifiers != null && !identifiers.isEmpty()){
					for (Identifier identifier :identifiers){
						identiferSmt.setString(1, identifier.getDeviceId());
						identiferSmt.setString(2, identifier.getDeviceIdType());
						identiferSmt.setString(3, identifier.getDeviceIdIssuingAgency());
						identiferSmt.setString(4, identifier.getContainsDINumber());		
						identiferSmt.setString(5, identifier.getPkgQuantity());
						identiferSmt.setString(6, identifier.getPkgDiscontinueDate());		
						identiferSmt.setString(7, identifier.getPkgStatus());
						identiferSmt.setString(8, identifier.getPkgType());
						identiferSmt.setInt(9, i);	
						identiferSmt.addBatch();
					}
				}
				
				CompanyAddress companyAddress = device.getCompanyAddress();
				if (companyAddress != null ){
					companySmt.setString(1, companyAddress.getStreet1());
					companySmt.setString(2, companyAddress.getStreet2());
					companySmt.setString(3, companyAddress.getCity());
					companySmt.setString(4, companyAddress.getState());		
					companySmt.setString(5, companyAddress.getZip());
					companySmt.setString(6, companyAddress.getCountry());
					companySmt.setInt(7, i);	
					companySmt.addBatch();
				}
				
				List<Contact> contacts = device.getContacts();
				if (contacts != null ){
					for(Contact contact : contacts){
						contactSmt.setString(1, contact.getPhone());
						contactSmt.setString(2, contact.getPhoneExtension());
						contactSmt.setString(3, contact.getEmail());
						contactSmt.setInt(4, i);	
						contactSmt.addBatch();
					}
				}
				
				List<PreMarketSubmission> preMarketSubmissions = device.getPreMarketSubmissions();
				if (preMarketSubmissions != null ){
					for(PreMarketSubmission preMarketSubmission : preMarketSubmissions){
						preMarketSubmissionSmt.setString(1, preMarketSubmission.getSubmissionNumber());
						preMarketSubmissionSmt.setString(2, preMarketSubmission.getSupplementNumber());
						preMarketSubmissionSmt.setInt(3, i);
						preMarketSubmissionSmt.addBatch();
					}
				}
				
				List<FDAListing> fdaListings = device.getFdaListings();
				if (fdaListings != null ){
					for(FDAListing fdaListing : fdaListings){
						fdaListingSmt.setString(1, fdaListing.getFdaListingNumber());
						fdaListingSmt.setInt(2, i);
						fdaListingSmt.addBatch();
					}
				}
				
				List<GmdnTerm> gmdnTerms = device.getGmdnTerms();
				if (gmdnTerms != null ){
					for(GmdnTerm gmdnTerm : gmdnTerms){
						gmdnSmt.setString(1, gmdnTerm.getGmdnPTCode());
						gmdnSmt.setString(2, gmdnTerm.getGmdnPTName());
						gmdnSmt.setString(3, gmdnTerm.getGmdnPTDefinition());
						gmdnSmt.setInt(4, i);
						gmdnSmt.addBatch();
					}
				}
				
				List<ProductCode> productCodes = device.getProductCodes();
				if (productCodes != null ){
					for(ProductCode productCode : productCodes){
						productCodeSmt.setString(1, productCode.getProductCode());
						productCodeSmt.setString(2, productCode.getProductCodeName());
						productCodeSmt.setInt(3, i);
						productCodeSmt.addBatch();
					}
				}
				
				List<DeviceSize> deviceSizes = device.getDeviceSizes();
				if (deviceSizes != null ){
					for(DeviceSize deviceSize : deviceSizes){
						deviceSizeSmt.setString(1, deviceSize.getSizeType());
						deviceSizeSmt.setString(2, deviceSize.getSize());
						deviceSizeSmt.setString(3, deviceSize.getSizeText());
						deviceSizeSmt.setInt(4, i);
						deviceSizeSmt.addBatch();
					}
				}
				
				List<EnvironMentalCondiation> environMentalCondiations = device.getEnvironMentalCondiations();
				if (environMentalCondiations != null ){
					for(EnvironMentalCondiation environMentalCondiation : environMentalCondiations){
						environMentalCondiationSmt.setString(1, environMentalCondiation.getStorageHandlingType());
						environMentalCondiationSmt.setString(2, "");
						environMentalCondiationSmt.setString(3, "");
						environMentalCondiationSmt.setString(4, "");
						environMentalCondiationSmt.setString(5, "");
						environMentalCondiationSmt.setString(6, environMentalCondiation.getStorageHandlingSpecialConditionText());
						environMentalCondiationSmt.setInt(7, i);
						environMentalCondiationSmt.addBatch();
					}
				}
				
				List<Sterilization> sterilizations = device.getSterilization();
				if (sterilizations != null ){
					for(Sterilization Sterilization : sterilizations){
						sterilizationSmt.setString(1, Sterilization.getDeviceSterile());
						sterilizationSmt.setString(2, Sterilization.getSterilizationPriorToUse());
						sterilizationSmt.setString(3, Sterilization.getMethodTypes());
						sterilizationSmt.setString(4, Sterilization.getSterilizationMethod());
						sterilizationSmt.setInt(5, i);
						sterilizationSmt.addBatch();
					}
				}
				
				if (counter % 1000 == 0 || counter == deviceLength) {
					pstmt.executeBatch();
					identiferSmt.executeBatch();
					companySmt.executeBatch();
					contactSmt.executeBatch();
					preMarketSubmissionSmt.executeBatch();
					fdaListingSmt.executeBatch();
					gmdnSmt.executeBatch();
					productCodeSmt.executeBatch();
					deviceSizeSmt.executeBatch();
					environMentalCondiationSmt.executeBatch();
					sterilizationSmt.executeBatch();
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			if (conn != null)
				conn.close();
			if (pstmt != null)
				pstmt.close();
			if (identiferSmt != null)
				identiferSmt.close();
			if (companySmt != null)
				companySmt.close();
			if (contactSmt!=null)
				contactSmt.close();
			if (preMarketSubmissionSmt != null)
				preMarketSubmissionSmt.close();
			if (fdaListingSmt != null)
				fdaListingSmt.close();
			if (gmdnSmt != null)
				gmdnSmt.close();
			if (productCodeSmt != null)
				productCodeSmt.close();
			if (deviceSizeSmt != null)
				deviceSizeSmt.close();
			if (environMentalCondiationSmt != null)
				environMentalCondiationSmt.close();
			if (sterilizationSmt != null)
				sterilizationSmt.close();
			System.out.println("Completed");
		}	
	}
}
