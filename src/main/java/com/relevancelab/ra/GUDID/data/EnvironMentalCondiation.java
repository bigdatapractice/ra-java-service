package com.relevancelab.ra.GUDID.data;

public class EnvironMentalCondiation {
	private String storageHandlingType;
	private String value2;
	private String unit3;
	private String value4;
	private String unit5;
	private String storageHandlingSpecialConditionText;

	public String getStorageHandlingType() {
		return storageHandlingType;
	}

	public void setStorageHandlingType(String storageHandlingType) {
		this.storageHandlingType = storageHandlingType;
	}

	public String getValue2() {
		return value2;
	}

	public void setValue2(String value2) {
		this.value2 = value2;
	}

	public String getUnit3() {
		return unit3;
	}

	public void setUnit3(String unit3) {
		this.unit3 = unit3;
	}

	public String getValue4() {
		return value4;
	}

	public void setValue4(String value4) {
		this.value4 = value4;
	}

	public String getUnit5() {
		return unit5;
	}

	public void setUnit5(String unit5) {
		this.unit5 = unit5;
	}

	public String getStorageHandlingSpecialConditionText() {
		return storageHandlingSpecialConditionText;
	}

	public void setStorageHandlingSpecialConditionText(String storageHandlingSpecialConditionText) {
		this.storageHandlingSpecialConditionText = storageHandlingSpecialConditionText;
	}
}
