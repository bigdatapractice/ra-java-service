package com.relevancelab.ra.GUDID.data;

public class GmdnTerm {
	private String gmdnPTCode;
	private String gmdnPTName;
	private String gmdnPTDefinition;
	
	public String getGmdnPTCode() {
		return gmdnPTCode;
	}
	public void setGmdnPTCode(String gmdnPTCode) {
		this.gmdnPTCode = gmdnPTCode;
	}
	public String getGmdnPTName() {
		return gmdnPTName;
	}
	public void setGmdnPTName(String gmdnPTName) {
		this.gmdnPTName = gmdnPTName;
	}
	public String getGmdnPTDefinition() {
		return gmdnPTDefinition;
	}
	public void setGmdnPTDefinition(String gmdnPTDefinition) {
		this.gmdnPTDefinition = gmdnPTDefinition;
	}
}
