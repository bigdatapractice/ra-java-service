package com.relevancelab.ra.GUDID.data;

public class ProductCode {
    private String productCode;
    private String productCodeName;
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductCodeName() {
		return productCodeName;
	}
	public void setProductCodeName(String productCodeName) {
		this.productCodeName = productCodeName;
	}
}
