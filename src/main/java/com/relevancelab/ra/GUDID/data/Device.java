package com.relevancelab.ra.GUDID.data;

import java.util.List;

public class Device {
	private int id;
	private String deviceRecordStatus;
	private String devicePublishDate;
	private String deviceCommDistributionEndDate;
	private String deviceCommDistributionStatus;
	private List<Identifier> identifiers;
	private String brandName;
	private String versionModelNumber;
	private String catalogNumber;
	private String dunsNumber;
	private String companyName;
	private CompanyAddress companyAddress;
	private String deviceCount;
	private String deviceDescription;
	private String dmExempt;
	private String premarketExempt;
	private String deviceHCTP;
	private String deviceKit;
	private String deviceCombinationProduct;
	private String singleUse;
	private String lotBatch;
	private String serialNumber;
	private String manufacturingDate;
	private String expirationDate;
	private String donationIdNumber;
	private String labeledContainsNRL;
	private String labeledNoNRL;
	private String mriSafetyStatus;
	private String rx;
	private String otc;
	private List<Contact> contacts;
	private List<PreMarketSubmission> preMarketSubmissions;
	private List<FDAListing> fdaListings;
	private List<GmdnTerm> gmdnTerms;
	private List<ProductCode> productCodes;
	private List<DeviceSize> deviceSizes;
	private List<EnvironMentalCondiation> environMentalCondiations;
	private List<Sterilization> sterilization;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDeviceRecordStatus() {
		return deviceRecordStatus;
	}

	public void setDeviceRecordStatus(String deviceRecordStatus) {
		this.deviceRecordStatus = deviceRecordStatus;
	}

	public String getDevicePublishDate() {
		return devicePublishDate;
	}

	public void setDevicePublishDate(String devicePublishDate) {
		this.devicePublishDate = devicePublishDate;
	}

	public String getDeviceCommDistributionEndDate() {
		return deviceCommDistributionEndDate;
	}

	public void setDeviceCommDistributionEndDate(String deviceCommDistributionEndDate) {
		this.deviceCommDistributionEndDate = deviceCommDistributionEndDate;
	}

	public String getDeviceCommDistributionStatus() {
		return deviceCommDistributionStatus;
	}

	public void setDeviceCommDistributionStatus(String deviceCommDistributionStatus) {
		this.deviceCommDistributionStatus = deviceCommDistributionStatus;
	}

	public List<Identifier> getIdentifiers() {
		return identifiers;
	}

	public void setIdentifiers(List<Identifier> identifiers) {
		this.identifiers = identifiers;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getVersionModelNumber() {
		return versionModelNumber;
	}

	public void setVersionModelNumber(String versionModelNumber) {
		this.versionModelNumber = versionModelNumber;
	}

	public String getCatalogNumber() {
		return catalogNumber;
	}

	public void setCatalogNumber(String catalogNumber) {
		this.catalogNumber = catalogNumber;
	}
	
	public String getDunsNumber() {
		return dunsNumber;
	}

	public void setDunsNumber(String dunsNumber) {
		this.dunsNumber = dunsNumber;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public CompanyAddress getCompanyAddress() {
		return companyAddress;
	}

	public void setCompanyAddress(CompanyAddress companyAddress) {
		this.companyAddress = companyAddress;
	}

	public String getDeviceCount() {
		return deviceCount;
	}

	public void setDeviceCount(String deviceCount) {
		this.deviceCount = deviceCount;
	}

	public String getDeviceDescription() {
		return deviceDescription;
	}

	public void setDeviceDescription(String deviceDescription) {
		this.deviceDescription = deviceDescription;
	}

	public String getDmExempt() {
		return dmExempt;
	}

	public void setDmExempt(String dmExempt) {
		this.dmExempt = dmExempt;
	}

	public String getPremarketExempt() {
		return premarketExempt;
	}

	public void setPremarketExempt(String premarketExempt) {
		this.premarketExempt = premarketExempt;
	}

	public String getDeviceHCTP() {
		return deviceHCTP;
	}

	public void setDeviceHCTP(String deviceHCTP) {
		this.deviceHCTP = deviceHCTP;
	}

	public String getDeviceKit() {
		return deviceKit;
	}

	public void setDeviceKit(String deviceKit) {
		this.deviceKit = deviceKit;
	}

	public String getDeviceCombinationProduct() {
		return deviceCombinationProduct;
	}

	public void setDeviceCombinationProduct(String deviceCombinationProduct) {
		this.deviceCombinationProduct = deviceCombinationProduct;
	}

	public String getSingleUse() {
		return singleUse;
	}

	public void setSingleUse(String singleUse) {
		this.singleUse = singleUse;
	}

	public String getLotBatch() {
		return lotBatch;
	}

	public void setLotBatch(String lotBatch) {
		this.lotBatch = lotBatch;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getManufacturingDate() {
		return manufacturingDate;
	}

	public void setManufacturingDate(String manufacturingDate) {
		this.manufacturingDate = manufacturingDate;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getDonationIdNumber() {
		return donationIdNumber;
	}

	public void setDonationIdNumber(String donationIdNumber) {
		this.donationIdNumber = donationIdNumber;
	}

	public String getLabeledContainsNRL() {
		return labeledContainsNRL;
	}

	public void setLabeledContainsNRL(String labeledContainsNRL) {
		this.labeledContainsNRL = labeledContainsNRL;
	}

	public String getLabeledNoNRL() {
		return labeledNoNRL;
	}

	public void setLabeledNoNRL(String labeledNoNRL) {
		this.labeledNoNRL = labeledNoNRL;
	}

	public String getMriSafetyStatus() {
		return mriSafetyStatus;
	}

	public void setMriSafetyStatus(String mriSafetyStatus) {
		this.mriSafetyStatus = mriSafetyStatus;
	}

	public String getRx() {
		return rx;
	}

	public void setRx(String rx) {
		this.rx = rx;
	}

	public String getOtc() {
		return otc;
	}

	public void setOtc(String otc) {
		this.otc = otc;
	}

	public List<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(List<Contact> contacts) {
		this.contacts = contacts;
	}

	public List<PreMarketSubmission> getPreMarketSubmissions() {
		return preMarketSubmissions;
	}

	public void setPreMarketSubmissions(List<PreMarketSubmission> preMarketSubmissions) {
		this.preMarketSubmissions = preMarketSubmissions;
	}

	public List<FDAListing> getFdaListings() {
		return fdaListings;
	}

	public void setFdaListings(List<FDAListing> fdaListings) {
		this.fdaListings = fdaListings;
	}

	public List<GmdnTerm> getGmdnTerms() {
		return gmdnTerms;
	}

	public void setGmdnTerms(List<GmdnTerm> gmdnTerms) {
		this.gmdnTerms = gmdnTerms;
	}

	public List<ProductCode> getProductCodes() {
		return productCodes;
	}

	public void setProductCodes(List<ProductCode> productCodes) {
		this.productCodes = productCodes;
	}

	public List<DeviceSize> getDeviceSizes() {
		return deviceSizes;
	}

	public void setDeviceSizes(List<DeviceSize> deviceSizes) {
		this.deviceSizes = deviceSizes;
	}

	public List<EnvironMentalCondiation> getEnvironMentalCondiations() {
		return environMentalCondiations;
	}

	public void setEnvironMentalCondiations(List<EnvironMentalCondiation> environMentalCondiations) {
		this.environMentalCondiations = environMentalCondiations;
	}

	public List<Sterilization> getSterilization() {
		return sterilization;
	}

	public void setSterilization(List<Sterilization> sterilization) {
		this.sterilization = sterilization;
	}
}
