package com.relevancelab.ra.GUDID.data;

public class Sterilization {
	private String deviceSterile;
	private String sterilizationPriorToUse;
	private String methodTypes;
	private String sterilizationMethod;
	public String getDeviceSterile() {
		return deviceSterile;
	}
	public void setDeviceSterile(String deviceSterile) {
		this.deviceSterile = deviceSterile;
	}
	public String getSterilizationPriorToUse() {
		return sterilizationPriorToUse;
	}
	public void setSterilizationPriorToUse(String sterilizationPriorToUse) {
		this.sterilizationPriorToUse = sterilizationPriorToUse;
	}
	public String getMethodTypes() {
		return methodTypes;
	}
	public void setMethodTypes(String methodTypes) {
		this.methodTypes = methodTypes;
	}
	public String getSterilizationMethod() {
		return sterilizationMethod;
	}
	public void setSterilizationMethod(String sterilizationMethod) {
		this.sterilizationMethod = sterilizationMethod;
	}
}
