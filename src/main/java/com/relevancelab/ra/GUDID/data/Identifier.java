package com.relevancelab.ra.GUDID.data;

public class Identifier {
	private String deviceId;
	private String deviceIdType;
	private String deviceIdIssuingAgency;
	private String containsDINumber;
	private String pkgQuantity;
	private String pkgDiscontinueDate;
	private String pkgStatus;
	private String pkgType;

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceIdType() {
		return deviceIdType;
	}

	public void setDeviceIdType(String deviceIdType) {
		this.deviceIdType = deviceIdType;
	}

	public String getDeviceIdIssuingAgency() {
		return deviceIdIssuingAgency;
	}

	public void setDeviceIdIssuingAgency(String deviceIdIssuingAgency) {
		this.deviceIdIssuingAgency = deviceIdIssuingAgency;
	}

	public String getContainsDINumber() {
		return containsDINumber;
	}

	public void setContainsDINumber(String containsDINumber) {
		this.containsDINumber = containsDINumber;
	}

	public String getPkgQuantity() {
		return pkgQuantity;
	}

	public void setPkgQuantity(String pkgQuantity) {
		this.pkgQuantity = pkgQuantity;
	}

	public String getPkgDiscontinueDate() {
		return pkgDiscontinueDate;
	}

	public void setPkgDiscontinueDate(String pkgDiscontinueDate) {
		this.pkgDiscontinueDate = pkgDiscontinueDate;
	}

	public String getPkgStatus() {
		return pkgStatus;
	}

	public void setPkgStatus(String pkgStatus) {
		this.pkgStatus = pkgStatus;
	}

	public String getPkgType() {
		return pkgType;
	}

	public void setPkgType(String pkgType) {
		this.pkgType = pkgType;
	}
}
