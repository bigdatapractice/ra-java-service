package com.relevancelab.ra.GUDID.data;

public class PreMarketSubmission {
	private String submissionNumber;
	private String supplementNumber;

	public String getSubmissionNumber() {
		return submissionNumber;
	}

	public void setSubmissionNumber(String submissionNumber) {
		this.submissionNumber = submissionNumber;
	}

	public String getSupplementNumber() {
		return supplementNumber;
	}

	public void setSupplementNumber(String supplementNumber) {
		this.supplementNumber = supplementNumber;
	}
}
