package com.relevancelab.ra.GUDID.data;

public class DeviceSize {
	private String sizeType;
	private String size;
	private String sizeText;
	public String getSizeType() {
		return sizeType;
	}
	public void setSizeType(String sizeType) {
		this.sizeType = sizeType;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getSizeText() {
		return sizeText;
	}
	public void setSizeText(String sizeText) {
		this.sizeText = sizeText;
	}
}
