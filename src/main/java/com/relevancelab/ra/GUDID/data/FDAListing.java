package com.relevancelab.ra.GUDID.data;

public class FDAListing {
	private String fdaListingNumber;

	public String getFdaListingNumber() {
		return fdaListingNumber;
	}

	public void setFdaListingNumber(String fdaListingNumber) {
		this.fdaListingNumber = fdaListingNumber;
	}
}
