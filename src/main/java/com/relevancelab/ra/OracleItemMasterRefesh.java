
package com.relevancelab.ra;

import java.io.FileReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

import org.etsi.uri.x01903.v13.impl.GenericTimeStampTypeImpl;

import com.relevancelab.ra.data.JobDetails;
import com.relevancelab.ra.util.Util;

public class OracleItemMasterRefesh {

	private OracleItemMasterRefesh() {

	};

	private static int batchSize = 1000;

	private static String dbURL = null;
	private static String dbUser = null;
	private static String dbPwd = null;

	private static String oracleHostName = null;
	private static String oracleUserName = null;
	private static String oraclePassword = null;

	public static void main(String[] args) throws Exception {
		String spectraURL = null;
		String spectraUser = null;
		String spectraPwd = null;
		String project;
		String job;
		long startTime = 0;
		JobDetails jobDetails = new JobDetails();
		FileReader fr = null;
		try {
			jobDetails = new JobDetails();
			fr = new FileReader("config.properties");
			Properties properties = new Properties();
			properties.load(fr);
			startTime = System.currentTimeMillis();
			dbURL = properties.getProperty("PGURL");
			dbUser = properties.getProperty("PGUSER");
			dbPwd = properties.getProperty("PGPASS");
			spectraURL = properties.getProperty("SPECTRAURL");
			spectraUser = properties.getProperty("SPECTRAUSR");
			spectraPwd = properties.getProperty("SPECTRAPWD");
			oracleHostName = properties.getProperty("SQLSERVER");
			oracleUserName = properties.getProperty("SERVERUSERNAME");
			oraclePassword = properties.getProperty("SERVERPASSWORD");
			batchSize = 5000;
			project = properties.getProperty("project");
			job = properties.getProperty("job");

			jobDetails.setProject(project);
			jobDetails.setJob(job);
			jobDetails.setDbURL(spectraURL);
			jobDetails.setDbUserName(spectraUser);
			jobDetails.setDbPassword(spectraPwd);
			jobDetails.setStartTime(startTime);
			jobDetails.setStatus(true);
			//refreshItemMaster();
			refreshInvoice();
			Util.updateJobDetails(jobDetails);
		} catch (Exception e) {
			jobDetails.setStatus(false);
			e.printStackTrace();
		} finally {
			if (fr != null)
				fr.close();
		}
	}

	private static void refreshItemMaster() {
		Connection oracleConn = null;
		ResultSet oracleRs = null;
		Connection postgresConn = null;
		Statement oracleStmt = null;
		PreparedStatement postgresPst = null;
		String oracleSQL;
		String postgresSQL;
		int counter = 0;
		try {
			oracleSQL = "SELECT  \"Organization\", \"Item\", \"DESCRIPTION\", \"Main/UOM:Primary\", \"Main/User Item Type\", \"Main/Item Status\", \"Inventory/Inventory Item\", \"Inventory/Stockable\", \"Inventory/Transactable\", "
					+ "\"Inventory/Revision Control\", \"Inventory/Reservable\", \"Inv/Check Material Shortage\","
					+ " \"Inventory/Cycle Count Enabled\", \"Inv/Positive Measurement Error\", "
					+ "\"Inv/Negative Measurement Error\", \"Inventory/Lot Expiry Control\", "
					+ "\"Inventory/Serial Generation\", \"Inv/Serial Starting Prefix\", "
					+ "\"Inv/Serial Starting Number\", \"Inventory/Lot Control\", \"Inventory/Locator Control\", "
					+ "\"Inventory/Lot Starting Prefix\", \"Inventory/Lot Starting Number\", \"Inventory/Maturity Days\", \"Inventory/Hold Days\","
					+ " \"Inv/Restrict Subinventory\", \"Inventory/Restrict Locators\", \"Inventory/Lot Status Enabled\", \"Inv/Serial Status Enabled\", \"Inventory/Lot Split Enabled\", \"Inv/Lot Translate Enabled\", \"Inventory/Lot Divisible\", \"BOM/BOM Allowed\", \"BOM/BOM Item Type\", \"BOM/Effective  Control\", \"Cost/Costing Enabled\", \"Cost/Inventory Asset Value\", \"Cost/Include in Roll Up\", \"Cost/Goods Sold Account\", \"Cost/Standard Lot Size\", \"Purchasing/Purchased\", \"Purchasing/Purchasable\", \"Pur/Use Approved Supplier\", \"Pur/Outside Processing Item\", \"Pur/Outside Process Unit Type\", \"Purchasing/RFQ Required\", \"Purchasing/Receipt Required\", \"Purchasing/Inspection Required\", \"Purchasing/Default Buyer\", \"Pur/Receipt Close Tolerance\", \"Purchasing/List Price\", \"Purchasing/Price Tolerance\", \"Purchasing/Expense Account\", \"Receiving/Receipt Routing\", \"Physical Attribute/Weight:UOM\", \"Physical Attribute/Weight\", \"PA/Dimension:UOM\", \"PA/Dimension:Length\", \"PA/Dimension:Width\", \"PA/Dimension:Height\", \"Physical Attribute/Container\", \"Physical Attr/OM Indivisible\", \"GP/Inventory Planning Method\", \"GP/Make/Buy\", \"Gp/Minimum Quantity\", \"Gp/Maximum Quantity\", \"Gp/Minimum Order Quantity\", \"Gp/Maximum Order Quantity\", \"GP/Safety Stock Method\", \"GP/Fixed Order Quantity\", \"GP/Fixed Days Supply\", \"GP/Fixed Lot Multiplier\", \"GP/Replishment Min Qty\", \"GP/Replish Min Days of Supply\", \"GP/Order Qty Max Qty\", \"GP/Order Qty Max Day of Supply\", \"GP/Oder Qty Fixed Qty\", \"GP/Release Authorization Reqd\", \"GP/Consigned\", \"Plan/Planing Method\", \"Plan/ForeCast Control\", \"Plan/Pegging\", \"Plan/Create Supply\", \"Lead Time/Preprocessing\", \"Lead Time/Processing\", \"Lead Time/Postprocessing\", \"Lead Time/Fixed\", \"Lead Time/Variable\", \"Lead/Cumulative Manufacturing\", \"Lead Time/Cumulative Total\", \"Lead Time/Lot_Size\", \"WIP/Build in WIP\", \"WIP/Supply Type\", \"WIP/Supply Subinventory\", \"WIP/Supply Locator\", \"OM/Customer Ordered\", \"OM/Customer Order Enabled\", \"OM/Shippable\", \"OM/Internal Order\", \"OM/Internal Order Enabled\", \"OM/OE Transactable\", \"OM/Pick Components\", \"OM/Assemble to Order\", \"OM/ATP Rule\", \"OM/Default Shipping Org\", \"OM/Default So Source Type\", \"OM/Returnable\", \"OM/RMA Inspection Required\", \"Invoicing/Invoiceable Item\", \"Invoicing/Invoice Enabled\", \"Invoicing/Sales Account\", \"Service/Service Request\", \"Service/Enable Defect Tracking\", \"Service/Contract Item Type\", \"Service/Create Fixed Asset\", \"Service/Instance Class\", \"Serv/Recover Part Disposition\", \"Service/Enable Service Billing\", \"Service/Billing Type\", \"Web/Web Status\", \"Web/Orderable On Web\", \"Web/Back Orderable\", \"DFF/Commission Flag\", \"DFF/Commission Start date\", \"DFF/Commission End date\", \"DFF/Storage Condition\", \"DFF/Returnable\", \"DFF/Context\", \"DFF/Country of Origin\", \"DFF/Integra Manufactured\", \"DFF/Integra Manufacturer Name\", \"DFF/HTS Code\", \"DFF/Schedule B Number\", \"DFF/Eccn\", \"DFF/Lisence Required\", \"DFF/501K/PMA/Exempt\", \"DFF/FDA Product Code(ILS)\", \"DFF/ILS EST Registration No\", \"DFF/Vendor Device Number\", \"DFF/FDA Device Listing#ILS\", \"DFF/EU Comodity Code\", \"DFF/Vendor FDA Registration#\", \"DFF/Preffered Name Code\", \"DFF/Canadian Medical License\", \"DFF/GMDN Code\", \"DFF/EMDNS Code\", \"LAST UPDATE DATE\"  FROM XX_ITEM_ATTRIBUTES_V";
			postgresSQL = "INSERT INTO oracle_item_master(organization,item,description,main_uom_primary,main_user_item_type,main_item_status,"
					+ "inventory_inventory_item,inventory_stockable,"
					+ "inventory_transactable,inventory_revision_control,inventory_reservable,inv_check_material_shortage,inventory_cycle_count_enabled,"
					+ "inv_positive_measurement_error,inv_negative_measurement_error,"
					+ "inventory_lot_expiry_control,inventory_serial_generation,inv_serial_starting_prefix,"
					+ "inv_serial_starting_number,inventory_lot_control,"
					+ "inventory_locator_control,inventory_lot_starting_prefix,inventory_lot_starting_number,"
					+ "inventory_maturity_days,inventory_hold_days,inv_restrict_subinventory,inventory_restrict_locators,"
					+ "inventory_lot_status_enabled,inv_serial_status_enabled,inventory_lot_split_enabled,inv_lot_translate_enabled,"
					+ "inventory_lot_divisible,bom_bom_allowed,bom_bom_item_type,bom_effective_control,cost_costing_enabled,"
					+ "cost_inventory_asset_value,cost_include_in_roll_up,cost_goods_sold_account,cost_standard_lot_size,"
					+ "purchasing_purchased,purchasing_purchasable,pur_use_approved_supplier,pur_outside_processing_item,"
					+ "pur_outside_process_unit_type,purchasing_rfq_required,purchasing_receipt_required,purchasing_inspection_required,"
					+ "purchasing_default_buyer,pur_receipt_close_tolerance,purchasing_list_price,purchasing_price_tolerance,purchasing_expense_account,receiving_receipt_routing,physical_attribute_weight_uom,physical_attribute_weight,pa_dimension_uom,pa_dimension_length,pa_dimension_width,pa_dimension_height,physical_attribute_container,physical_attr_om_indivisible,gp_inventory_planning_method,gp_make_buy,gp_minimum_quantity,gp_maximum_quantity,gp_minimum_order_quantity,gp_maximum_order_quantity,gp_safety_stock_method,gp_fixed_order_quantity,gp_fixed_days_supply,gp_fixed_lot_multiplier,gp_replishment_min_qty,gp_replish_min_days_of_supply,gp_order_qty_max_qty,gp_order_qty_max_day_of_supply,gp_oder_qty_fixed_qty,gp_release_authorization_reqd,gp_consigned,plan_planing_method,plan_forecast_control,plan_pegging,plan_create_supply,lead_time_preprocessing,lead_time_processing,lead_time_postprocessing,lead_time_fixed,lead_time_variable,lead_cumulative_manufacturing,lead_time_cumulative_total,lead_time_lot_size,wip_build_in_wip,wip_supply_type,wip_supply_subinventory,wip_supply_locator,om_customer_ordered,om_customer_order_enabled,om_shippable,om_internal_order,om_internal_order_enabled,om_oe_transactable,om_pick_components,om_assemble_to_order,om_atp_rule,om_default_shipping_org,om_default_so_source_type,om_returnable,om_rma_inspection_required,invoicing_invoiceable_item,invoicing_invoice_enabled,invoicing_sales_account,service_service_request,service_enable_defect_tracking,service_contract_item_type,service_create_fixed_asset,service_instance_class,serv_recover_part_disposition,service_enable_service_billing,service_billing_type,web_web_status,web_orderable_on_web,web_back_orderable,dff_commission_flag,dff_commission_start_date,dff_commission_end_date,dff_storage_condition,dff_returnable,dff_context,dff_country_of_origin,dff_integra_manufactured,dff_integra_manufacturer_name,dff_hts_code,dff_schedule_b_number,dff_eccn,dff_lisence_required,dff_501k_pma_exempt,dff_fda_product_code_ils,dff_ils_est_registration_no,dff_vendor_device_number,dff_fda_device_listing_ils,dff_eu_comodity_code,dff_vendor_fda_registration,dff_preffered_name_code,dff_canadian_medical_license,dff_gmdn_code,dff_emdns_code,last_update_date)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			oracleConn = Util.getOracleDBConnetion(oracleHostName, oracleUserName, oraclePassword);
			postgresConn = Util.getPostGresConnection(dbURL, dbUser, dbPwd);
			oracleStmt = oracleConn.createStatement();
			postgresPst = postgresConn.prepareStatement(postgresSQL);
			oracleRs = oracleStmt.executeQuery(oracleSQL);
			while (oracleRs.next()) {
				counter++;
				for (int columnIndex = 1; columnIndex <= 147; columnIndex++) {
					postgresPst.setString(columnIndex, oracleRs.getString(columnIndex));
				}
				postgresPst.addBatch();
				if (counter % batchSize == 0)
					postgresPst.executeBatch();
			}
			postgresPst.executeBatch();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (oracleRs != null)
				try {
					oracleRs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}

			if (oracleStmt != null)
				try {
					oracleStmt.close();
				} catch (Exception e) {
					e.printStackTrace();
				}

			if (oracleConn != null)
				try {
					oracleConn.close();
				} catch (Exception e) {
					e.printStackTrace();
				}

			if (postgresPst != null)
				try {
					postgresPst.close();
				} catch (Exception e) {
					e.printStackTrace();
				}

			if (postgresConn != null)
				try {
					postgresConn.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
		}
	}
	private static void refreshInvoice(){
		Connection oracleConn = null;
		ResultSet oracleRs = null;
		Connection postgresConn = null;
		Statement oracleStmt = null;
		PreparedStatement postgresPst = null;
		String oracleSQL;
		String postgresSQL;
		int i=0;
		int counter = 0;
		try {
			oracleSQL = "SELECT Invoice_dimCustomer_ShipTo.custnum AS Customer_Number, "
					+ "Invoice_dimCustomer_ShipTo.custname AS Customer_Name, Invoice_dimCountry_Rpt_ShipTo.countrycode AS CountryCode, Invoice_dimCustomer_ShipTo.zip     AS ZIP, Invoice_dimCustomer_ShipTo.city    AS City, Invoice_dimCountry_Rpt_ShipTo.countryname AS CountryName, Invoice_dimCountry_ShipTo.geo_region      AS Geographic_Region, Invoice_tblidate_InvoiceDate.idate AS Date8, tblFactInvoice.invnum AS Invoice_Number, tblFactInvoice.invtype      AS Invoice_Type, SUM(tblFactInvoice.extprice_usd)   AS Ext_Sales___USD, tblFactInvoice.lineitemnum  AS Line_Item_Number, SUM(tblFactInvoice.sellqty)  AS Sell_Qty, Invoice_dimCurrency_Trans.currencycode    AS TransCurrency, Invoice_dimCurrency_Func.currencycode     AS FuncCurrency, Invoice_dimProduct.sku      AS SKU, Invoice_dimProduct.productdescription     AS ProductDescription, SUM(tblFactInvoice.extprice_trans) AS Ext_Sales___Trans, T9.shortname   AS Data_Source, Invoice_dimProdRoll_GPR.gprl1      AS Global_Reporting_L1, Invoice_dimProdRoll_GPR.gprl2      AS Global_Reporting_L2, Invoice_dimProdRoll_GPR.gprl3      AS Global_Reporting_L3, Invoice_dimProdRoll_GPR.gprl4      AS Global_Reporting_L4,    Invoice_dimDcode.dcode      as Dcode FROM   (((((((dimcountry Invoice_dimCountry_ShipTo  inner join dimcustomer Invoice_dimCustomer_ShipTo   ON Invoice_dimCountry_ShipTo.idcountry =     Invoice_dimCustomer_ShipTo.countryid) full outer join dimcusttype Invoice_dimCustType_ShipTo      ON Invoice_dimCustType_ShipTo.idcusttype =   Invoice_dimCustomer_ShipTo.custtypeid)      left outer join ((((dimdcode Invoice_dimDCode     inner join dimproduct Invoice_dimProduct      ON Invoice_dimDCode.iddcode =   Invoice_dimProduct.dcodeid)    inner join tblfactinvoice_rpt tblFactInvoice     ON Invoice_dimProduct.idproduct =  tblFactInvoice.productid)    inner join dimcurrency Invoice_dimCurrency_Func     ON Invoice_dimCurrency_Func.idcurrency =  tblFactInvoice.funccurrencyid)   inner join dimcurrency Invoice_dimCurrency_Trans    ON Invoice_dimCurrency_Trans.idcurrency =      tblFactInvoice.transcurrencyid)     ON Invoice_dimCustomer_ShipTo.idcustomer =  tblFactInvoice.customerid)     full outer join (SELECT dimprodroll_gpr.idgpr AS idGPR,  dimprodroll_gpr.gprl1 AS GPRL1,  dimprodroll_gpr.gprl2 AS GPRL2,  dimprodroll_gpr.gprl3 AS GPRL3,  dimprodroll_gpr.gprl4 AS GPRL4  FROM   dimprodroll_gpr dimProdRoll_GPR  WHERE  dimprodroll_gpr.active_flag = 'Y')     Invoice_dimProdRoll_GPR    ON Invoice_dimProdRoll_GPR.idgpr = Invoice_dimDCode.gprid_c)    full outer join dimproductdivision Invoice_dimProductDivision   ON Invoice_dimProductDivision.idproductdivision =    Invoice_dimDCode.productdivisionid_c)   full outer join dimdatasource T9   ON T9.iddatasource = tblFactInvoice.datasourceid)  full outer join tblidate Invoice_tblidate_InvoiceDate  ON Invoice_tblidate_InvoiceDate.idateid =    tblFactInvoice.invoicedateid) left outer join dimcountry Invoice_dimCountry_Rpt_ShipTo ON Invoice_dimCustomer_ShipTo.rptcountryid =   Invoice_dimCountry_Rpt_ShipTo.idcountry WHERE  concat(Invoice_tblidate_InvoiceDate.iyear,Invoice_tblidate_InvoiceDate.imonth) > (concat(year(DATEADD(month, -12, GETDATE())), month(DATEADD(month, -12, GETDATE()))))    AND Invoice_dimCountry_Rpt_ShipTo.countrycode = 'USA' AND Invoice_dimCustType_ShipTo.typeid NOT IN ( N'104', N'103', N'5' ) AND Invoice_dimProductDivision.proddiv <> N'SPINE_HDWR' AND Invoice_dimCustType_ShipTo.custtype NOT IN (     N'Codman Affiliate Day1 Customer' ) GROUP  BY Invoice_dimCustomer_ShipTo.custnum,    Invoice_dimCustomer_ShipTo.custname,    Invoice_dimCountry_Rpt_ShipTo.countrycode,    Invoice_dimCustomer_ShipTo.zip,    Invoice_dimCustomer_ShipTo.city,    Invoice_dimCountry_Rpt_ShipTo.countryname, Invoice_dimCountry_ShipTo.geo_region,    Invoice_tblidate_InvoiceDate.idate,    tblFactInvoice.invnum,    tblFactInvoice.invtype,    tblFactInvoice.lineitemnum,Invoice_dimCurrency_Trans.currencycode,    Invoice_dimCurrency_Func.currencycode,    Invoice_dimProduct.sku,    Invoice_dimProduct.productdescription,    T9.shortname,    Invoice_dimProdRoll_GPR.gprl1,    Invoice_dimProdRoll_GPR.gprl2,   Invoice_dimProdRoll_GPR.gprl3,Invoice_dimProdRoll_GPR.gprl4,Invoice_dimDcode.dcode";
			postgresSQL = "INSERT INTO l0_invoice(customer_number,customer_name,country_code,country_name,"
					+ "invoice_date,invoice_number,invoice_type,"
					+ "sales_value,invoice_line_item_number,sell_qty,item_no,item_description"
					+ ")VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
			//String numericColumns = ["sales_value","sell_qty"];
			oracleConn = Util.getMsSqlSever(oracleHostName,oracleUserName, oraclePassword);
			postgresConn = Util.getPostGresConnection(dbURL, dbUser, dbPwd);
			oracleStmt = oracleConn.createStatement();
			postgresPst = postgresConn.prepareStatement(postgresSQL);
			System.out.println("Getting ResultSet");
			oracleRs = oracleStmt.executeQuery(oracleSQL);
			System.out.println("Got ResultSet");

			while (oracleRs.next()) {
				counter++;
				postgresPst.setString(1,oracleRs.getString("Customer_Number"));
				postgresPst.setString(2,oracleRs.getString("Customer_Name"));
				postgresPst.setString(3,oracleRs.getString("CountryCode"));
				postgresPst.setString(4,oracleRs.getString("CountryName"));
				postgresPst.setString(5,oracleRs.getDate("Date8").toString());
				postgresPst.setString(6,oracleRs.getString("Invoice_Number"));
				postgresPst.setString(7, oracleRs.getString("Invoice_Type"));
				postgresPst.setDouble(8,oracleRs.getDouble("Ext_Sales___USD"));
				postgresPst.setString(9,oracleRs.getString("Line_Item_Number"));
				postgresPst.setDouble(10,oracleRs.getDouble("Sell_Qty"));
				postgresPst.setString(11,oracleRs.getString("SKU"));
				postgresPst.setString(12,oracleRs.getString("ProductDescription"));
				/*for (int columnIndex = 1; columnIndex <= 12; columnIndex++) {
					postgresPst.setString(columnIndex,
							oracleRs.getString(columnIndex));
				}*/
				postgresPst.addBatch();
				System.out.println(i++);
				if (counter % batchSize == 0)
					postgresPst.executeBatch();
			
			}
			postgresPst.executeBatch();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (oracleRs != null)
				try {
					oracleRs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}

			if (oracleStmt != null)
				try {
					oracleStmt.close();
				} catch (Exception e) {
					e.printStackTrace();
				}

			if (oracleConn != null)
				try {
					oracleConn.close();
				} catch (Exception e) {
					e.printStackTrace();
				}

			if (postgresPst != null)
				try {
					postgresPst.close();
				} catch (Exception e) {
					e.printStackTrace();
				}

			if (postgresConn != null)
				try {
					postgresConn.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
		}
	
	}
	
}

