package com.relevancelab.ra;
/*this interface contains methods that are used to avid repeated code inside the function,or to use function
inside function using lambda
*/public interface repeatedMethods {
//this is used to get key for master 510 k by concatinating submission no and supplement no
String key(String submissionNo,String supplementNo);
}
