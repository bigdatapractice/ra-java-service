﻿drop view l1_gudid
  CREATE OR REPLACE VIEW l1_gudid AS 
 SELECT a.version_model_number,
    a.catalog_number,
    a.company_name,
    b.product_code,
    a.dm_exempt,
    c.submission_number,
    c.supplement_number,
    d.fda_listing_number,
    e.gmdn_pt_code,
    a.duns_number
   FROM l0_gudid_device a
     LEFT JOIN l0_gudid_product_code b ON a.id = b.refid
     LEFT JOIN l0_gudid_pre_market_submission c ON a.id = c.refid
     LEFT JOIN l0_gudid_fda_listing d ON a.id = d.refid
     LEFT JOIN l0_gudid_gmdn_term e ON a.id = e.refid
  GROUP BY a.version_model_number, a.catalog_number, a.company_name,b.product_code, a.dm_exempt, c.submission_number, c.supplement_number, d.fda_listing_number, e.gmdn_pt_code, a.duns_number;


  select version_model_number,count(1) from l1_gudid group by version_model_number order by 2 desc
  select * from l1_gudid where version_model_number='FFINSTSET'
  select * from l1_gudid where version_model_number='SET188-A001'

select version_model_number,count(1) from 
  (select version_model_number,supplement_number from l1_gudid group by version_model_number,supplement_number)a 
 group by  version_model_number
 having count(1)>1
drop view l1_agile;
create view l1_agile as 
 select model_version,lifecycle,device_description,fda_product_code from di_metadata where model_version is not null
 select count(*) from l1_agile
 select distinct lifecycle from l1_agile
select count(*) from l1_agile where lifecycle='Published' and model_version in (
 select model_version from l1_agile where lifecycle='Obsolete') limit 10
 
select model_version,published,count(1) from  l1_agile group by model_version order by 2 desc  