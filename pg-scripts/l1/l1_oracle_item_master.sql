﻿drop table l1_oracle_item_master;
create view l1_oracle_item_master as 
select itemno,inventory_item_status_code ,item_type ,description ,ip_owner_name ,intg_mfg_11 ,
intg_site_12 ,site_descr ,ils_510k_20 ,ils_fda_21 ,
ils_dev_6 ,make_buy ,ils_est_reg_22 ,ils_reg_address ,
vendor_510k_old_hts_descr ,vend_est_reg_25 ,vend_dev_23  ,
gmdn_28  from oracle_extract;