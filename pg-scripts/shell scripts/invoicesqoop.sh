#!/bin/sh

#
#SHELL SCRIPT TO EXECUTE REMOTE PROGRAM/SCRIPT
#

LOG=/home/qfind/integra/log/invoicesqoop-$(date "+%Y%m%d-%H%M").log

USERNAME=qfind
HOSTNAME=172.16.11.55
RSCRIPTPATH=/home/qfind/prodinvoicesqoop.sh



#echo "Command To Execute:ssh $USERNAME@${HOSTNAME} 'sh ${RSCRIPTPATH} && exit $?' >> ${LOG}"
echo "ssh $USERNAME@${HOSTNAME} 'sh ${RSCRIPTPATH} && exit $?' >> ${LOG}" | bash -
RC=$?

echo "Return CODE:${RC}"

if [ $RC -eq 0 ];
then
        echo "Success execution of ssh:Copy Successful"
        echo "Check Logs for more Details:${LOG}"
        exit $RC
else
   echo "SSH failed..Please check logs:${LOG} "
   echo $RC
  # indicating error so shell action fails in Oozie
  exit $RC
fi
