
#!/bin/sh

LOG=./Integra-$(date "+%Y%m%d-%H%M").log

export PYTHON_EGG_CACHE=./myeggs

sqoop eval --connect jdbc\:postgresql\://172.16.10.145/QFIND_RA --driver org.postgresql.Driver --username postgres  --password postgres --query "truncate l0_invoice"

sqoop import --connect 'jdbc:sqlserver://US311EDW004:1433;database=DimensionDB;username=DataStage;password=D4ta$tag3!' --query "SELECT  Invoice_dimCustomer_ShipTo.custnum AS Customer_Number, Invoice_dimCustomer_ShipTo.custname AS Customer_Name, Invoice_dimCountry_Rpt_ShipTo.countrycode AS CountryCode, Invoice_dimCustomer_ShipTo.zip     AS ZIP, Invoice_dimCustomer_ShipTo.city    AS City, Invoice_dimCountry_Rpt_ShipTo.countryname AS CountryName, Invoice_dimCountry_ShipTo.geo_region      AS Geographic_Region, Invoice_tblidate_InvoiceDate.idate AS Date8, tblFactInvoice.invnum AS Invoice_Number, tblFactInvoice.invtype      AS Invoice_Type, SUM(tblFactInvoice.extprice_usd)   AS Ext_Sales___USD, tblFactInvoice.lineitemnum  AS Line_Item_Number, SUM(tblFactInvoice.sellqty)  AS Sell_Qty, Invoice_dimCurrency_Trans.currencycode    AS TransCurrency, Invoice_dimCurrency_Func.currencycode     AS FuncCurrency, Invoice_dimProduct.sku      AS SKU, Invoice_dimProduct.productdescription     AS ProductDescription, SUM(tblFactInvoice.extprice_trans) AS Ext_Sales___Trans, T9.shortname   AS Data_Source, Invoice_dimProdRoll_GPR.gprl1      AS Global_Reporting_L1, Invoice_dimProdRoll_GPR.gprl2      AS Global_Reporting_L2, Invoice_dimProdRoll_GPR.gprl3      AS Global_Reporting_L3, Invoice_dimProdRoll_GPR.gprl4      AS Global_Reporting_L4,    Invoice_dimDcode.dcode      as Dcode FROM   (((((((dimcountry Invoice_dimCountry_ShipTo  inner join dimcustomer Invoice_dimCustomer_ShipTo   ON Invoice_dimCountry_ShipTo.idcountry =     Invoice_dimCustomer_ShipTo.countryid) full outer join dimcusttype Invoice_dimCustType_ShipTo      ON Invoice_dimCustType_ShipTo.idcusttype =   Invoice_dimCustomer_ShipTo.custtypeid)      left outer join ((((dimdcode Invoice_dimDCode     inner join dimproduct Invoice_dimProduct      ON Invoice_dimDCode.iddcode =   Invoice_dimProduct.dcodeid)    inner join tblfactinvoice_rpt tblFactInvoice     ON Invoice_dimProduct.idproduct =  tblFactInvoice.productid)    inner join dimcurrency Invoice_dimCurrency_Func     ON Invoice_dimCurrency_Func.idcurrency =  tblFactInvoice.funccurrencyid)   inner join dimcurrency Invoice_dimCurrency_Trans    ON Invoice_dimCurrency_Trans.idcurrency =      tblFactInvoice.transcurrencyid)     ON Invoice_dimCustomer_ShipTo.idcustomer =  tblFactInvoice.customerid)     full outer join (SELECT dimprodroll_gpr.idgpr AS idGPR,  dimprodroll_gpr.gprl1 AS GPRL1,  dimprodroll_gpr.gprl2 AS GPRL2,  dimprodroll_gpr.gprl3 AS GPRL3,  dimprodroll_gpr.gprl4 AS GPRL4  FROM   dimprodroll_gpr dimProdRoll_GPR  WHERE  dimprodroll_gpr.active_flag = 'Y')     Invoice_dimProdRoll_GPR    ON Invoice_dimProdRoll_GPR.idgpr = Invoice_dimDCode.gprid_c)    full outer join dimproductdivision Invoice_dimProductDivision   ON Invoice_dimProductDivision.idproductdivision =    Invoice_dimDCode.productdivisionid_c)   full outer join dimdatasource T9   ON T9.iddatasource = tblFactInvoice.datasourceid)  full outer join tblidate Invoice_tblidate_InvoiceDate  ON Invoice_tblidate_InvoiceDate.idateid =    tblFactInvoice.invoicedateid) left outer join dimcountry Invoice_dimCountry_Rpt_ShipTo ON Invoice_dimCustomer_ShipTo.rptcountryid =   Invoice_dimCountry_Rpt_ShipTo.idcountry WHERE  concat(Invoice_tblidate_InvoiceDate.iyear,Invoice_tblidate_InvoiceDate.imonth) > (concat(year(DATEADD(month, -12, GETDATE())), month(DATEADD(month, -12, GETDATE()))))    AND Invoice_dimCountry_Rpt_ShipTo.countrycode = 'USA' AND Invoice_dimCustType_ShipTo.typeid NOT IN ( N'104', N'103', N'5' ) AND Invoice_dimProductDivision.proddiv <> N'SPINE_HDWR' AND Invoice_dimCustType_ShipTo.custtype NOT IN (     N'Codman Affiliate Day1 Customer' ) AND  \$CONDITIONS GROUP  BY Invoice_dimCustomer_ShipTo.custnum,    Invoice_dimCustomer_ShipTo.custname,    Invoice_dimCountry_Rpt_ShipTo.countrycode,    Invoice_dimCustomer_ShipTo.zip,    Invoice_dimCustomer_ShipTo.city,    Invoice_dimCountry_Rpt_ShipTo.countryname, Invoice_dimCountry_ShipTo.geo_region,    Invoice_tblidate_InvoiceDate.idate,    tblFactInvoice.invnum,    tblFactInvoice.invtype,    tblFactInvoice.lineitemnum,Invoice_dimCurrency_Trans.currencycode,    Invoice_dimCurrency_Func.currencycode,    Invoice_dimProduct.sku,    Invoice_dimProduct.productdescription,    T9.shortname,    Invoice_dimProdRoll_GPR.gprl1,    Invoice_dimProdRoll_GPR.gprl2,   Invoice_dimProdRoll_GPR.gprl3,Invoice_dimProdRoll_GPR.gprl4,Invoice_dimDcode.dcode" --target-dir '/qa/edl/codman_stg/INVOICE_TABLE' --fields-terminated-by '|' -m 1 --delete-target-dir  >> ${LOG} 2>&1



if [ $? == 0 ]; then
        echo "Proceed to Sqoop Export.."
		sqoop export --connect jdbc\:postgresql\://172.16.10.145/QFIND_RA --driver org.postgresql.Driver --username postgres  --password postgres --table l0_invoice  --hcatalog-database codman_stg --hcatalog-table invoice_data --columns "customer_number ,customer_name ,countrycode,countryname,date8,invoice_number,invoice_type ,   ext_sales___usd  ,   line_item_number ,   sell_qty  ,   sku ,   productdescription"  --input-fields-terminated-by '|' --input-lines-terminated-by '\n'  --input-null-string "\\\\N" --input-null-non-string "\\\\N" --verbose  -m 1 >> ${LOG} 2>&1
		if [ $? == 0 ]; then
		echo "Sqoop Export Successful" 
		exit 0
		else
		echo "Sqoop Export Failed..Please check logs"
		exit 1
		fi
	else echo "sqoop import failed....."
	fi



