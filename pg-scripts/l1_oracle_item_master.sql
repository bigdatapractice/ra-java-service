﻿dff_fda_product_code_ils,dff_fda_device_listing_ils


drop view l1_oracle_item_master;
CREATE OR REPLACE VIEW l1_oracle_item_master AS 
 SELECT l0_oracle_item_master.item AS itemno,
    l0_oracle_item_master.main_item_status AS item_status,
    l0_oracle_item_master.main_user_item_type AS item_type,
    l0_oracle_item_master.description AS product_name,
    '' AS ip_owner_name,
    l0_oracle_item_master.dff_integra_manufactured AS integra_manufactured,
    l0_oracle_item_master.dff_integra_manufacturer_name AS integra_manufacturer_name,
    '' AS site_descr,
    l0_oracle_item_master.dff_501k_pma_exempt,
    l0_oracle_item_master.dff_fda_product_code_ils,
    l0_oracle_item_master.dff_fda_device_listing_ils,
    l0_oracle_item_master.gp_make_buy AS make_buy,
    l0_oracle_item_master.dff_ils_est_registration_no,
    '' AS ils_reg_address,
    '' AS vendor_510k_old_hts_descr,
    l0_oracle_item_master.dff_vendor_fda_registration,
    l0_oracle_item_master.dff_vendor_device_number,
    l0_oracle_item_master.dff_gmdn_code,
    dff_fda_product_code_ils as ils_dev_21,
    dff_fda_device_listing_ils as ils_dev_6
   FROM l0_oracle_item_master
  WHERE l0_oracle_item_master.organization = 'MST'::text;