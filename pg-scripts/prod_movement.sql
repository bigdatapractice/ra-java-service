
  -- production movement
--addition of extra columns in audit
  alter table source_consolidation_audit
  add column item_in_gudid text,
  add column model_version text,
  add column device_exempt_from_premarket_submission_requirements text,
  add column fda_premarket_submission_numbers text,
  add column dl_in_furls_based_on_submission_in_gudid text,
  add column submission__exception_in_furls_based_on_dl_in_gudid text,
  add column product_code_in_furls_based_on_submission_in_gudid text,
  add column approval_clearance_letter_filed_internally text,
  add column permission_to_use_letter_filed_internally text,
  add column fda_product_codes text,
  add column fda_submission_supplement_numbers text,
  add column fda_medical_device_listing_number_dl text,
  add column product_code_in_furls_based_on_dl_in_gudid_ text,
  add column ils_510k_20 text,
  add column ils_fda_21 text,
  add column ils_dev_6 text,
  add column dl_in_furls_based_on_submission_in_oracle text,
  add column product_code_in_furls_based_on_submission_in_oracle text,
  add column submission_exception_in_furls_based_on_dl_in_oracle1 text,
  add column product_code_in_furls_based_on_dl_in_oracle text,
  add column fda_class1 text,
  add column fda_product_code1 text,
  add column fda_regulatory_status text,
  add column dl_in_furls_based_on_submission_in_oracle_ text,
  add column submission_exception_in_furls_based_on_dl_in_oracle text,
  add column user_status text;
  

--audit trigger update to accomodate newly added columns to audit table  
  
CREATE OR REPLACE FUNCTION addsourceconsolidationaudit()
  RETURNS trigger AS
$BODY$
BEGIN
INSERT INTO source_consolidation_audit(

  item_no  ,
  division ,
  site_rollup ,
  approved ,
  fda_product_code ,
  fda_class ,
  submission_type ,
  fda_submission_number ,
  fda_medical_device_listing_number ,
  fda_submission_supplement_number ,
  approval_date ,
  submission_status ,
  submission_date ,
  k510_dt_if_used_to_bring_sku_to_market ,
  gmdn_codes ,
  cat_no_on_label ,
  item_status ,
  item_type ,
  product_name ,
  product_name_on_the_label ,
  legal_manufacturer ,
  comments ,
  updated_by ,
  updated_on ,
  action ,
  item_in_gudid ,
  model_version ,
  device_exempt_from_premarket_submission_requirements ,
  fda_premarket_submission_numbers ,
  dl_in_furls_based_on_submission_in_gudid ,
  submission__exception_in_furls_based_on_dl_in_gudid ,
  product_code_in_furls_based_on_submission_in_gudid ,
  approval_clearance_letter_filed_internally ,
  permission_to_use_letter_filed_internally ,
  fda_product_codes ,
  fda_submission_supplement_numbers ,
  fda_medical_device_listing_number_dl ,
  product_code_in_furls_based_on_dl_in_gudid_ ,
  ils_510k_20 ,
  ils_fda_21 ,
  ils_dev_6 ,
  dl_in_furls_based_on_submission_in_oracle ,
  product_code_in_furls_based_on_submission_in_oracle ,
  submission_exception_in_furls_based_on_dl_in_oracle1 ,
  product_code_in_furls_based_on_dl_in_oracle ,
  fda_class1 ,
  fda_product_code1 ,
  fda_regulatory_status ,
  dl_in_furls_based_on_submission_in_oracle_ ,
  submission_exception_in_furls_based_on_dl_in_oracle ,
  user_status
)values


(
  new.item_no  ,
  new.division ,
  new.site_rollup ,
  new.approved ,
  new.fda_product_code ,
  new.fda_class ,
  new.submission_type ,
  new.fda_submission_number ,
  new.fda_medical_device_listing_number ,
  new.fda_submission_supplement_number ,
  new.approval_date ,
  new.submission_status ,
  new.submission_date ,
  new.k510_dt_if_used_to_bring_sku_to_market ,
  new.gmdn_codes ,
  new.cat_no_on_label ,
  new.item_status ,
  new.item_type ,
  new.product_name ,
  new.product_name_on_the_label ,
  new.legal_manufacturer ,
  new.comments ,
  new.updated_by ,
  now() ,
  new.action ,
  new.item_in_gudid ,
  new.model_version ,
  new.device_exempt_from_premarket_submission_requirements ,
  new.fda_premarket_submission_numbers ,
  new.dl_in_furls_based_on_submission_in_gudid ,
  new.submission__exception_in_furls_based_on_dl_in_gudid ,
  new.product_code_in_furls_based_on_submission_in_gudid ,
  new.approval_clearance_letter_filed_internally ,
  new.permission_to_use_letter_filed_internally ,
  new.fda_product_codes ,
  new.fda_submission_supplement_numbers ,
  new.fda_medical_device_listing_number_dl ,
  new.product_code_in_furls_based_on_dl_in_gudid_ ,
  new.ils_510k_20 ,
  new.ils_fda_21 ,
  new.ils_dev_6 ,
  new.dl_in_furls_based_on_submission_in_oracle ,
  new.product_code_in_furls_based_on_submission_in_oracle ,
  new.submission_exception_in_furls_based_on_dl_in_oracle1 ,
  new.product_code_in_furls_based_on_dl_in_oracle ,
  new.fda_class1 ,
  new.fda_product_code1 ,
  new.fda_regulatory_status ,
  new.dl_in_furls_based_on_submission_in_oracle_ ,
  new.submission_exception_in_furls_based_on_dl_in_oracle,
  new.user_status
  );
 RETURN NEW;
END $BODY$
  LANGUAGE plpgsql VOLATILE

 
 
 --update in view to get lastest user and system data of item nos in the audit table. takes only the data from the audit where items have been updated by user
 
 CREATE OR REPLACE VIEW updated_user_results AS 
 SELECT b.item_no,
    b.division,
    b.site_rollup,
    b.gmdn_codes,
    b.cat_no_on_label,
    b.item_status,
    b.item_type,
    b.product_name,
    b.product_name_on_the_label,
    b.legal_manufacturer,
    b.item_in_gudid,
    b.model_version,
    b.fda_product_codes,
    b.device_exempt_from_premarket_submission_requirements,
    b.fda_premarket_submission_numbers,
    b.fda_submission_supplement_numbers,
    b.fda_medical_device_listing_number_dl,
    b.dl_in_furls_based_on_submission_in_gudid,
    b.product_code_in_furls_based_on_submission_in_gudid,
    b.submission__exception_in_furls_based_on_dl_in_gudid,
    b.product_code_in_furls_based_on_dl_in_gudid_,
    b.ils_510k_20,
    b.ils_fda_21,
    b.ils_dev_6,
    b.dl_in_furls_based_on_submission_in_oracle,
    b.product_code_in_furls_based_on_submission_in_oracle,
    b.submission_exception_in_furls_based_on_dl_in_oracle1,
    b.product_code_in_furls_based_on_dl_in_oracle,
    b.fda_class1,
    b.fda_product_code1,
    b.fda_regulatory_status,
    b.dl_in_furls_based_on_submission_in_oracle_,
    b.submission_exception_in_furls_based_on_dl_in_oracle,
    b.approved,
    b.fda_product_code,
    b.fda_class,
    b.submission_type,
    b.fda_submission_number,
    b.fda_submission_supplement_number,
    b.submission_status,
    b.submission_date,
    b.approval_date,
    b.k510_dt_if_used_to_bring_sku_to_market,
    b.fda_medical_device_listing_number,
    b.comments,
    b.updated_by,
    b.version,
    b.updated_on,
    b.action,
	b.user_status
   FROM l1_source_consolidation b
  WHERE b.updated_by <> 'systemgenerated'::text
UNION
 SELECT a.item_no,
    a.division,
    a.site_rollup,
    a.gmdn_codes,
    a.cat_no_on_label,
    a.item_status,
    a.item_type,
    a.product_name,
    a.product_name_on_the_label,
    a.legal_manufacturer,
    a.item_in_gudid,
    a.model_version,
    a.fda_product_codes,
    a.device_exempt_from_premarket_submission_requirements,
    a.fda_premarket_submission_numbers,
    a.fda_submission_supplement_numbers,
    a.fda_medical_device_listing_number_dl,
    a.dl_in_furls_based_on_submission_in_gudid,
    a.product_code_in_furls_based_on_submission_in_gudid,
    a.submission__exception_in_furls_based_on_dl_in_gudid,
    a.product_code_in_furls_based_on_dl_in_gudid_,
    a.ils_510k_20,
    a.ils_fda_21,
    a.ils_dev_6,
    a.dl_in_furls_based_on_submission_in_oracle,
    a.product_code_in_furls_based_on_submission_in_oracle,
    a.submission_exception_in_furls_based_on_dl_in_oracle1,
    a.product_code_in_furls_based_on_dl_in_oracle,
    a.fda_class1,
    a.fda_product_code1,
    a.fda_regulatory_status,
    a.dl_in_furls_based_on_submission_in_oracle_,
    a.submission_exception_in_furls_based_on_dl_in_oracle,
    a.approved,
    a.fda_product_code,
    a.fda_class,
    a.submission_type,
    a.fda_submission_number,
    a.fda_submission_supplement_number,
    a.submission_status,
    a.submission_date,
    a.approval_date,
    a.k510_dt_if_used_to_bring_sku_to_market,
    a.fda_medical_device_listing_number,
    a.comments,
    a.updated_by,
    a.version,
    a.updated_on,
    a.action,
	a.user_status
   FROM ( SELECT b.item_no,
            b.division,
            b.site_rollup,
            b.gmdn_codes,
            b.cat_no_on_label,
            b.item_status,
            b.item_type,
            b.product_name,
            b.product_name_on_the_label,
            b.legal_manufacturer,
            b.item_in_gudid,
            b.model_version,
            b.fda_product_codes,
            b.device_exempt_from_premarket_submission_requirements,
            b.fda_premarket_submission_numbers,
            b.fda_submission_supplement_numbers,
            b.fda_medical_device_listing_number_dl,
            b.dl_in_furls_based_on_submission_in_gudid,
            b.product_code_in_furls_based_on_submission_in_gudid,
            b.submission__exception_in_furls_based_on_dl_in_gudid,
            b.product_code_in_furls_based_on_dl_in_gudid_,
            b.ils_510k_20,
            b.ils_fda_21,
            b.ils_dev_6,
            b.dl_in_furls_based_on_submission_in_oracle,
            b.product_code_in_furls_based_on_submission_in_oracle,
            b.submission_exception_in_furls_based_on_dl_in_oracle1,
            b.product_code_in_furls_based_on_dl_in_oracle,
            b.fda_class1,
            b.fda_product_code1,
            b.fda_regulatory_status,
            b.dl_in_furls_based_on_submission_in_oracle_,
            b.submission_exception_in_furls_based_on_dl_in_oracle,
            b.approved,
            b.fda_product_code,
            b.fda_class,
            b.submission_type,
            b.fda_submission_number,
            b.fda_submission_supplement_number,
            b.submission_status,
            b.submission_date,
            b.approval_date,
            b.k510_dt_if_used_to_bring_sku_to_market,
            b.fda_medical_device_listing_number,
            b.comments,
            b.updated_by,
            b.version,
            b.updated_on,
            b.action,
			b.user_status,
            row_number() OVER (PARTITION BY b.item_no ORDER BY b.updated_on DESC) AS rn
           FROM ( SELECT a_2.item_no,
                    a_2.division,
                    a_2.site_rollup,
                    a_2.gmdn_codes,
                    a_2.cat_no_on_label,
                    a_2.item_status,
                    a_2.item_type,
                    a_2.product_name,
                    a_2.product_name_on_the_label,
                    a_2.legal_manufacturer,
                    a_2.item_in_gudid,
                    a_2.model_version,
                    a_2.fda_product_codes,
                    a_2.device_exempt_from_premarket_submission_requirements,
                    a_2.fda_premarket_submission_numbers,
                    a_2.fda_submission_supplement_numbers,
                    a_2.fda_medical_device_listing_number_dl,
                    a_2.dl_in_furls_based_on_submission_in_gudid,
                    a_2.product_code_in_furls_based_on_submission_in_gudid,
                    a_2.submission__exception_in_furls_based_on_dl_in_gudid,
                    a_2.product_code_in_furls_based_on_dl_in_gudid_,
                    a_2.ils_510k_20,
                    a_2.ils_fda_21,
                    a_2.ils_dev_6,
                    a_2.dl_in_furls_based_on_submission_in_oracle,
                    a_2.product_code_in_furls_based_on_submission_in_oracle,
                    a_2.submission_exception_in_furls_based_on_dl_in_oracle1,
                    a_2.product_code_in_furls_based_on_dl_in_oracle,
                    a_2.fda_class1,
                    a_2.fda_product_code1,
                    a_2.fda_regulatory_status,
                    a_2.dl_in_furls_based_on_submission_in_oracle_,
                    a_2.submission_exception_in_furls_based_on_dl_in_oracle,
                    a_2.approved,
                    a_2.fda_product_code,
                    a_2.fda_class,
                    a_2.submission_type,
                    a_2.fda_submission_number,
                    a_2.fda_submission_supplement_number,
                    a_2.submission_status,
                    a_2.submission_date,
                    a_2.approval_date,
                    a_2.k510_dt_if_used_to_bring_sku_to_market,
                    a_2.fda_medical_device_listing_number,
                    a_2.comments,
                    a_2.updated_by,
                    a_2.version,
                    a_2.updated_on,
                    a_2.action,
					a_2.user_status
                   FROM l1_source_consolidation a_2
                  WHERE a_2.updated_by <> 'systemgenerated'::text) a_1
             LEFT JOIN ( SELECT b_1.item_no,
                    b_1.division,
                    b_1.site_rollup,
                    b_1.gmdn_codes,
                    b_1.cat_no_on_label,
                    b_1.item_status,
                    b_1.item_type,
                    b_1.product_name,
                    b_1.product_name_on_the_label,
                    b_1.legal_manufacturer,
                    b_1.item_in_gudid,
                    b_1.model_version,
                    b_1.fda_product_codes,
                    b_1.device_exempt_from_premarket_submission_requirements,
                    b_1.fda_premarket_submission_numbers,
                    b_1.fda_submission_supplement_numbers,
                    b_1.fda_medical_device_listing_number_dl,
                    b_1.dl_in_furls_based_on_submission_in_gudid,
                    b_1.product_code_in_furls_based_on_submission_in_gudid,
                    b_1.submission__exception_in_furls_based_on_dl_in_gudid,
                    b_1.product_code_in_furls_based_on_dl_in_gudid_,
                    b_1.ils_510k_20,
                    b_1.ils_fda_21,
                    b_1.ils_dev_6,
                    b_1.dl_in_furls_based_on_submission_in_oracle,
                    b_1.product_code_in_furls_based_on_submission_in_oracle,
                    b_1.submission_exception_in_furls_based_on_dl_in_oracle1,
                    b_1.product_code_in_furls_based_on_dl_in_oracle,
                    b_1.fda_class1,
                    b_1.fda_product_code1,
                    b_1.fda_regulatory_status,
                    b_1.dl_in_furls_based_on_submission_in_oracle_,
                    b_1.submission_exception_in_furls_based_on_dl_in_oracle,
                    b_1.approved,
                    b_1.fda_product_code,
                    b_1.fda_class,
                    b_1.submission_type,
                    b_1.fda_submission_number,
                    b_1.fda_submission_supplement_number,
                    b_1.submission_status,
                    b_1.submission_date,
                    b_1.approval_date,
                    b_1.k510_dt_if_used_to_bring_sku_to_market,
                    b_1.fda_medical_device_listing_number,
                    b_1.comments,
                    b_1.updated_by,
                    b_1.version,
                    b_1.updated_on,
                    b_1.action,
					b_1.user_status
                   FROM source_consolidation_audit b_1
                  WHERE b_1.updated_by = 'systemgenerated'::text) b ON a_1.item_no = b.item_no) a
  WHERE a.rn <= 1;



  
--consolidation function called to compare between temp_l1_source_consolidation and l1_source_consolidation  
  
  CREATE OR REPLACE FUNCTION consolidationupdate()
  RETURNS void AS
$BODY$
begin


 update temp_l1_source_consolidation set md5=
  md5
  (COALESCE(item_no::text,''::text)||
  COALESCE(is_item_in_oracle::text,''::text)||
  COALESCE(is_item_in_agile::text,''::text)||
  COALESCE(cat_no_on_label::text,''::text)||
  COALESCE(item_status::text,''::text)||
  COALESCE(item_type::text,''::text)||
  COALESCE(division::text,''::text)||
  COALESCE(site_rollup::text,''::text)||
  COALESCE(product_name::text,''::text)||
  COALESCE(product_name_on_the_label::text,''::text)||
  COALESCE(legal_manufacturer::text,''::text)||
  COALESCE(design_specification_owner::text,''::text)||
  COALESCE(intg_mfg_11::text,''::text)||
  COALESCE(intg_site_12::text,''::text)||
  COALESCE(approved::text,''::text)||
  COALESCE(fda_product_code::text,''::text)||
  COALESCE(fda_class::text,''::text)||
  COALESCE(submission_type::text,''::text)||
  COALESCE(fda_submission_number::text,''::text)||
  COALESCE(fda_submission_supplement_number::text,''::text)||
  COALESCE(submission_status::text,''::text)||
  COALESCE(submission_date::text,''::text)||
  COALESCE(approval_date::text,''::text)||
  COALESCE(k510_dt_if_used_to_bring_sku_to_market::text,''::text)||
  COALESCE(fda_medical_device_listing_number::text,''::text)||
  COALESCE(gmdn_codes::text,''::text)||
  COALESCE(item_in_gudid::text,''::text)||
  COALESCE(model_version::text,''::text)||
  COALESCE(cat_no::text,''::text)||
  COALESCE(labeler_duns_in_gudid::text,''::text)||
  COALESCE(fda_product_codes::text,''::text)||
  COALESCE(device_exempt_from_premarket_submission_requirements::text,''::text)||
  COALESCE(fda_premarket_submission_numbers::text,''::text)||
  COALESCE(fda_submission_supplement_numbers::text,''::text)||
  COALESCE(fda_medical_device_listing_number_dl::text,''::text)||
  COALESCE(gmdn_codes1::text,''::text)||
  COALESCE(dl_in_furls_based_on_submission_in_gudid::text,''::text)||
  COALESCE(product_code_in_furls_based_on_submission_in_gudid::text,''::text)||
  COALESCE(submission__exception_in_furls_based_on_dl_in_gudid::text,''::text)||
  COALESCE(product_code_in_furls_based_on_dl_in_gudid_::text,''::text)||
  COALESCE(ils_510k_20::text,''::text)||
  COALESCE(ils_fda_21::text,''::text)||
  COALESCE(ils_dev_6::text,''::text)||
  COALESCE(dl_in_furls_based_on_submission_in_oracle::text,''::text)||
  COALESCE(product_code_in_furls_based_on_submission_in_oracle::text,''::text)||
  COALESCE(submission_exception_in_furls_based_on_dl_in_oracle1::text,''::text)||
  COALESCE(product_code_in_furls_based_on_dl_in_oracle::text,''::text)||
  COALESCE(fda_class1::text,''::text)||
  COALESCE(fda_product_code1::text,''::text)||
  COALESCE(fda_regulatory_status::text,''::text)||
  COALESCE(dl_in_furls_based_on_submission_in_oracle_::text,''::text)|| 
  COALESCE(submission_exception_in_furls_based_on_dl_in_oracle::text,''::text)|| 
  COALESCE(device_class_from_fda_public_pro_code_db_based_on_pro_code_in_a::text,''::text)||
  COALESCE(make_buy::text,''::text)||
  COALESCE(ils_est_reg_22::text,''::text)||
  COALESCE(vend_est_reg_25::text,''::text)||
  COALESCE(vend_dev_23::text,''::text)||
  COALESCE(gmdn_28::text,''::text)
  );


  
 update l1_source_consolidation set md5=
  md5
  (COALESCE(item_no::text,''::text)||
  COALESCE(is_item_in_oracle::text,''::text)||
  COALESCE(is_item_in_agile::text,''::text)||
  COALESCE(cat_no_on_label::text,''::text)||
  COALESCE(item_status::text,''::text)||
  COALESCE(item_type::text,''::text)||
  COALESCE(division::text,''::text)||
  COALESCE(site_rollup::text,''::text)||
  COALESCE(product_name::text,''::text)||
  COALESCE(product_name_on_the_label::text,''::text)||
  COALESCE(legal_manufacturer::text,''::text)||
  COALESCE(design_specification_owner::text,''::text)||
  COALESCE(intg_mfg_11::text,''::text)||
  COALESCE(intg_site_12::text,''::text)||
  COALESCE(approved::text,''::text)||
  COALESCE(fda_product_code::text,''::text)||
  COALESCE(fda_class::text,''::text)||
  COALESCE(submission_type::text,''::text)||
  COALESCE(fda_submission_number::text,''::text)||
  COALESCE(fda_submission_supplement_number::text,''::text)||
  COALESCE(submission_status::text,''::text)||
  COALESCE(submission_date::text,''::text)||
  COALESCE(approval_date::text,''::text)||
  COALESCE(k510_dt_if_used_to_bring_sku_to_market::text,''::text)||
  COALESCE(fda_medical_device_listing_number::text,''::text)||
  COALESCE(gmdn_codes::text,''::text)||
  COALESCE(item_in_gudid::text,''::text)||
  COALESCE(model_version::text,''::text)||
  COALESCE(cat_no::text,''::text)||
  COALESCE(labeler_duns_in_gudid::text,''::text)||
  COALESCE(fda_product_codes::text,''::text)||
  COALESCE(device_exempt_from_premarket_submission_requirements::text,''::text)||
  COALESCE(fda_premarket_submission_numbers::text,''::text)||
  COALESCE(fda_submission_supplement_numbers::text,''::text)||
  COALESCE(fda_medical_device_listing_number_dl::text,''::text)||
  COALESCE(gmdn_codes1::text,''::text)||
  COALESCE(dl_in_furls_based_on_submission_in_gudid::text,''::text)||
  COALESCE(product_code_in_furls_based_on_submission_in_gudid::text,''::text)||
  COALESCE(submission__exception_in_furls_based_on_dl_in_gudid::text,''::text)||
  COALESCE(product_code_in_furls_based_on_dl_in_gudid_::text,''::text)||
  COALESCE(ils_510k_20::text,''::text)||
  COALESCE(ils_fda_21::text,''::text)||
  COALESCE(ils_dev_6::text,''::text)||
  COALESCE(dl_in_furls_based_on_submission_in_oracle::text,''::text)||
  COALESCE(product_code_in_furls_based_on_submission_in_oracle::text,''::text)||
  COALESCE(submission_exception_in_furls_based_on_dl_in_oracle1::text,''::text)||
  COALESCE(product_code_in_furls_based_on_dl_in_oracle::text,''::text)||
  COALESCE(fda_class1::text,''::text)||
  COALESCE(fda_product_code1::text,''::text)||
  COALESCE(fda_regulatory_status::text,''::text)||
  COALESCE(dl_in_furls_based_on_submission_in_oracle_::text,''::text)|| 
  COALESCE(submission_exception_in_furls_based_on_dl_in_oracle::text,''::text)|| 
  COALESCE(device_class_from_fda_public_pro_code_db_based_on_pro_code_in_a::text,''::text)||
  COALESCE(make_buy::text,''::text)||
  COALESCE(ils_est_reg_22::text,''::text)||
  COALESCE(vend_est_reg_25::text,''::text)||
  COALESCE(vend_dev_23::text,''::text)||
  COALESCE(gmdn_28::text,''::text)
  );
  
  
  delete from l1_source_consolidation a using temp_l1_source_consolidation b where a.item_no=b.item_no and a.md5!=b.md5;
 
  insert into l1_source_consolidation  select a.* from temp_l1_source_consolidation a left join l1_source_consolidation b on
  a.md5=b.md5  where b.md5 is null ;


  end;
$BODY$
  LANGUAGE plpgsql VOLATILE

  
  
  
  
  