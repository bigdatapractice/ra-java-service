﻿


CREATE OR REPLACE VIEW l1_agile AS 
 SELECT DISTINCT l0_parts_metadata.item_number,
    l0_parts_metadata.subclass AS item_type,
    l0_parts_metadata.lifecycle AS item_status,
    l0_parts_metadata.description AS product_name,
    l0_parts_metadata.design_spec_owner,
    l0_parts_metadata.fda_class,
    l0_parts_metadata.fda_product_codes,
    l0_parts_metadata.fda_regulatory_status,
    b.model_version,
    b.premarket_submission_number,
    b.lifecycle
   FROM l0_parts_metadata left join
(select a.model_version,premarket_submission_number,lifecycle from
(select * from 
(select model_version,latest_rev,premarket_submission_number,
ROW_NUMBER () over(PARTITION by model_version order by latest_rev::float desc) as rn 
 from l0_di_metadata)a where a.rn<=1)a left join 
(
 select  model_version,lifecycle from l0_di_metadata where lifecycle='Published' group by model_version,lifecycle
 union 
select * from (
select a.* from 
 (select  model_version,lifecycle from l0_di_metadata where lifecycle='Obsolete' group by model_version,lifecycle
)a
left join
( select  model_version,lifecycle from l0_di_metadata where lifecycle='Published' group by model_version,lifecycle
)b
on a.model_version=b.model_version
 )a where a.model_version is not null
 )b on a.model_version=b.model_version)b
  on l0_parts_metadata.parts_or_documents = 'Parts'::text AND l0_parts_metadata.item_number IS NOT NULL and l0_parts_metadata.item_number=b.model_version






